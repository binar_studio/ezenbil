<?php

return [
    'order_status' => ['cart', 'cancelled', 'checkout_checkout', 'checkout_review', 'checkout_payment', 'checkout_complete', 'pending', 'processing', 'completed'],
    'discount_type' => [
        'percentage' => 'Faiz endirimi',
        'nplus1' => 'N alana M hədiyyə'
    ]
];