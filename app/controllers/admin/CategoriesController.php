<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 11/17/14
 * Time: 8:07 PM
 */

namespace Admin;

use Category;
use Feature;
use UnitGroup;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Config;

class CategoriesController extends AdminController {

    public function getIndex()
    {

        $nodes = null;
        $sector_id = null;

        $sector_list = array();

        $sectors = Config::get('app.sections');

        foreach($sectors['byid'] as $id => $sector)
        {
            $sector_list[$id] = $sector['slug'];
        }

        if(Input::has('sector') && Input::get('sector') != null) {
            $sector_id = Input::get('sector');
            $nodes = Category::roots()->where('sector_id', '==', $sector_id)->get();
        } else {
            $nodes = Category::roots()->get();
        }

        return View::make('admin.categories.index')
            ->with(compact('nodes', 'sector_id', 'sector_list'));

    }

    public function getViewGeneral($id)
    {

        $category = Category::find($id);

        return View::make('admin.categories.tab-general', compact('category'));
    }

    public function getViewFeatures($id)
    {

        $category = Category::find($id);
        $featuresList = Feature::get()->lists('global_name', 'id');
        $unitGroupList = UnitGroup::lists('name', 'id');

        return View::make('admin.categories.tab-features', compact('category', 'featuresList', 'unitGroupList'));
    }

    public function getCreate()
    {

        $category = new Category;

        return View::make('admin.categories.new', compact('category'));

    }

    public function postStore()
    {

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/categories/new')
                ->withErrors($validator)
                ->withInput();

        } else {

            $category = new Category;

            $category->name_az = Input::get('name_az');
            $category->name_ru = Input::get('name_ru');

            // TODO make dynamic
            $category->sector_id = 1;

            $category->description_az = Input::get('description_az');
            $category->description_ru = Input::get('description_ru');
            $category->tags = Input::get('tags');
            $category->visible = Input::has('visible');
            $category->is_banner = Input::has('is_banner');
            $category->symbol = Input::get('symbol');

            $category->save();

            if($category->parent_id != Input::get('parent_id'))
            {
                $category->makeChildOf(Input::get('parent_id'));
            }

            // redirect
            Session::flash('message', 'success|Kateqoriya müvəffəqiyyətlə yaddaşa verildi!');
            return Redirect::to('admin/category/'.$category->id);

        }

    }

    public function getRemove($id)
    {

        $category = Category::find($id);

        return View::make('admin.categories.tab-remove', compact('category'));
    }

    public function postRemove($id)
    {

        $category = Category::find($id);

        if (!$category)
        {

            Session::flash('message', 'danger|Əməliyyat mümkün deyil');

            return Redirect::to('admin/categories');

        } else {

            // TODO not working. Fix
            $category->delete();

            // redirect
            Session::flash('message', 'success|Kateqoriya müvəffəqiyyətlə silindi!');
            return Redirect::to('admin/categories');

        }

    }

    public function postUpdateGeneral($id)
    {

        $category = Category::find($id);

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/category/'.$id)
                ->withErrors($validator)
                ->withInput();

        } else {

            $category->name_az = Input::get('name_az');
            $category->name_ru = Input::get('name_ru');
            $category->description_az = Input::get('description_az');
            $category->description_ru = Input::get('description_ru');
            $category->tags = Input::get('tags');
            $category->visible = Input::has('visible');
            $category->is_banner = Input::has('is_banner');
            $category->color = Input::get('color');
            $category->fa_icon = Input::get('fa_icon');
            $category->symbol = Input::get('symbol');

            if($category->parent_id != Input::get('parent_id'))
            {
                $category->makeChildOf(Input::get('parent_id'));
            }

            $category->visible = Input::get('visible');
            $category->save();

            // redirect
            Session::flash('message', 'success|Məhsul müvəffəqiyyətlə yeniləndi!');
            return Redirect::to('admin/category/'.$id);

        }

    }

    public function postAddFeature($id)
    {
        $category = Category::find($id);

        if(Input::has('feature_id')) {
            $category->features()->attach(Input::get('feature_id'));
        }

        return Redirect::to('admin/category/'.$id.'/features');
    }

    public function postAddNewFeature($id)
    {
        $category = Category::find($id);

        $validator = Validator::make(Input::all(), array());

        if($validator->fails()) {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/category/'.$id.'/features')
                ->withErrors($validator)
                ->withInput();

        } else {

            $feature = new Feature;
            $feature->name_az = Input::get('name_az');
            $feature->name_ru = Input::get('name_ru');
            $feature->descriptor = Input::get('descriptor');

            if (Input::has('unit_group_id'))
            {
                $feature->unit_group_id = Input::get('unit_group_id');
            }

            if (Input::has('is_boolean'))
            {
                $feature->is_boolean = Input::get('is_boolean');
            }

            $feature->save();

            $category->features()->attach($feature->id);
        }

        return Redirect::to('admin/category/'.$id.'/features');
    }

    public function postRemoveFeature($id, $feature_id) {

        $category = Category::find($id);

        $category->features()->detach($feature_id);

        Session::flash('message', 'success|Xüsusiyyət müvəffəqiyyətlə silindi');

        return Redirect::to('admin/category/'.$id.'/features');
    }

} 