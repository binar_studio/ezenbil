<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 11/11/14
 * Time: 12:52 AM
 */

namespace Admin;

use AttributeGroup;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use DB;
use Config;
use Product;
use Discount;

class DiscountsController extends \BaseController {

    public function getIndex()
    {

        $discounts = Discount::paginate(20);

        return View::make('admin.discounts.index')
            ->with('discounts', $discounts);
    }

    public function getCreate() {

        if (Input::has('type') && in_array(Input::get('type'), array_keys(Config::get('enums.discount_type'))) ) {

            $type = Input::get('type');

            $discount = new Discount;

            return View::make("admin.discounts.new_form")
                ->with(compact('discount', 'type'));

        } else {
            return View::make('admin.discounts.new');
        }
    }


    public function postStore()
    {

        $type = Input::get('type');

        // TODO validate seperately by types

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to("admin/discount/new?type=$type")
                ->withErrors($validator)
                ->withInput();

        } else {

            $discount = new Discount();

            $discount->title = Input::get('title');
            $discount->limit = Input::get('limit');
            $discount->starts = Input::get('starts');
            $discount->ends = Input::get('ends');
            $discount->type = Input::get('type');

            if ($discount->type == 'percentage') {
                $discount->percentage = Input::get('percentage');
            } else if ($discount->type == 'nplus1') {
                $discount->nplusm_n = Input::get('nplusm_n');
                $discount->nplusm_m = Input::get('nplusm_m');
            }

            $discount->save();

            // redirect
            Session::flash('message', 'success|Endirim yaddaşa verildi!');
            return Redirect::to('admin/discount/'.$discount->id);

        }

    }

    public function getViewGeneral($id)
    {

        $discount = Discount::find($id);

        return View::make('admin.discounts.tab-general', compact('discount'));
    }

    public function getViewProducts($id)
    {

        $discount = Discount::find($id);

        return View::make('admin.discounts.tab-products', compact('discount'));
    }

    public function getRemove($id)
    {

        $discount = Discount::find($id);

        return View::make('admin.discounts.tab-remove', compact('discount'));
    }

    public function postRemove($id)
    {

        $discount = Discount::find($id);

        if (! $discount)
        {

            Session::flash('message', 'danger|Əməliyyat mümkün deyil');

            return Redirect::to('admin/discounts');

        } else {

            DB::table('products')
                ->where('discount_id', $id)
                ->update(array('discount_id' => null));

            $discount->delete();

            // redirect
            Session::flash('message', 'success|Endirim uğurla silindi!');
            return Redirect::to('admin/discounts');

        }

    }

    public function postUpdateGeneral($id)
    {

        $discount = Discount::find($id);

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/product/'.$id)
                ->withErrors($validator)
                ->withInput();

        } else {

            $discount->title = Input::get('title');
            $discount->limit = Input::get('limit');
            $discount->starts = Input::get('starts');
            $discount->ends = Input::get('ends');

            if ($discount->type == 'percentage') {
                $discount->percentage = Input::get('percentage');
            } else if ($discount->type == 'nplus1') {
                $discount->nplusm_n = Input::get('nplusm_n');
                $discount->nplusm_m = Input::get('nplusm_m');
            }

            $discount->save();

            // redirect
            Session::flash('message', 'success|Məhsulla bağlı dəyişiklik yaddaşa verildi!');
            return Redirect::to('admin/discount/'.$id);

        }

    }

    public function getAjaxProductSearch($id) {

        $search = Input::get('search');

        $products = Product::where('name_az', 'LIKE', "%$search%")->orWhere('name_ru', 'LIKE', "%$search%")->get();

        return View::make("admin.discounts.ajax-form-add-products")
            ->with(compact('products', 'id'));

    }

    public function postAddProducts($id) {

        $keys = array_keys(Input::all());
        $ids = array_reduce ($keys, array($this, '_product_matcher'), array ());

        DB::table('products')
            ->whereIn('id', $ids)
            ->update(array('discount_id' => $id));

        Session::flash('message', 'success|Məhsullar yaddaşa verildi!');
        return Redirect::to('admin/discount/'.$id.'/products');
    }

    public function postUpdateProducts($id)
    {

        $discount = Discount::find($id);

        // TODO

        // redirect
        Session::flash('message', 'success|Məhsullar yaddaşa verildi!');
        return Redirect::to('admin/discount/'.$id.'/products');
    }

    static function _product_matcher ($m, $str) {
        if (preg_match ('/^product-(\d+)$/', $str, $matches))
            $m[] = $matches[1];

        return $m;
    }

}