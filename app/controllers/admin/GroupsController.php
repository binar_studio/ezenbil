<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 11/11/14
 * Time: 12:52 AM
 */

namespace Admin;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Sentry;
use User;
use Group;
use Groups;

class GroupsController extends \BaseController {

    public function getIndex()
    {

        $groups = Group::paginate(20);

        return View::make('admin.groups.index')
            ->with('groups', $groups);
    }

    public function getCreate() {

        $group = new Group;

        return View::make('admin.groups.new')
            ->with(compact('group'));
    }

    public function postStore()
    {

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/groups/new')
                ->withErrors($validator)
                ->withInput();

        } else {

            $group = new Group;

            $group->name = Input::get('name');
            $group->save();

            // redirect
            Session::flash('message', 'success|Qrup yaddaşa verildi!');
            return Redirect::to('admin/group/'.$group->id);

        }

    }

    public function getViewGeneral($id)
    {

        $group = Group::find($id);

        return View::make('admin.groups.tab-general', compact('group'));
    }

    public function getRemove($id)
    {

        $group = Sentry::findGroupById($id);

        return View::make('admin.groups.tab-remove', compact('group'));
    }

    public function postRemove($id)
    {

        $group = Sentry::findGroupById($id);

        if (! $group)
        {

            Session::flash('message', 'danger|Əməliyyat mümkün deyil');

            return Redirect::to('admin/groups');

        } else {

            $group->delete();

            // redirect
            Session::flash('message', 'success|Qrup uğurla silindi!');
            return Redirect::to('admin/groups');

        }

    }

    public function postUpdateGeneral($id)
    {

        $group = Sentry::findGroupById($id);

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/group/'.$id)
                ->withErrors($validator)
                ->withInput();

        } else {

            try {
                $group->name = Input::get('name');
                $group->save();
            }
            catch (Cartalyst\Sentry\Groups\GroupExistsException $e)
            {
                Session::flash('message', 'danger|Xəta baş verdi. Bu ad ilə başqa qrup artıq mövcuddur');
            }


            // redirect
            Session::flash('message', 'success|Qrupla bağlı dəyişiklik yaddaşa verildi!');
            return Redirect::to('admin/group/'.$id);

        }

    }

} 