<?php

namespace Admin;

use Illuminate\Support\Facades\View;

class HomeController extends AdminController {

	public function getIndex()
	{
		return View::make('admin/blank');
	}

}
