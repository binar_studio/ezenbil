<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 11/11/14
 * Time: 12:52 AM
 */

namespace Admin;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Order;
use LineItem;
use Config;

class OrdersController extends \BaseController {

    public function getIndex()
    {

        $orders = null;

        if (Input::has('user_id')) {
            $orders = Order::where('user_id', '=', Input::get('user_id'))->paginate(20);
        } else {
            $orders = Order::paginate(20);
        }

        return View::make('admin.orders.index')
            ->with('orders', $orders);
    }

    public function getCreate() {

        $order = new Order;
        $statusList = Config::get('enums.order_status');

        return View::make('admin.orders.new')
            ->with(compact('order', 'statusList'));
    }

    public function postStore()
    {

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/orders/new')
                ->withErrors($validator)
                ->withInput();

        } else {

            $order = new Order;

            $order->user_id = Input::get('user_id');
            $order->status = Input::get('status');
            $order->save();

            // redirect
            Session::flash('message', 'success|Sifariş/səbət yaddaşa verildi!');
            return Redirect::to('admin/order/'.$order->id);

        }

    }

    public function getViewGeneral($id)
    {

        $order = Order::find($id);
        $statusList = Config::get('enums.order_status');

        return View::make('admin.orders.tab-general', compact('order', 'statusList'));
    }

    public function getViewList($id)
    {

        $order = Order::find($id);

        return View::make('admin.orders.tab-list', compact('order'));
    }

    public function getRemove($id)
    {

        $order = Order::find($id);

        return View::make('admin.orders.tab-remove', compact('order'));
    }

    public function postRemove($id)
    {

        $order = Order::find($id);

        if (! $order)
        {

            Session::flash('message', 'danger|Əməliyyat mümkün deyil');

            return Redirect::to('admin/orders');

        } else {

            $order->delete();

            // redirect
            Session::flash('message', 'success|Sifariş/səbət uğurla silindi!');
            return Redirect::to('admin/orders');

        }

    }

    public function postUpdateGeneral($id)
    {

        $order = Order::find($id);

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/order/'.$id)
                ->withErrors($validator)
                ->withInput();

        } else {

            $order->user_id = Input::get('user_id');
            $order->status = Input::get('status');
            $order->save();

            // redirect
            Session::flash('message', 'success|Sifarişlə bağlı dəyişiklik yaddaşa verildi!');
            return Redirect::to('admin/order/'.$id);

        }

    }

    public function removeLineItem() {
        // TODO
    }

    public function addLineItem() {
        // TODO
    }

} 