<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 11/11/14
 * Time: 12:52 AM
 */

namespace Admin;

use AttributeGroup;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Product;
use Category;
use Unit;
use Attribute;
use Photo;
use FeatureOption;
use Quantity;

class ProductsController extends \BaseController {

    public function getIndex()
    {

        $products = Product::paginate(20);

        return View::make('admin.products.index')
            ->with('products', $products);
    }

    public function getCreate() {

        $product = new Product;
        $categoryList = Category::lists('name_az', 'id');

        return View::make('admin.products.new')
            ->with(compact('product', 'categoryList'));
    }

    public function postStore()
    {

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/products/new')
                ->withErrors($validator)
                ->withInput();

        } else {

            $product = new Product;

            $product->name_az = Input::get('name_az');
            $product->name_ru = Input::get('name_ru');
            $product->category_id = Input::get('category_id');
            $product->save();

            // TODO Add features from categories
            $category = Category::find(Input::get('category_id'));

            foreach($category->features as $feature)
            {

                if (isset($feature->unit_group_id))
                {
                    $unit = Unit::whereUnitGroupId($feature->unit_group_id)->first();
                    $product->features()->attach($feature->id, array('unit_id' => $unit->id));
                } else {
                    $product->features()->attach($feature->id);
                }

                /*$unit = Unit::whereUnitGroupId($feature->unit_group_id)->first();
                $product->features()->attach($feature->id, array('unit_id' => $unit->id));*/
            }

            // redirect
            Session::flash('message', 'success|Məhsul yaddaşa verildi!');
            return Redirect::to('admin/product/'.$product->id);

        }

    }

    public function getViewGeneral($id)
    {

        $product = Product::find($id);
        $categoryList = Category::lists('name_az', 'id');

        return View::make('admin.products.tab-general', compact('product', 'categoryList'));
    }

    public function getViewFeatures($id)
    {

        $product = Product::find($id);

        return View::make('admin.products.tab-features', compact('product'));
    }

    public function getViewAttributes($id)
    {

        $product = Product::find($id);

        $attrGroups = AttributeGroup::get()->lists('global_name', 'id');

        return View::make('admin.products.tab-attributes', compact('product', 'attrGroups'));
    }

    public function getViewPhotos($id)
    {

        $product = Product::find($id);

        return View::make('admin.products.tab-photos', compact('product'));
    }

    public function getRemove($id)
    {

        $product = Product::find($id);

        return View::make('admin.products.tab-remove', compact('product'));
    }

    public function postRemove($id)
    {

        $product = Product::find($id);

        if (! $product)
        {

            Session::flash('message', 'danger|Əməliyyat mümkün deyil');

            return Redirect::to('admin/products');

        } else {

            $product->delete();

            // redirect
            Session::flash('message', 'success|Məhsul uğurla silindi!');
            return Redirect::to('admin/products');

        }

    }

    public function postUpdateGeneral($id)
    {

        $product = Product::find($id);

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/product/'.$id)
                ->withErrors($validator)
                ->withInput();

        } else {

            $product->name_az = Input::get('name_az');
            $product->name_ru = Input::get('name_ru');
            $product->description_az = Input::get('description_az');
            $product->description_ru = Input::get('description_ru');
            $product->category_id = Input::get('category_id');
            $product->visible = Input::get('visible');
            $product->price = Input::get('price');
            $product->tags = Input::get('tags');
            $product->save();

            // redirect
            Session::flash('message', 'success|Məhsulla bağlı dəyişiklik yaddaşa verildi!');
            return Redirect::to('admin/product/'.$id);

        }

    }

    public function postUpdateFeatures($id)
    {

        $product = Product::find($id);

        foreach($product->features as $f) {

            if($f->has_options) {

                if(Input::has('feature-'.$f->id.'-option-new-name_az')) {

                    $name_az = Input::get('feature-'.$f->id.'-option-new-name_az');
                    $name_ru = Input::has('feature-'.$f->id.'-option-new-name_ru') ? Input::get('feature-'.$f->id.'-option-new-name_ru') : $name_az;

                    $option = $f->options()->save(new FeatureOption(array('name_az' => $name_az, 'name_ru' => $name_ru)));

                    $f->pivot->option_id = $option->id;

                } else if (Input::has('feature-'.$f->id.'-option')) {

                    $f->pivot->option_id = Input::get('feature-'.$f->id.'-option');

                }

            } else {

                if ( $f->is_boolean ) {

                    if ( Input::has('feature-'.$f->id) ) {
                        $f->pivot->value = true;
                    } else {
                        $f->pivot->value = false;
                    }

                } else {

                    if ( Input::has('feature-'.$f->id) ) {

                        $f->pivot->value = Input::get('feature-'.$f->id);

                        if(Input::has('feature-'.$f->id.'-unit')) {
                            $f->pivot->unit_id = Input::get('feature-'.$f->id.'-unit');
                        }

                        $f->pivot->unit_id = Input::get('feature-'.$f->id.'-unit');

                    }
                }

            }

            $f->pivot->save();
            $f->save();

        }

        $product->save();

        // redirect
        //Session::flash('message', 'success|Məhsul xüsusiyyətləri yaddaşa verildi!');
        return Redirect::to('admin/product/'.$id.'/features');
    }

    public function getAddAttributes($id, $group_id)
    {
        $attributes = AttributeGroup::find($group_id)->attributes;
        return View::make('admin.products.ajax-form-add-attr', compact('attributes', 'id', 'group_id'));
    }

    public function postAddAttributes($id, $group_id)
    {
        $attributes = AttributeGroup::find($group_id)->attributes;
        $product = Product::find($id);

        foreach($attributes as $attr)
        {
            if (Input::has('attr-'.$attr->id))
            {
                $product->attributes()->attach($attr->id);
            }
        }

        return Redirect::to('admin/product/'.$id.'/attributes');
    }

    public function getAddNewAttribute($id, $group_id)
    {

        $attr = new Attribute;
        return View::make('admin.products.ajax-form-add-new-attr', compact('attr', 'id', 'group_id'));
    }

    public function postAddNewAttribute($id, $group_id)
    {
        $product = Product::find($id);


        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/product/'.$id.'/attributes')
                ->withErrors($validator)
                ->withInput();

        } else {

            $attr = new Attribute;
            $attr->name_az = Input::get('name_az');
            $attr->name_ru = Input::get('name_ru');
            $attr->attribute_group_id = $group_id;
            $attr->color = Input::get('color');
            $attr->save();

            $product->attributes()->attach($attr->id);

            // redirect
            Session::flash('message', 'success|Məhsulla bağlı dəyişiklik yaddaşa verildi!');

        }

        return Redirect::to('admin/product/'.$id.'/attributes');
    }

    public function postUpdateAttributes($id)
    {
        $product = Product::find($id);

        if (Input::has("save")) {

            foreach($product->attributes as $a) {
                if (Input::has('attr-'.$a->id.'-price-impact') ) {

                    $a->pivot->price_impact = Input::get('attr-'.$a->id.'-price-impact');
                    $a->pivot->save();
                }
            }

            // redirect
            Session::flash('message', 'success|Məhsulun atributları yaddaşa verildi!');

        } else if (Input::has("remove")) {

            foreach($product->attributes as $a) {
                if (Input::has('attr-'.$a->id.'-check') ) {

                    $product->attributes()->detach($a->id);
                }
            }

            // redirect
            Session::flash('message', 'success|Məhsulun atributları yaddaşa verildi!');

        }

        // redirect
        return Redirect::to('admin/product/'.$id.'/attributes');
    }

    public function postUploadPhoto($product_id)
    {

        $file = Input::file('file');
        $destinationPath = '/uploads/photos/'.$product_id;
        // If the uploads fail due to file system, you can try doing public_path().'/uploads'

        $filename = str_random(12);
        //$filename = $file->getClientOriginalName();
        //$extension =$file->getClientOriginalExtension();
        $upload_success = Input::file('file')->move(public_path().$destinationPath, $filename);

        if( $upload_success ) {

            $photo = new Photo;

            $photo->product_id = $product_id;
            $photo->filename = $destinationPath.'/'.$filename;
            $photo->save();

            return Response::json('success', 200);
        } else {
            return Response::json('error', 400);
        }
    }

    public function postSetMainPhoto($id, $photo_id)
    {
        $product = Product::find($id);
        $photo = Photo::find($photo_id);

        $message = array();

        if ($product && $photo && $photo->product_id = $product->id) {

            $is_ok = true;
            // TODO size check

            if($is_ok) {

                $product->main_photo_id = $photo->id;
                $product->save();

                $message = array('status' => 'ok', 'message' => 'Əsas şəkil dəyişdirildi');

            } else {
                $message = array('status' => 'error', 'message' => 'Bu şəkil əsas şəkil seçilə bilməz');

            }

        } else {

            $message = array('status' => 'error', 'message' => 'Əməliyyat mümkün deyil');

        }

        return Response::json($message);
    }

    public function postRemovePhoto($id, $photo_id)
    {
        $product = Product::find($id);
        $photo = Photo::find($photo_id);

        $message = array();

        if ($product && $photo && $photo->product_id = $product->id) {

            $wasMainPhoto = $product->main_photo_id == $photo_id;

            if ($wasMainPhoto) {
                $product->main_photo_id = null;
                $product->save();
            }

            $success = $photo->delete();

            if($success) {
                $message = array('status' => 'ok', 'message' => 'Şəkil silindi');
            } else {
                $message = array('status' => 'error', 'message' => 'Şəkil silinə bilmədi');
            }

        } else {

            $message = array('status' => 'error', 'message' => 'Əməliyyat mümkün deyil');

        }

        return Response::json($message);
    }

    public function getQuantities($id)
    {
        $quantities = Quantity::where('product_id', $id)->get();
        return View::make('admin.products.tab-quantities', compact('quantities', 'id'));
    }

    public function postCheckQuantities($id)
    {

        $product = Product::find($id);

        $ok = true;

        if ($product->attributes->isEmpty()) {

            $q = Quantity::where('product_id', $id)->where('attribute_id', null)->first();

            if (!$q) {
                $q = new Quantity;
                $q->product_id = $id;

                $success = $q->save();
                $ok &= $success;
            }

        } else {

            // Remove null attr quantity if exists
            $q = Quantity::where('product_id', $id)->where('attribute_id', null)->first();
            if ($q) {
                $success = $q->delete();
                $ok &= $success;
            }

            foreach ($product->attributes as $attr)
            {

                $q = Quantity::where('product_id', $id)->where('attribute_id', $attr->id)->first();

                if (!$q) {
                    $q = new Quantity;
                    $q->product_id = $id;
                    $q->attribute_id = $attr->id;

                    $success = $q->save();
                    $ok &= $success;
                }
            }
        }

        if ($ok) {
            Session::flash('message', 'success|Məlumatlar uğurla yoxlanıldı (və ya yeniləndi)');
        } else {
            Session::flash('message', 'error|Məlumatların yoxlanılmasında (və ya yenilənilməsində) problemlər yarandı');
        }

        return Redirect::to('admin/product/'.$id.'/quantities');
    }

} 