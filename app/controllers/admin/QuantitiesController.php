<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 14/02/15
 * Time: 08:05 PM
 */

namespace Admin;

use AttributeGroup;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Product;
use Attribute;
use Quantity;

class QuantitiesController extends \BaseController {

    public function getIndex()
    {

        $quantities = Quantity::paginate(20);

        return View::make('admin.quantities.index')
            ->with('quantities', $quantities);
    }

    public function getView($id)
    {

        $quantity = Quantity::find($id);

        return View::make('admin.quantities.tab-general', compact('quantity'));
    }

    public function postUpdate($id)
    {

        $quantity = Quantity::find($id);

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/quantity/'.$id)
                ->withErrors($validator)
                ->withInput();

        } else {

            $quantity->value = Input::get('value');
            $quantity->save();

            // redirect
            Session::flash('message', 'success|Məhsulla bağlı dəyişiklik yaddaşa verildi!');
            return Redirect::to('admin/quantity/'.$id);

        }

    }

    public function getRemove($id)
    {

        $quantity = Quantity::find($id);

        return View::make('admin.quantities.tab-remove', compact('quantity'));
    }

    public function postRemove($id)
    {

        $quantity = Quantity::find($id);

        if (! $quantity)
        {

            Session::flash('message', 'danger|Əməliyyat mümkün deyil');
            return Redirect::to('admin/quantities');

        } else {

            $quantity->delete();

            // redirect
            Session::flash('message', 'success|Məlumat uğurla silindi!');
            return Redirect::to('admin/quantities');

        }

    }

} 