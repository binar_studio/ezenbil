<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 11/11/14
 * Time: 12:52 AM
 */

namespace Admin;

use AttributeGroup;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Illuminate\Database\QueryException;
use Product;
use Category;
use Unit;
use UnitGroup;
use Attribute;
use Photo;

class UnitsController extends \BaseController {

    public function getIndex()
    {

        $unitGroups = UnitGroup::paginate(20);

        return View::make('admin.unitgroups.index')
            ->with(compact('unitGroups'));
    }

    public function getCreateGroup() {

        $unitGroup = new UnitGroup();

        return View::make('admin.unitgroups.new')
            ->with(compact('unitGroup'));
    }

    public function postStoreGroup()
    {

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/unitgroups/new')
                ->withErrors($validator)
                ->withInput();

        } else {

            $unitGroup = new UnitGroup();

            $unitGroup->name = Input::get('name');
            $unitGroup->save();

            // redirect
            Session::flash('message', 'success|Vahidlər qrupu yaddaşa verildi');
            return Redirect::to('admin/unitgroup/'.$unitGroup->id);

        }

    }

    public function getViewGroupGeneral($id)
    {

        $unitGroup = UnitGroup::find($id);

        return View::make('admin.unitgroups.tab-general', compact('unitGroup'));
    }

    public function postUpdateGroupGeneral($id)
    {

        $unitGroup = UnitGroup::find($id);

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/unitgroup/'.$id)
                ->withErrors($validator)
                ->withInput();

        } else {

            $unitGroup->name = Input::get('name');
            $unitGroup->save();

            // redirect
            Session::flash('message', 'success|Vahidlər qrupu ilə bağlı dəyişiklik yaddaşa verildi');
            return Redirect::to('admin/unitgroup/'.$id);

        }

    }

    public function getViewGroupUnits($id)
    {

        $unitGroup = UnitGroup::find($id);

        return View::make('admin.unitgroups.tab-units', compact('unitGroup'));
    }

    public function getRemoveGroup($id)
    {

        $unitGroup = UnitGroup::find($id);

        return View::make('admin.unitgroups.tab-remove', compact('unitGroup'));
    }

    public function postRemoveGroup($id)
    {

        $unitGroup = UnitGroup::find($id);

        if (! $unitGroup)
        {

            Session::flash('message', 'danger|Əməliyyat mümkün deyil');

            return Redirect::to('admin/unitgroups');

        } else {

            $unitGroup->delete();

            // redirect
            Session::flash('message', 'success|Vahidlər qrupu uğurla silindi');
            return Redirect::to('admin/unitgroups');

        }

    }
    public function postGroupAddUnit($id) {

        $unitGroup = UnitGroup::find($id);

        $validator = Validator::make(Input::all(), array());

        if($validator->fails()) {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/unitgroup/'.$id.'/units')
                ->withErrors($validator)
                ->withInput();

        } else {

            $unit = new Unit;
            $unit->name_az = Input::get('name_az');
            $unit->name_ru = Input::get('name_ru');
            $unit->unit_group_id = $id;

            $unit->save();
        }

        return Redirect::to('admin/unitgroup/'.$id.'/units');

    }

    public function postGroupRemoveUnit($id, $unit_id)
    {

        $unit = Unit::find($unit_id);

        if (! $unit)
        {

            Session::flash('message', 'danger|Əməliyyat mümkün deyil. Belə nömrəli vahid mövcud deyil');
            return Redirect::to('admin/unitgroup/'.$id.'/units');

        } else {

            try {
                $unit->delete();

            } catch(QueryException $ex) {

                Session::flash('message', 'danger|Əməliyyat mümkün deyil. Məhsullar tərəfindən istifadə olunan vahidi silmək olmaz');
                return Redirect::to('admin/unitgroup/'.$id.'/units');
            }



            // redirect
            Session::flash('message', 'success|Vahid uğurla silindi!');
            return Redirect::to('admin/unitgroup/'.$id.'/units');

        }

    }

} 