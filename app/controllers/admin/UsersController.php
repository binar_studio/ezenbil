<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 11/11/14
 * Time: 12:52 AM
 */

namespace Admin;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Sentry;
use User;
use Order;
use Group;

class UsersController extends \BaseController {

    public function getIndex()
    {

        $users = User::paginate(20);

        return View::make('admin.users.index')
            ->with('users', $users);
    }

    public function getCreate() {

        $user = new User;
        $groupList = Group::lists('name', 'id');

        return View::make('admin.users.new')
            ->with(compact('user', 'groupList'));
    }

    public function postStore()
    {

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/users/new')
                ->withErrors($validator)
                ->withInput();

        } else {

            $user = new User;

            $user->email = Input::get('email');
            $user->first_name = Input::get('first_name');
            $user->last_name = Input::get('last_name');
            $user->save();

            // redirect
            Session::flash('message', 'success|Sifariş/səbət yaddaşa verildi!');
            return Redirect::to('admin/user/'.$user->id);

        }

    }

    public function getViewGeneral($id)
    {

        $user = User::find($id);
        $groupList = Group::lists('name', 'id');

        return View::make('admin.users.tab-general', compact('user', 'groupList'));
    }

    public function getRemove($id)
    {

        $user = User::find($id);

        return View::make('admin.users.tab-remove', compact('user'));
    }

    public function postRemove($id)
    {

        $user = Sentry::findUserById($id);

        if (! $user)
        {

            Session::flash('message', 'danger|Əməliyyat mümkün deyil');

            return Redirect::to('admin/users');

        } else {

            $user->delete();

            // redirect
            Session::flash('message', 'success|İstifadəçi uğurla silindi!');
            return Redirect::to('admin/users');

        }

    }

    public function postUpdateGeneral($id)
    {

        $user = Sentry::findUserById($id);

        $validator = Validator::make(Input::all(), array());

        if ($validator->fails())
        {

            Session::flash('message', 'danger|Xəta baş verdi. Səhvləri düzəldin');

            return Redirect::to('admin/user/'.$id)
                ->withErrors($validator)
                ->withInput();

        } else {

            try {
                $user->email = Input::get('email');
                $user->first_name = Input::get('first_name');
                $user->last_name = Input::get('last_name');
                $user->save();

                if (Input::has('group_id')) {

                    $group_id = Input::get('group_id');

                    $group = Sentry::findGroupById($group_id);
                    $user->addGroup($group);
                }
            }
            catch (Cartalyst\Sentry\Users\UserExistsException $e)
            {
                Session::flash('message', 'danger|Xəta baş verdi. Bu email ilə başqa istifadəçi artıq mövcuddur');
            }


            // redirect
            Session::flash('message', 'success|Sifarişlə bağlı dəyişiklik yaddaşa verildi!');
            return Redirect::to('admin/user/'.$id);

        }

    }

} 