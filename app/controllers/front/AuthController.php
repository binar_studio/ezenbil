<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 2/2/15
 * Time: 4:59 PM
 */

namespace Front;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Session;
use Sentry;
use User;
use Lang;

class AuthController extends FrontController {

    public function getSignIn() {

        return View::make('front.auth.signin');
    }

    public function postSignIn() {

        $input = Input::only('email', 'password');
        $validator = Validator::make(Input::all(), array('email' => 'required|email', 'password' => 'required'));
        if ($validator->fails()) {
            $messages = $validator->messages();

            Session::flash('message', 'danger|'.Lang::get('auth.please_fix_errors'));

            return Redirect::to('/signin')
                ->withErrors($validator)
                ->withInput();

        }

        try
        {
            Sentry::authenticate($input, Input::has('remember'));
        }
        catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
        {
            Session::flash('message', 'danger|'.Lang::get('auth.invalid_creadientals'));
            return Redirect::back()->withInput();
        }
        catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e)
        {
            Session::flash('message', 'danger|'.Lang::get('auth.user_not_activated'));
            return Redirect::back()->withInput();
        }
        // Logged in successfully - redirect based on type of user
        $user = Sentry::getUser();

        $customers = Sentry::findGroupByName('Customers');

        if ($user->inGroup($customers))
            return Redirect::intended('/');
        else
            return Redirect::intended('/admin');

    }

    public function getSignUp() {
        return View::make('front.auth.signup');
    }

    public function postSignUp() {

        $input = Input::only('email', 'password', 'password_confirmation', 'first_name', 'last_name');

        $validator = Validator::make(Input::all(), User::$rules);

        if ($validator->fails()) {

            $messages = $validator->messages();

            Session::flash('message', 'danger|'.Lang::get('auth.please_fix_errors'));


            return Redirect::to('/signup')
                ->withErrors($validator)
                ->withInput();

        } else {

            try
            {
                $user = Sentry::getUserProvider()->create(array(
                    'email' => $input['email'],
                    'password' => $input['password'],
                    'first_name' => $input['first_name'],
                    'last_name' => $input['last_name'],
                    'activated' => 1,
                ));

                // Find the group using the group name
                $customersGroup = Sentry::findGroupByName('Customers');
                // Assign the group to the user
                $user->addGroup($customersGroup);

                Session::flash('message', 'success|'.Lang::get('auth.user_successfully_created'));
                return Redirect::to('signin');
            }
            catch(\Cartalyst\Sentry\Users\UserExistsException $e)
            {
                Session::flash('message', 'danger|'.Lang::get('auth.user_with_this_email_already_exists'));
                return Redirect::to('signin');
            }


        }

    }

    public function getSignOut() {

        Sentry::logout();
        return Redirect::to('/');
    }

} 