<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 2/2/15
 * Time: 11:03 PM
 */

namespace Front;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Response;
use Cartalyst\Sentry\Facades\Laravel\Sentry;
use Lang;
use User;
use Product;
use ProductLineItem;
use LineItem;
use Order;

class CartController extends FrontController {

    public function getCart()
    {
        return View::make('front.cart.cart');
    }

    public function getAjaxCartContainer()
    {
        $user = Sentry::getUser();
        $user = User::find($user->id);

        $sector_id = 1; // TODO get from param

        $cart = Order::getOrCreateCartForUser($user, $sector_id);

        $cartDetails = Order::getCartDetails($cart);

        return View::make('front.cart.ajax_cart_container')
            ->with('details', $cartDetails);
    }

    public function getAjaxCart()
    {
        $user = Sentry::getUser();
        $user = User::find($user->id);

        $sector_id = 1; // TODO get from param

        $cart = Order::getOrCreateCartForUser($user, $sector_id);

        $cartDetails = Order::getCartDetails($cart);

        return View::make('front.cart.ajax_cart')
            ->with('details', $cartDetails);
    }

    public function postAjaxAddToCart()
    {

        $input = Input::only('product_id', 'attribute_id', 'quantity');

        $user = Sentry::getUser();

        $user = User::find($user->id);

        $cart = Order::getOrCreateCartForUser($user);

        // TODO
        $validator = Validator::make($input, array());

        $response = array();

        if ($validator->fails()) {
            // TODO
        } else {

            $product = Product::find($input['product_id']);

            if (! is_null($product)) {

                $attr = $product->attributes()->where('attribute_id', '=', $input['attribute_id'])->first();

                $hasAtributes = $product->attributes->count() > 0;

                if (!$hasAtributes || ( $hasAtributes && !is_null($attr) )) {

                    $productLineItem = ProductLineItem::create($input);

                    $lineItem = $cart->lineItems()->save(new LineItem());

                    $success = $productLineItem->lineItem()->save($lineItem);

                    if ($success) {
                        $response = array('status' => 'ok', 'message' => Lang::get('cart.product_added_to_cart'));
                    } else {
                        $response = array('status' => 'error', 'message' => Lang::get('cart.product_couldnt_added_to_cart'));
                    }

                } else {
                    $response = array('status' => 'error', 'message' => Lang::get('cart.attribute_not_selected'));
                }

            } else {

                $response = array('status' => 'error', 'message' => Lang::get('cart.product_not_selected'));
            }
        }

        return Response::json($response);
    }

    public function postAjaxChangeQuantity()
    {
        $input = Input::only('product_line_item_id', 'quantity');

        $user = Sentry::getUser();

        // TODO
        $validator = Validator::make($input, array());

        $response = array();

        if ($validator->fails()) {
            // TODO
        } else {

            $productLineItem = ProductLineItem::find($input['product_line_item_id']);

            if ($productLineItem->lineItem->order->user->id == $user->id) {

                if ($input['quantity'] == 0) {
                    $success = $productLineItem->delete();

                    if($success) {
                        $response = array('status' => 'ok', 'message' => Lang::get('cart.item_deleted'));
                    } else {
                        $response = array('status' => 'error', 'message' => Lang::get('cart.icant_delete_item'));
                    }

                } else {
                    $productLineItem->quantity = $input['quantity'];

                    $success = $productLineItem->save();

                    if($success) {
                        $response = array('status' => 'ok', 'message' => Lang::get('cart.quantity_changed'));
                    }
                }

            } else {
                $response = array('status' => 'error', 'message' => Lang::get('cart.not_authorized'));
            }

        }

        return Response::json($response);
    }

}