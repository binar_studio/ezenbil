<?php

namespace Front;

use Category;
use Product;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\App;

class CatalogController extends FrontController
{

    public function electronics()
    {

        $technoRoots = Category::roots()->whereSectorId(1)->rememberForever('technoroots')->get();

        $products = Product::getProductsForSectionQuery(1)->get();

        $best_sales = $products->sortByDesc('price')->take(16);
        $new_items = $products->sortByDesc('updated_at')->take(4);

        return View::make('front.catalogs.electronics')
            ->with("techno_roots", $technoRoots)
            ->with("best_sales", $best_sales)
            ->with("new_items", $new_items);

    }

    public function electronicsCategory($category_slug)
    {
        $category = Category::where('id', '=', $category_slug )->first();
        if(!$category) {
            App::abort(404);
        }
        $categoryRoots = $category::roots()->whereSectorId(1)->rememberForever('technoroots')->get();

        $products = Product::getProductsForCategoryQuery($category->id)->paginate(9);

        $best_sales = $products->sortByDesc('price')->take(16);
        $new_items = $products->sortByDesc('updated_at')->take(8);

        $recent_products = Product::orderBy('updated_at', 'desc')->take(6)->get();
        $popular_products = Product::orderBy('updated_at', 'desc')->take(6)->get();

        return View::make('front.catalogs.electronicsCategory')
            ->with("category", $category)
            ->with("roots", $categoryRoots)
            ->with("new_items", $new_items)
            ->with("recent_products", $recent_products)
            ->with("popular_products", $popular_products);

    }

    public function getAjaxProducts($id)
    {
        $category = Category::where('id', '=', $id )->first();

        if(!$category) {
            App::abort(404);
        }

        $productsQuery = Product::getProductsForCategoryQuery($category->id);

        if (Input::has('price_range')) {

            $range = Input::get('price_range');

            $productsQuery = $productsQuery->whereBetween('price', explode(';', $range));
        }

        if (Input::has('feature.bool')) {

            $booleans = Input::get('feature.bool');

            foreach ($booleans as $key => $value)
            {
                $productsQuery = $productsQuery->whereHas('features', function($q) use ($key, $value)
                {
                    $q->where('feature_id', '=', $key)->where('value', '=', $value);
                });
            }
        }

        if (Input::has('feature.option')) {

            $options = Input::get('feature.option');

            foreach ($options as $key => $value)
            {
                $productsQuery = $productsQuery->whereHas('features', function($q) use ($key, $value)
                {
                    $q->where('feature_id', '=', $key)->where('option_id', '=', $value);
                });
            }
        }

        $products = $productsQuery->paginate(9);

        if (Input::get('page') > $products->getLastPage()) {
            App::abort(404);
        }

        $best_sales = $products->sortByDesc('price')->take(16);
        $new_items = $products->sortByDesc('updated_at')->take(8);

        $recent_products = Product::orderBy('updated_at', 'desc')->take(6)->get();
        $popular_products = Product::orderBy('updated_at', 'desc')->take(6)->get();


        return View::make('front.catalogs.ajax_products')
            ->with("category", $category)
            ->with("products", $products);

    }

}
