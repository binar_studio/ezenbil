<?php

namespace Front;

use App;
use Illuminate\Support\Facades\Input;
use Session;
use Response;
use Category, Product;
use View;


class CompareController extends FrontController
{

    public function showCompare($category)
    {

        $category = Category::find("$category");

        if ($category) {
            $compare_products = Session::get("compare.items$category->id");
            if ($compare_products != null) {
                $compare_products = Product::whereIn('id', $compare_products)
                    ->get();
            } else {
                $compare_products = [];
            }
        } else {
            App::abort(404);
        }

        return View::make("front.compare")
            ->with("compare_products", $compare_products)
            ->with('category', $category);
    }

    public function postAjaxAddToCompare()
    {
        $product_id = Input::only('product_id');

        $product = Product::find($product_id)->first();

        if ($product) {
            $compare_items = Session::get("compare.items$product->category_id");
            if ($compare_items != null) {
                if (in_array($product->id, $compare_items)) {
                    $response = array('status' => 'error', 'message' => trans('compare.this_item_already_in_compare_list'), 'notification' => 'information');
                } else {
                    Session::push("compare.items$product->category_id", $product->id);
                    $response = array('status' => 'ok', 'message' => trans('compare.product_added_to_your_compare_list'), 'notification' => 'success');
                }
            } else {
                Session::push("compare.items$product->category_id", $product->id);
                $response = array('status' => 'ok', 'message' => trans('compare.product_added_to_your_compare_list'), 'notification' => 'success');
            }
        } else {
            $response = array('status' => 'ok', 'message' => trans('compare.product_not_found'), 'notification' => 'error');
        }

        $response["count"] = count(Session::get("compare.items$product->category_id"));


        return Response::json($response);

    }

    public function postAjaxRemoveFromCompare()
    {
        $product_id = Input::only('product_id');

        $product = Product::find($product_id)->first();

        $compare_products = Session::get("compare.items$product->category_id");

        if ($product) {
            if (($key = array_search($product->id, $compare_products)) !== false) {
                unset($compare_products[$key]);
            }
        }

        Session::forget("compare.items$product->category_id");

        Session::put("compare.items$product->category_id", $compare_products);

        return Response::json($compare_products);

    }

}