<?php

namespace Front;

use BaseController;
use Illuminate\Support\Facades\Redis;

class FrontController extends BaseController {
    public function redis (){

        $redis = Redis::connection();

        $test = array("test" => "hello", "ikinci" => "bu ikincidir");

        $redis->set('name', json_encode($test));

        $name = $redis->get('name');

        return $name;

    }
}
