<?php

namespace Front;

use Illuminate\Support\Facades\View;
use Category;

class HomeController extends FrontController {
    public function __construct()
    {

    }

    public function showWelcome()
	{
        $roots = Category::roots()
            ->whereSectorId(1)
            ->get();

		return View::make('front.homepage')
            ->with("roots", $roots);
	}

}
