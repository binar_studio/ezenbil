<?php

namespace Front;

use Category;
use Illuminate\Support\Collection;
use Product;
use Attribute;
use AttributeGroup;
use Illuminate\Support\Facades\View;

class ProductController extends FrontController
{

    public function showProduct($product_id)
    {
        $product = Product::where("id", "=", $product_id)->first();

        $recent_products = Product::orderBy('updated_at', 'desc')->take(6)->get();
        $popular_products = Product::orderBy('updated_at', 'desc')->take(6)->get();

        $category = Category::where("id", "=", $product["category_id"])->first();

        return View::make('front.product')
            ->with("product", $product)
            ->with("category", $category)
            ->with("recent_products", $recent_products)
            ->with("popular_products", $popular_products);

    }

}
