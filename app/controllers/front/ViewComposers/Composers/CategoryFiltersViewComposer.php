<?php

namespace Front\ViewComposers\Composers;

use Category;
use Product;

class CategoryFiltersViewComposer{
    public function compose($view)
    {
        $viewdata= $view->getData();

        $category = Category::find($viewdata['category_id']);

        $productsQuery = Product::getProductsForCategoryQuery($category->id);

        $priceMax = $productsQuery->max('price');
        $priceMin = $productsQuery->min('price');

        $optionables = array();
        $booleanables = array();

        foreach ($category->features as $f)
        {
            if ($f->has_options) {
                $optionables []= $f;
            }
            else if ($f->is_boolean)
            {
                $booleanables []= $f;
            }
        }

        $view->with(compact('category', 'optionables', 'booleanables', 'priceMax', 'priceMin'));
    }
}