<?php

namespace Front\ViewComposers\Composers;

use Category;

class MenuViewComposer{
    public function compose($view)
    {
        $root_categories = Category::roots()->rememberForever("root-categories")->get();
        $view->with('root_categories', $root_categories);
    }
}