<?php

namespace Front\ViewComposers\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->view->composer('front.elements.menu', 'Front\ViewComposers\Composers\MenuViewComposer');
        $this->app->view->composer('front.elements.filters', 'Front\ViewComposers\Composers\CategoryFiltersViewComposer');
    }

}