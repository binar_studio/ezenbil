<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttributeGroupsTable extends Migration {

	public function up()
	{
		Schema::create('attribute_groups', function(Blueprint $table) {
			$table->increments('id');
			$table->string('global_name', 20);
			$table->string('name_az', 20);
			$table->string('name_ru', 20);
			$table->boolean('is_color_group');
		});
	}

	public function down()
	{
		Schema::drop('attribute_groups');
	}
}