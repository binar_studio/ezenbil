<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttributesTable extends Migration {

	public function up()
	{
		Schema::create('attributes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('attribute_group_id')->unsigned()->index();
			$table->string('name_az');
			$table->string('color', 15)->nullable();
			$table->string('name_ru');
		});
	}

	public function down()
	{
		Schema::drop('attributes');
	}
}