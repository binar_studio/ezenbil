<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesFeaturesTable extends Migration {

	public function up()
	{
		Schema::create('categories_features', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->integer('category_id')->unsigned()->index();
			$table->integer('feature_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('categories_features');
	}
}