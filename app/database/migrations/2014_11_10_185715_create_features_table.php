<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFeaturesTable extends Migration {

	public function up()
	{
		Schema::create('features', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name_az', 100);
			$table->string('name_ru', 100);
			$table->integer('unit_group_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('features');
	}
}