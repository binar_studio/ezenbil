<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePhotosTable extends Migration {

	public function up()
	{
		Schema::create('photos', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id')->unsigned()->index();
			$table->integer('attribute_id')->unsigned()->index();
			$table->string('title_az');
			$table->string('title_ru');
			$table->string('filename');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('photos');
	}
}