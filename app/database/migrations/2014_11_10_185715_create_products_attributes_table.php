<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsAttributesTable extends Migration {

	public function up()
	{
		Schema::create('products_attributes', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id')->unsigned()->index();
			$table->integer('attribute_id')->unsigned()->index();
			$table->decimal('price_impact', 8,2)->default('0');
			$table->decimal('price_total');
		});
	}

	public function down()
	{
		Schema::drop('products_attributes');
	}
}