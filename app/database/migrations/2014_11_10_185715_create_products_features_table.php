<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsFeaturesTable extends Migration {

	public function up()
	{
		Schema::create('products_features', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id')->unsigned()->index();
			$table->integer('feature_id')->unsigned()->index();
			$table->integer('unit_id')->unsigned()->index();
			$table->string('value');
		});
	}

	public function down()
	{
		Schema::drop('products_features');
	}
}