<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('category_id')->unsigned()->index();
			$table->string('name_az');
			$table->string('name_ru');
			$table->timestamps();
			$table->softDeletes();
			$table->decimal('price');
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}