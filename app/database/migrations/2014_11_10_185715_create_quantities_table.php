<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateQuantitiesTable extends Migration {

	public function up()
	{
		Schema::create('quantities', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('product_id')->unsigned()->index();
			$table->integer('attribute_id')->unsigned()->nullable()->index();
			$table->decimal('value');
		});
	}

	public function down()
	{
		Schema::drop('quantities');
	}
}