<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStockMovementReasonsTable extends Migration {

	public function up()
	{
		Schema::create('stock_movement_reasons', function(Blueprint $table) {
			$table->increments('id');
			$table->string('value', 100);
		});
	}

	public function down()
	{
		Schema::drop('stock_movement_reasons');
	}
}