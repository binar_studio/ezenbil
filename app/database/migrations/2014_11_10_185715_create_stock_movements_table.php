<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStockMovementsTable extends Migration {

	public function up()
	{
		Schema::create('stock_movements', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('quantity_id')->unsigned()->index();
			$table->integer('change');
			$table->integer('reason_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('stock_movements');
	}
}