<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnitGroupsTable extends Migration {

	public function up()
	{
		Schema::create('unit_groups', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->integer('base_unit_id')->unsigned()->index();
		});
	}

	public function down()
	{
		Schema::drop('unit_groups');
	}
}