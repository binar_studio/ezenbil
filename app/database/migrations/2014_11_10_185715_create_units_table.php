<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUnitsTable extends Migration {

	public function up()
	{
		Schema::create('units', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name_az', 10);
			$table->string('name_ru', 10);
			$table->integer('unit_group_id')->unsigned()->index();
			$table->float('base_ratio');
		});
	}

	public function down()
	{
		Schema::drop('units');
	}
}