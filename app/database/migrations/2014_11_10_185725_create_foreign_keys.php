<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('products', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('features', function(Blueprint $table) {
			$table->foreign('unit_group_id')->references('id')->on('unit_groups')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('attributes', function(Blueprint $table) {
			$table->foreign('attribute_group_id')->references('id')->on('attribute_groups')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('categories', function(Blueprint $table) {
			$table->foreign('parent_id')->references('id')->on('categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('units', function(Blueprint $table) {
			$table->foreign('unit_group_id')->references('id')->on('unit_groups')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('quantities', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('quantities', function(Blueprint $table) {
			$table->foreign('attribute_id')->references('id')->on('attributes')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('products_features', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('products_features', function(Blueprint $table) {
			$table->foreign('feature_id')->references('id')->on('features')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('products_features', function(Blueprint $table) {
			$table->foreign('unit_id')->references('id')->on('units')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('photos', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('photos', function(Blueprint $table) {
			$table->foreign('attribute_id')->references('id')->on('attributes')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('products_attributes', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('products')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('products_attributes', function(Blueprint $table) {
			$table->foreign('attribute_id')->references('id')->on('attributes')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('unit_groups', function(Blueprint $table) {
			$table->foreign('base_unit_id')->references('id')->on('units')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('stock_movements', function(Blueprint $table) {
			$table->foreign('quantity_id')->references('id')->on('quantities')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('stock_movements', function(Blueprint $table) {
			$table->foreign('reason_id')->references('id')->on('stock_movement_reasons')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('categories_features', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('categories')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('categories_features', function(Blueprint $table) {
			$table->foreign('feature_id')->references('id')->on('features')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('products', function(Blueprint $table) {
			$table->dropForeign('products_category_id_foreign');
		});
		Schema::table('features', function(Blueprint $table) {
			$table->dropForeign('features_unit_group_id_foreign');
		});
		Schema::table('attributes', function(Blueprint $table) {
			$table->dropForeign('attributes_attribute_group_id_foreign');
		});
		Schema::table('categories', function(Blueprint $table) {
			$table->dropForeign('categories_parent_id_foreign');
		});
		Schema::table('units', function(Blueprint $table) {
			$table->dropForeign('units_unit_group_id_foreign');
		});
		Schema::table('quantities', function(Blueprint $table) {
			$table->dropForeign('quantities_product_id_foreign');
		});
		Schema::table('quantities', function(Blueprint $table) {
			$table->dropForeign('quantities_attribute_id_foreign');
		});
		Schema::table('products_features', function(Blueprint $table) {
			$table->dropForeign('products_features_product_id_foreign');
		});
		Schema::table('products_features', function(Blueprint $table) {
			$table->dropForeign('products_features_feature_id_foreign');
		});
		Schema::table('products_features', function(Blueprint $table) {
			$table->dropForeign('products_features_unit_id_foreign');
		});
		Schema::table('photos', function(Blueprint $table) {
			$table->dropForeign('photos_product_id_foreign');
		});
		Schema::table('photos', function(Blueprint $table) {
			$table->dropForeign('photos_attribute_id_foreign');
		});
		Schema::table('products_attributes', function(Blueprint $table) {
			$table->dropForeign('products_attributes_product_id_foreign');
		});
		Schema::table('products_attributes', function(Blueprint $table) {
			$table->dropForeign('products_attributes_attribute_id_foreign');
		});
		Schema::table('unit_groups', function(Blueprint $table) {
			$table->dropForeign('unit_groups_base_unit_id_foreign');
		});
		Schema::table('stock_movements', function(Blueprint $table) {
			$table->dropForeign('stock_movements_quantity_id_foreign');
		});
		Schema::table('stock_movements', function(Blueprint $table) {
			$table->dropForeign('stock_movements_reason_id_foreign');
		});
		Schema::table('categories_features', function(Blueprint $table) {
			$table->dropForeign('categories_features_category_id_foreign');
		});
		Schema::table('categories_features', function(Blueprint $table) {
			$table->dropForeign('categories_features_feature_id_foreign');
		});
	}
}