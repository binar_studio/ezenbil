<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeCategoriesTree extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
    public function up()
    {


        DB::statement('ALTER TABLE `categories` MODIFY `parent_id` INTEGER UNSIGNED NULL;');
        DB::statement('UPDATE `categories` SET `parent_id` = NULL WHERE `parent_id` = 0;');


        Schema::table('categories', function (Blueprint $table) {

            $table->integer('lft')->nullable()->index();
            $table->integer('rgt')->nullable()->index();
            $table->integer('depth')->nullable();

            $table->timestamps();
        });
    }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {

      DB::statement('UPDATE `categories` SET `parent_id` = 0 WHERE `parent_id` IS NULL;');
      DB::statement('ALTER TABLE `categories` MODIFY `parent_id` INTEGER UNSIGNED NOT NULL;');


      Schema::table('categories', function (Blueprint $table) {

          $table->dropColumn('lft');
          $table->dropColumn('rgt');
          $table->dropColumn('depth');

          $table->dropTimestamps();
      });
  }

}
