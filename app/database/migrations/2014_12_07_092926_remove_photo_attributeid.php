<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemovePhotoAttributeid extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
        Schema::table('photos', function($table)
        {
            $table->dropForeign('photos_attribute_id_foreign');
            $table->dropColumn('attribute_id');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
        Schema::table('photos', function($table)
        {
            $table->integer('attribute_id')->unsigned()->index();
        });
	}

}
