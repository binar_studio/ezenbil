<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveUnitgroupsBaseUnitId extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('unit_groups', function($table)
        {
            $table->dropForeign('unit_groups_base_unit_id_foreign');
            $table->dropColumn('base_unit_id');

        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

        Schema::table('unit_groups', function($table)
        {
            $table->integer('base_unit_id')->unsigned()->index();
        });
	}

}
