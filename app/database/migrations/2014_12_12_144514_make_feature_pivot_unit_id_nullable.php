<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeFeaturePivotUnitIdNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        DB::statement('ALTER TABLE `features` MODIFY `unit_group_id` INTEGER UNSIGNED NULL;');
        DB::statement('UPDATE `features` SET `unit_group_id` = NULL WHERE `unit_group_id` = 0;');

        DB::statement('ALTER TABLE `products_features` MODIFY `unit_id` INTEGER UNSIGNED NULL;');
        DB::statement('UPDATE `products_features` SET `unit_id` = NULL WHERE `unit_id` = 0;');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        DB::statement('UPDATE `features` SET `unit_group_id` = 0 WHERE `unit_group_id` IS NULL;');
        DB::statement('ALTER TABLE `features` MODIFY `unit_group_id` INTEGER UNSIGNED NOT NULL;');

        DB::statement('UPDATE `products_features` SET `unit_id` = 0 WHERE `unit_id` IS NULL;');
        DB::statement('ALTER TABLE `products_features` MODIFY `unit_id` INTEGER UNSIGNED NOT NULL;');
	}

}
