<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAttrgroupsGlobalnameToDescriptor extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::table('attribute_groups', function($table)
        {
            $table->renameColumn('global_name', 'descriptor');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::table('attribute_groups', function($table)
        {
            $table->renameColumn('descriptor', 'global_name');
        });
	}

}
