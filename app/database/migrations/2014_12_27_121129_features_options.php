<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FeaturesOptions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('feature_options', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('feature_id')->unsigned()->index();
            $table->string('name_az');
            $table->string('name_ru');
        });

        Schema::table('features', function($table)
        {
            $table->boolean('has_options')->default(false);
        });

        Schema::table('products_features', function($table)
        {
            $table->integer('option_id')->unsigned()->nullable()->index();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('feature_options');

        Schema::table('features', function($table)
        {
            $table->dropColumn('has_options');
        });

        Schema::table('products_features', function($table)
        {
            $table->dropForeign('products_features_option_id_foreign');
            $table->dropColumn('option_id');
        });
	}

}
