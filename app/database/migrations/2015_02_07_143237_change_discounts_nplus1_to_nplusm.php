<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeDiscountsNplus1ToNplusm extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('discounts', function($table)
        {
            $table->dropColumn('nplus1');
            $table->integer('nplusm_n')->nullable();
            $table->integer('nplusm_m')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('discounts', function($table)
        {
            $table->integer('nplus1')->nullable();
            $table->dropColumn('nplusm_n');
            $table->dropColumn('nplusm_m');
        });
    }

}
