<?php

class AttributeGroupTableSeeder extends Seeder {

	public function run()
	{
		DB::table('attribute_groups')->delete();

        // color
        AttributeGroup::create(array(
            'descriptor' => '',
            'name_az' => 'Rəng',
            'name_ru' => 'Цвет',
            'is_color_group' => true
        ));

        AttributeGroup::create(array(
            'descriptor' => '',
            'name_az' => 'Ölçü',
            'name_ru' => 'Razmer',
            'is_color_group' => false
        ));
	}
}