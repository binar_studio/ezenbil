<?php

class AttributeTableSeeder extends Seeder {

	public function run()
	{
		DB::table('attributes')->delete();

		// attr_small
		Attribute::create(array(
				'attribute_group_id' => 1,
				'name_az' => 'Kiçik',
				'name_ru' => 'Малый'
			));

		// attr_big
		Attribute::create(array(
				'attribute_group_id' => 1,
				'name_az' => 'Böyük',
				'name_ru' => 'Большой'
			));
	}
}