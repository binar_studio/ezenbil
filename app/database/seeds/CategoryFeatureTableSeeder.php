<?php

class CategoryFeatureTableSeeder extends Seeder {

	public function run()
	{
		DB::table('categories_features')->delete();

		// cat2_f1
		CategoryFeature::create(array(
				'category_id' => 2,
				'feature_id' => 1
			));

		// cat2_f2
		CategoryFeature::create(array(
				'category_id' => 2,
				'feature_id' => 2
			));
	}
}