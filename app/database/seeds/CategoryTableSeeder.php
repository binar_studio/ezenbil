<?php

class CategoryTableSeeder extends Seeder {

	public function run()
	{
		DB::table('categories')->delete();

		// main
		$root = Category::create(array(
				'name_az' => 'Əsas kateqoriya',
				'name_ru' => 'Главная категория',
                'sector_id' => 1
			));

		// child1
		$child1 = Category::create(array(
				'name_az' => 'Alt kateqoriya 1',
				'name_ru' => 'Подкатегория 1',
                'sector_id' => 1
			));

		// child2
		$child2 = Category::create(array(
				'name_az' => 'Alt kateqoriya 2',
				'name_ru' => 'Подкатегория 2',
                'sector_id' => 1
			));

        $child1->makeChildOf($root);
        $child1->save();

        $child2->makeChildOf($root);
        $child2->save();
	}
}