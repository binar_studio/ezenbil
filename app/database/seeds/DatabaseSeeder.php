<?php

class DatabaseSeeder extends Seeder {

	public function run()
	{
		Eloquent::unguard();

        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

		$this->call('ProductTableSeeder');
		$this->command->info('Product table seeded!');

		$this->call('FeatureTableSeeder');
		$this->command->info('Feature table seeded!');

		$this->call('AttributeTableSeeder');
		$this->command->info('Attribute table seeded!');

		$this->call('CategoryTableSeeder');
		$this->command->info('Category table seeded!');

		$this->call('UnitTableSeeder');
		$this->command->info('Unit table seeded!');

		$this->call('QuantityTableSeeder');
		$this->command->info('Quantity table seeded!');

		$this->call('ProductAttributeTableSeeder');
		$this->command->info('ProductAttribute table seeded!');

		$this->call('UnitGroupTableSeeder');
		$this->command->info('UnitGroup table seeded!');

		$this->call('AttributeGroupTableSeeder');
		$this->command->info('AttributeGroup table seeded!');

		$this->call('StockMovementTableSeeder');
		$this->command->info('StockMovement table seeded!');

		$this->call('StockMovementReasonTableSeeder');
		$this->command->info('StockMovementReason table seeded!');

		$this->call('CategoryFeatureTableSeeder');
		$this->command->info('CategoryFeature table seeded!');

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}
}