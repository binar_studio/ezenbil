<?php

class FeatureTableSeeder extends Seeder {

	public function run()
	{
		DB::table('features')->delete();

		// feature1
		$f1 = Feature::create(array(
				'name_az' => 'Uzunluq',
				'name_ru' => 'Длина',
				'unit_group_id' => 1
			));

		// feature2
		$f2 = Feature::create(array(
				'name_az' => 'En',
				'name_ru' => 'Ширина',
				'unit_group_id' => 1
			));

		$p1 = Product::first();

		$p1->features()->attach($f1, array('value' => 10, 'unit_id' => 2));
		$p1->features()->attach($f2, array('value' => 20, 'unit_id' => 2));
	}
}