<?php

class ProductAttributeTableSeeder extends Seeder {

	public function run()
	{
		DB::table('products_attributes')->delete();

		// prod1_attr1
		ProductAttribute::create(array(
				'price_impact' => 1,
				'product_id' => 1,
				'attribute_id' => 1
			));

		// prod1_attr2
		ProductAttribute::create(array(
				'product_id' => 1,
				'attribute_id' => 2,
				'price_impact' => -1,
			));
	}
}