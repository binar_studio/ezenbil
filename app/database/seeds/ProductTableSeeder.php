<?php

class ProductTableSeeder extends Seeder {

	public function run()
	{
		DB::table('products')->delete();

		// product1
		Product::create(array(
				'category_id' => 2,
				'name_az' => 'Nümunə məhsul 1',
				'name_ru' => 'Пример продукт 1',
				'price' => 50.00
			));
	}
}