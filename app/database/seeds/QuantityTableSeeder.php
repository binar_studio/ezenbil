<?php

class QuantityTableSeeder extends Seeder {

	public function run()
	{
		DB::table('quantities')->delete();

		// prod1_attr1
		Quantity::create(array(
				'product_id' => 1,
				'attribute_id' => 1,
				'value' => 10
			));

		// prod1_attr2
		Quantity::create(array(
				'product_id' => 1,
				'attribute_id' => 2,
				'value' => 15
			));
	}
}