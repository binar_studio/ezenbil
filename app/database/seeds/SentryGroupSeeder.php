<?php
class SentryGroupSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->delete();

        Sentry::getGroupProvider()->create(array(
            'name' => 'Admins',
            'permissions' => array(
                'manage_users' => 1,
            ),
        ));

        Sentry::getGroupProvider()->create(array(
            'name' => 'Managers',
            'permissions' => array(
                'manage_products' => 1,
                'manage_orders' => 1,
            ),
        ));

        Sentry::getGroupProvider()->create(array(
            'name' => 'Customers',
            'permissions' => array(
                'add_to_cart' => 1,
                'checkout' => 1,
            ),
        ));
    }
}