<?php
class SentryUserGroupSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_groups')->delete();

        $customerUser = Sentry::getUserProvider()->findByLogin('customer@example.com');
        $adminUser = Sentry::getUserProvider()->findByLogin('admin@example.com');

        $customerGroup = Sentry::getGroupProvider()->findByName('Customers');
        $adminGroup = Sentry::getGroupProvider()->findByName('Admins');

        // Assign the groups to the users
        $customerUser->addGroup($customerGroup);
        $adminUser->addGroup($adminGroup);
    }
}