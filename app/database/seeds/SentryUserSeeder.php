<?php
class SentryUserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        Sentry::getUserProvider()->create(array(
            'email' => 'customer@example.com',
            'password' => '12345',
            'first_name' => 'Example',
            'last_name' => 'Customer',
            'activated' => 1,
        ));

        Sentry::getUserProvider()->create(array(
            'email' => 'admin@example.com',
            'password' => '12345',
            'first_name' => 'Example',
            'last_name' => 'Admin',
            'activated' => 1,
        ));
    }
}