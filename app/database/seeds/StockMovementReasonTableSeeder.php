<?php

class StockMovementReasonTableSeeder extends Seeder {

	public function run()
	{
		DB::table('stock_movement_reasons')->delete();

		// new
		StockMovementReason::create(array(
				'value' => 'Supply'
			));

		// sold
		StockMovementReason::create(array(
				'value' => 'Sold'
			));
	}
}