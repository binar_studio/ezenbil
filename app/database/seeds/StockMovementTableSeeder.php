<?php

class StockMovementTableSeeder extends Seeder {

	public function run()
	{
		DB::table('stock_movements')->delete();

		// q1_import
		StockMovement::create(array(
				'quantity_id' => 1,
				'change' => 1,
				'reason_id' => 1
			));
	}
}