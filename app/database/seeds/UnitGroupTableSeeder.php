<?php

class UnitGroupTableSeeder extends Seeder {

	public function run()
	{
		DB::table('unit_groups')->delete();

		// Distance
		UnitGroup::create(array(
				'name' => 'distance',
			));
	}
}