<?php

class UnitTableSeeder extends Seeder {

	public function run()
	{
		DB::table('units')->delete();

		// millimeter
		Unit::create(array(
				'name_az' => 'mm',
				'unit_group_id' => 1,
				'name_ru' => 'мм'
			));

		// centimeter
		Unit::create(array(
				'name_az' => 'sm',
				'unit_group_id' => 1,
				'name_ru' => 'см'
			));

		// decimeter
		Unit::create(array(
				'name_az' => 'dm',
				'name_ru' => 'дм',
				'unit_group_id' => 1,
			));

		// meter
		Unit::create(array(
				'name_az' => 'm',
				'name_ru' => 'м',
				'unit_group_id' => 1,
			));
	}
}