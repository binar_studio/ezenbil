<?php

return array(
    "signin" => "Hesaba giriş",
    "email" => "E-mail",
    "password" => "Şifrə",
    "sign_in_to_your_account" => "Şəxsi hesabınıza daxil olun",
    "forgot_password" => "Parolunuzu unutmusunuz?",
    "no_account_register" => "Hesabınız yoxdur? Qeydiyyatdan keçin.",
    "login" => "Daxil ol",
    "details" => "Detallar",
    "comments" => "Şərhlər"

);