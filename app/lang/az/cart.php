<?php

return array(
    "upper_your_shopping_cart" => "SİZİN SƏBƏTİNİZ",
    "upper_product" => "MƏHSUL",
    "upper_price" => "QİYMƏT",
    "upper_quantity" => "MİQDAR",
    "upper_total" => "CƏMİ",
    "upper_cart_total" => "Səbətdə cəmi",
    "upper_calculate_shipping" => "Çatdırılma kalkulyatoru",
    "subtotal" => "Subtotal",
    "shipping" => "Çatdırılma",
    "order_total" => "Cəmi ödəniş",
    "update_total" => "Yenidən hesabla",
    "checkout" => "Ödə",
    "free_shipping" => "Pulsuz çatdırılma"
);