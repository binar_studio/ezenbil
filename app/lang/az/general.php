<?php

return array(
    "bestSales" => "Ən çox satılanlar",
    "newItems" => "Yeni əlavə olunanlar",
    "bestSale" => "Populyardır",
    "technology" => "Texnologiya",
    "viewMore" => "Ətraflı",
    "recentProducts" => "Yeni məhsullar",
    "popularProducts" => "Çox satılan məhsullar",
    "details" => "Detallar",
    "comments" => "Şərhlər",
    "add_to_compare" => "Müqaisə et",
    "price_range" => "Qiymət aralığı",
    "features" => "Xüsusiyyətlər",
    "upper_new" => "YENİ",
    "recent_opened_products" => "Baxılan məhsullar"

);