<?php

class Attribute extends Eloquent {

    use TranslationTrait;

	protected $table = 'attributes';
	public $timestamps = false;

    protected $translated = ['name'];

	public function group()
	{
		return $this->belongsTo('AttributeGroup', 'attribute_group_id');
	}

}