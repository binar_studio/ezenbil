<?php

class AttributeGroup extends Eloquent {

    use TranslationTrait;

	protected $table = 'attribute_groups';
    protected $fillable = array('name_az', 'name_ru', 'descriptor', 'is_color_group');
	public $timestamps = false;

    protected $translated = ['name'];

	public function attributes()
	{
		return $this->hasMany('Attribute');
	}

    public function getGlobalNameAttribute()
    {
        if (empty($this->descriptor)) {
            return $this->name_az;
        } else {
            return $this->name_az.' ('.$this->descriptor.')';
        }
    }

}