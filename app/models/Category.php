<?php
use Baum\Node;

/**
* Category
*/
class Category extends Node {

    use TranslationTrait;

	protected $table = 'categories';

    protected $translated = ['name'];

	public function features()
	{
		return $this->belongsToMany('Feature', 'categories_features');
	}

    // /**
    //  * Columns which restrict what we consider our Nested Set list
    //  *
    //  * @var array
    //  */
    protected $scoped = array('sector_id');

}