<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 1/23/15
 * Time: 3:58 PM
 */

class CouponLineItem extends LineItem {

    public function lineItem() {
        return $this->morphOne('LineItem', 'lineable');
    }

    public function coupon() {
        return $this->belongsTo('Coupon');
    }

} 