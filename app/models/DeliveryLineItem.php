<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 1/23/15
 * Time: 3:58 PM
 */

class DeliveryLineItem extends LineItem {

    protected $table = 'delivery_line_items';
    public $timestamps = false;

    public function lineItem(){
        return $this->morphOne('LineItem', 'lineable');
    }

} 