<?php

class Discount extends Eloquent {

    protected $table = 'discounts';
    protected $fillable = array('title', 'limit', 'percentage', 'nplusm_n', 'nplusm_m', 'starts', 'ends');
    public $timestamps = true;

    public function products()
    {
        return $this->hasMany('Product');
    }

}