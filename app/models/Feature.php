<?php

class Feature extends ProductFeature {

	protected $table = 'features';
	public $timestamps = false;
	protected $fillable = array('name_az', 'name_ru', 'has_options');
	protected $visible = array('name_az', 'name_ru', 'has_options');
    protected $appends = array('global_name');

    use TranslationTrait;
    protected $translated = ['name'];

	public function unitGroup()
	{
		return $this->belongsTo('UnitGroup');
	}

    public function getGlobalNameAttribute()
    {
        if (empty($this->descriptor)) {
            return $this->name_az;
        } else {
            return $this->name_az.' ('.$this->descriptor.')';
        }
    }

    public function options()
    {
        return $this->hasMany('FeatureOption');
    }

}