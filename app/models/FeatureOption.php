<?php

class FeatureOption extends Eloquent {

	protected $table = 'feature_options';
    protected $fillable = array('name_az', 'name_ru');
	public $timestamps = false;

    use TranslationTrait;
    protected $translated = ['name'];

	public function feature()
	{
		return $this->belongsTo('Feature');
	}

}