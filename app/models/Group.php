<?php

use Cartalyst\Sentry\Groups\GroupInterface;

class Group extends \Cartalyst\Sentry\Groups\Eloquent\Group implements GroupInterface  {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'groups';

}
