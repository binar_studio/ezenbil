<?php

class LineItem extends Eloquent {

    /*
     * Tutorial
     * http://stackoverflow.com/a/26704488/421791
     */

    protected $table = 'line_items';
    public $timestamps = false;

    public function lineable(){
        return $this->morphTo();
    }

    public function order() {
        return $this->belongsTo('Order');
    }

}