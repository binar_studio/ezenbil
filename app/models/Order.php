<?php

class Order extends Eloquent {

    protected $table = 'orders';
    public $timestamps = true;
    public $fillable = array('status', 'sector_id');

    public function user()
    {
        return $this->belongsTo('User');
    }

    public function lineItems()
    {
        return $this->hasMany('LineItem');
    }

    public static function getOrCreateCartForUser($user, $sector_id = 1)
    {
        $cart = $user->orders()
            ->where('status', '=', 'cart')
            ->where('sector_id', '=', $sector_id)
            ->first();

        if (is_null($cart)) {

            $cart = $user->orders()->save(new Order(array(
                'status' => 'cart',
                'sector_id' => $sector_id
            )));
        }

        return $cart;
    }

    /**
     * @param $cart Cart of user
     * @returns Mixed array
     */
    public static function getCartDetails($cart)
    {

        $cartDetails = array();

        $products = array();

        $totalOriginal = 0;
        $total = 0;

        $lineItems = $cart->lineItems;

        foreach ($lineItems as $lineItem) {

            if ($lineItem->lineable instanceof ProductLineItem)
            {
                $productItem = array();

                $item = $lineItem->lineable;
                $product = $item->product;

                $quantityOriginal = $item->quantity;
                $quantityPlus = 0;
                $quantity = $quantityOriginal;

                $priceOriginal = $product->price;
                if ($item->attribute) {
                    $attr = $product->attributes()->where('attributes.id', '=', $item->attribute->id)->get()->first();
                    $priceOriginal += $attr->pivot->price_impact;
                }
                $price = $priceOriginal;

                $subtotalOriginal = 0;
                $subtotal = 0;

                if ($product->discount) {

                    $discount = $product->discount;

                    if ($discount->type == 'percentage')
                    {
                        $price = $priceOriginal * (100 - $discount->percentage) / 100;
                        $subtotalOriginal = $priceOriginal * $quantityOriginal;
                        $subtotal = $price * $quantityOriginal;

                    } else if ($discount->type == 'nplus1' && $discount->nplusm_n)
                    {

                        $quantityPlus = floor($quantityOriginal / $discount->nplusm_n) * $discount->nplusm_m;
                        $quantity = $quantityOriginal + $quantityPlus;

                        $subtotalOriginal = $quantity * $priceOriginal;
                        $subtotal = $quantityOriginal * $priceOriginal;
                    }
                } else {
                    $subtotal = $priceOriginal * $quantityOriginal;
                }

                $total += $subtotal;
                $totalOriginal += $subtotalOriginal;

                $productItem['item'] = $item;
                $productItem['quantityOriginal'] = $quantityOriginal;
                $productItem['quantityPlus'] = $quantityPlus;
                $productItem['quantity'] = $quantity;
                $productItem['priceOriginal'] = $priceOriginal;
                $productItem['price'] = $price;
                $productItem['subtotalOriginal'] = $subtotalOriginal;
                $productItem['subtotal'] = $subtotal;
                $productItem['priceSale'] = $price < $priceOriginal;
                $productItem['quantitySale'] = $quantityPlus > 0;
                $productItem['subtotalSale'] = $subtotal < $subtotalOriginal;

                $products []= $productItem;

            } else if ($lineItem->lineable instanceof CouponLineItem)
            {
                // TODO
            } else if ($lineItem->lineable instanceof DeliveryLineItem)
            {
                // TODO
            }
        }

        $cartDetails['products'] = $products;
        $cartDetails['total'] = $total;
        $cartDetails['totalOriginal'] = $totalOriginal;

        return $cartDetails;

    }

}