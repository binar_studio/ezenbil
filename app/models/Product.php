<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\Database\Eloquent\Collection;

class Product extends ProductFeature
{

    protected $table = 'products';
    public $timestamps = true;

    use SoftDeletingTrait, TranslationTrait;

    protected $dates = ['deleted_at'];
    protected $fillable = array('name_az', 'name_ru');
    protected $visible = array('category_id', 'name_az', 'name_ru');

    protected $translated = ['name', 'description'];

    public function quantities()
    {
        return $this->hasMany('Quantity');
    }

    public function category()
    {
        return $this->belongsTo('Category');
    }

    public function features()
    {
        return $this->belongsToMany('Feature', 'products_features')->withPivot('unit_id', 'value', 'option_id');
    }

    public function photos()
    {
        return $this->hasMany('Photo');
    }

    public function mainPhoto()
    {
        return $this->hasOne('Photo', 'id', 'main_photo_id');
    }

    public function attributes()
    {
        return $this->belongsToMany('Attribute', 'products_attributes')->withPivot('price_impact');
    }

    
    public function discount()
    {
        return $this->belongsTo('Discount');
    }

    public static function getProductsForCategoryQuery($cat_id)
    {
        $category = Category::where('id', '=', $cat_id)->first();

        $categories = Category::where(DB::raw('rgt - lft'), '=', '1')
            ->where('lft', '>=', $category->lft)
            ->where('rgt', '<=', $category->rgt)
            ->select('id')
            ->get()
            ->toArray();

        $products = Product::whereIn('category_id', $categories);

        return $products;
    }

    public static function getProductsForSectionQuery($sec_id)
    {

        $categories = Category::where(DB::raw('rgt - lft'), '=', '1')
            ->where('sector_id', '=', $sec_id)
            ->select('id')
            ->get()
            ->toArray();

        $products = Product::whereIn('category_id', $categories);

        return $products;
    }

}