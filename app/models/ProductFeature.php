<?php

class ProductFeature extends Eloquent {

    public function newPivot(\Illuminate\Database\Eloquent\Model $parent, array $attributes, $table, $exists)
    {
        return new ProductFeatureFeatureOptionPivot($parent, $attributes, $table, $exists);
    }

}