<?php

use Illuminate\Database\Eloquent\Relations\Pivot;

class ProductFeatureFeatureOptionPivot extends Pivot {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products_features';
    protected $fillable = array('option_id');
    protected $guarded = array('id');

    public function option()
    {
        return $this->hasOne('FeatureOption', 'id', 'option_id');
    }

}