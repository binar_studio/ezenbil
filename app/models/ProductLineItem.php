<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 1/23/15
 * Time: 3:58 PM
 */

class ProductLineItem extends LineItem {

    protected $table = 'product_line_items';
    public $fillable = array('product_id', 'attribute_id', 'quantity');

    public function lineItem(){
        return $this->morphOne('LineItem', 'lineable');
    }

    public function product() {
        return $this->belongsTo('Product');
    }

    public function attribute() {
        return $this->belongsTo('Attribute');
    }
} 