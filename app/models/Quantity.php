<?php

class Quantity extends Eloquent {

	protected $table = 'quantities';
	public $timestamps = false;
	protected $fillable = array('value');
	protected $visible = array('value');


    public function product()
    {
        return $this->belongsTo('Product');
    }

    public function attribute()
    {
        return $this->belongsTo('Attribute');
    }

    public static function getForProduct($product_id, $attr_id = null)
    {

        $result = 0;

        if (is_null($attr_id)) {

            $result = DB::table('quantities')
                ->select(DB::raw('SUM(value) as quantity'))
                ->where('product_id', $product_id)
                ->first();

            $result = $result->quantity ? $result->quantity : 0;

        } else {
            $result = DB::table('quantities')
                ->select('value')
                ->where('product_id', $product_id)
                ->where('attribute_id', $attr_id)
                ->first();

            $result = $result ? $result->value : 0;
        }

        return $result;
    }

}