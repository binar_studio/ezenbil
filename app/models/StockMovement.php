<?php

class StockMovement extends Eloquent {

	protected $table = 'stock_movements';
	public $timestamps = true;

	public function quantity()
	{
		return $this->belongsTo('Quantity');
	}

	public function reason()
	{
		return $this->belongsTo('StockMovementReason');
	}

}