<?php

class StockMovementReason extends Eloquent {

	protected $table = 'stock_movement_reasons';
	public $timestamps = false;

}