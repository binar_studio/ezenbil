<?php
/**
 * Created by PhpStorm.
 * User: togrul
 * Date: 2/4/15
 * Time: 12:54 PM
 */

trait TranslationTrait {


    public function getAttribute($key)
    {
        $inTranslated = in_array($key, $this->translated);

        if($inTranslated)
        {
            return $this->{$key."_".App::getLocale()};

        } else {

            return parent::getAttribute($key);
        }

    }
}