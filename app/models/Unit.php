<?php

class Unit extends Eloquent {
    
	use TranslationTrait;

	protected $table = 'units';
	public $timestamps = false;

    protected $translated = ['name'];

	public $fillable = array("name_az", "name_ru");

	public function unitGroup()
	{
		return $this->belongsTo('UnitGroup');
	}

}