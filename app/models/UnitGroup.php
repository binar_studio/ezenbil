<?php

class UnitGroup extends Eloquent {

	protected $table = 'unit_groups';
	public $timestamps = false;

	public $fillable = array("name");

	public function baseUnit()
	{
		return $this->hasOne('Unit');
	}

    public function units()
    {
        return $this->hasMany('Unit');
    }

}