<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Cartalyst\Sentry\Users\Eloquent\User implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    protected $fillable = array('name', 'email', 'password');

    public static $rules = array(
        'email' => 'required|email',
        'password' => 'required',
        'phone' => 'numeric',
        'last_name' => 'alpha',
        'first_name' => 'alpha'
    );

	protected $hidden = array('password', 'remember_token');

    public function orders()
    {
        return $this->hasMany('Order');
    }

}
