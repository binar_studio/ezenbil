<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(
    array('prefix' => 'admin', 'namespace' => 'Admin', 'before' => 'Sentry|notInGroup:Customers'),
    function () {

        Route::get('/', 'HomeController@getIndex');

        Route::get('/products', 'ProductsController@getIndex');
        Route::get('/products/new', 'ProductsController@getCreate');
        Route::post('/products/new', 'ProductsController@postStore');

        Route::get('/product/{id}', 'ProductsController@getViewGeneral');
        Route::get('/product/{id}/features', 'ProductsController@getViewFeatures');
        Route::get('/product/{id}/attributes', 'ProductsController@getViewAttributes');
        Route::get('/product/{id}/photos', 'ProductsController@getViewPhotos');

        Route::get('/product/{id}/remove', 'ProductsController@getRemove');
        Route::post('/product/{id}/remove', 'ProductsController@postRemove');

        Route::post('/product/{id}/general', 'ProductsController@postUpdateGeneral');
        Route::post('/product/{id}/features', 'ProductsController@postUpdateFeatures');
        Route::post('/product/{id}/attributes', 'ProductsController@postUpdateAttributes');
        Route::post('/product/{id}/photos', 'ProductsController@postUploadPhoto');

        Route::post('/product/{id}/photos/{photo_id}/setmain', 'ProductsController@postSetMainPhoto');
        Route::post('/product/{id}/photos/{photo_id}/remove', 'ProductsController@postRemovePhoto');

        Route::get('/product/{id}/add-attributes/{group_id}', 'ProductsController@getAddAttributes');
        Route::post('/product/{id}/add-attributes/{group_id}', 'ProductsController@postAddAttributes');
        Route::get('/product/{id}/add-new-attributes/{group_id}', 'ProductsController@getAddNewAttribute');
        Route::post('/product/{id}/add-new-attributes/{group_id}', 'ProductsController@postAddNewAttribute');

        Route::get('/product/{id}/quantities', 'ProductsController@getQuantities');
        Route::post('/product/{id}/quantities/check', 'ProductsController@postCheckQuantities');

        Route::get('/quantities', 'QuantitiesController@getIndex');
        Route::get('/quantity/{id}', 'QuantitiesController@getView');
        Route::post('/quantity/{id}', 'QuantitiesController@postUpdate');
        Route::get('/quantity/{id}/remove', 'QuantitiesController@getRemove');
        Route::post('/quantity/{id}/remove', 'QuantitiesController@postRemove');

        Route::get('/discounts', 'DiscountsController@getIndex');
        Route::get('/discounts/new', 'DiscountsController@getCreate');
        Route::post('/discounts/new', 'DiscountsController@postStore');
        Route::get('/discount/{id}', 'DiscountsController@getViewGeneral');
        Route::get('/discount/{id}/products', 'DiscountsController@getViewProducts');
        Route::get('/discount/{id}/remove', 'DiscountsController@getRemove');
        Route::post('/discount/{id}/remove', 'DiscountsController@postRemove');
        Route::post('/discount/{id}/general', 'DiscountsController@postUpdateGeneral');
        Route::post('/discount/{id}/products', 'DiscountsController@postUpdateFProducts');
        Route::get('/discount/{id}/ajax-product-search', 'DiscountsController@getAjaxProductSearch');
        Route::post('/discount/{id}/add-products', 'DiscountsController@postAddProducts');

        Route::get('/categories', 'CategoriesController@getIndex');
        Route::get('/category/{id}', 'CategoriesController@getViewGeneral');
        Route::get('/category/{id}/features', 'CategoriesController@getViewFeatures');
        Route::post('/category/{id}/general', 'CategoriesController@postUpdateGeneral');
        Route::post('/category/{id}/add-feature', 'CategoriesController@postAddFeature');
        Route::post('/category/{id}/add-new-feature', 'CategoriesController@postAddNewFeature');
        Route::post('/category/{id}/feature/{feature_id}/remove', 'CategoriesController@postRemoveFeature');
        Route::get('/categories/new', 'CategoriesController@getCreate');
        Route::post('/categories/new', 'CategoriesController@postStore');
        Route::get('/category/{id}/remove', 'CategoriesController@getRemove');
        Route::post('/category/{id}/remove', 'CategoriesController@postRemove');

        Route::get('/unitgroups', 'UnitsController@getIndex');
        Route::get('/unitgroup/{id}', 'UnitsController@getViewGroupGeneral');
        Route::get('/unitgroup/{id}/units', 'UnitsController@getViewGroupUnits');
        Route::post('/unitgroup/{id}/general', 'UnitsController@postUpdateGroupGeneral');
        Route::post('/unitgroup/{id}/units/new', 'UnitsController@postGroupAddUnit');
        Route::post('/unitgroup/{id}/unit/{unit_id}/remove', 'UnitsController@postGroupRemoveUnit');

        Route::get('/unitgroups/new', 'UnitsController@getCreateGroup');
        Route::post('/unitgroups/new', 'UnitsController@postStoreGroup');
        Route::get('/unitgroup/{id}/remove', 'UnitsController@getRemoveGroup');
        Route::post('/unitgroup/{id}/remove', 'UnitsController@postRemoveGroup');

        Route::get('/orders', 'OrdersController@getIndex');
        Route::get('/orders/new', 'OrdersController@getCreate');
        Route::post('/orders/new', 'OrdersController@postStore');
        Route::get('/order/{id}', 'OrdersController@getViewGeneral');
        Route::get('/order/{id}/list', 'OrdersController@getViewList');
        Route::get('/order/{id}/remove', 'OrdersController@getRemove');
        Route::post('/order/{id}/remove', 'OrdersController@postRemove');
        Route::post('/order/{id}/general', 'OrdersController@postUpdateGeneral');

        Route::get('/users', 'UsersController@getIndex');
        Route::get('/users/new', 'UsersController@getCreate');
        Route::post('/users/new', 'UsersController@postStore');
        Route::get('/user/{id}', 'UsersController@getViewGeneral');
        Route::post('/user/{id}/general', 'UsersController@postUpdateGeneral');
        Route::get('/user/{id}/remove', 'UsersController@getRemove');
        Route::post('/user/{id}/remove', 'UsersController@postRemove');

        Route::get('/groups', 'GroupsController@getIndex');
        Route::get('/groups/new', 'GroupsController@getCreate');
        Route::post('/groups/new', 'GroupsController@postStore');
        Route::get('/group/{id}', 'GroupsController@getViewGeneral');
        Route::post('/group/{id}/general', 'GroupsController@postUpdateGeneral');
        Route::get('/group/{id}/remove', 'GroupsController@getRemove');
        Route::post('/group/{id}/remove', 'GroupsController@postRemove');


    }
);


Route::group(array(
        'prefix' => LaravelLocalization::setLocale(),
        'namespace' => 'Front',
        'before' => 'LaravelLocalizationRedirectFilter'
    ),

    function () {
        Route::get('/', 'CatalogController@electronics');

        Route::post('/ajaxAddToCart', 'CartController@postAjaxAddToCart');
        Route::post('/ajaxChangeQuantity', 'CartController@postAjaxChangeQuantity');
        Route::get('/ajaxCart', 'CartController@getAjaxCart');
        Route::get('/ajaxCartContainer', 'CartController@getAjaxCartContainer');
        Route::get('/cart', 'CartController@getCart');

        Route::post('/ajaxAddToCompare', 'CompareController@postAjaxAddToCompare');
        Route::post('/ajaxRemoveFromCompare', 'CompareController@postAjaxRemoveFromCompare');

        Route::get('/signin', 'AuthController@getSignIn');
        Route::get('/signup', 'AuthController@getSignUp');
        Route::get('/signout', 'AuthController@getSignOut');

        Route::post('/signin', 'AuthController@postSignIn');
        Route::post('/signup', 'AuthController@postSignUp');


        Route::get('electronics/{category}/compare/', 'CompareController@showCompare');

        Route::get('/electronics', 'CatalogController@electronics');
        Route::get('/test', 'CatalogController@test');
        Route::get('/electronics/{category}', 'CatalogController@electronicsCategory');
        Route::get('/electronics/{category}/ajaxProducts', 'CatalogController@getAjaxProducts');
        Route::get('/product/{id}', 'ProductController@showProduct');
    });




