<div class="row">

<!-- Profile Info and Notifications -->
<div class="col-md-6 col-sm-8 clearfix">

<ul class="user-info pull-left pull-none-xsm">

    <!-- Profile Info -->
    <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right -->

        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <?php $email = Sentry::getUser()->email; ?>
            {{ HTML::image('http://www.gravatar.com/avatar/'.md5($email), $alt = '', $attributes = array('class' => 'img-circle', 'width' => 44)) }}
            {{ $email }}
        </a>

        <!--<ul class="dropdown-menu">

            <li class="caret"></li>

            <li>
                <a href="extra-timeline.html">
                    <i class="entypo-user"></i>
                    Edit Profile
                </a>
            </li>

            <li>
                <a href="mailbox.html">
                    <i class="entypo-mail"></i>
                    Inbox
                </a>
            </li>

            <li>
                <a href="extra-calendar.html">
                    <i class="entypo-calendar"></i>
                    Calendar
                </a>
            </li>

            <li>
                <a href="#">
                    <i class="entypo-clipboard"></i>
                    Tasks
                </a>
            </li>
        </ul>-->
    </li>

</ul>

<ul class="user-info pull-left pull-right-xs pull-none-xsm">


</ul>

</div>


<!-- Raw Links -->
<div class="col-md-6 col-sm-4 clearfix hidden-xs">

    <ul class="list-inline links-list pull-right">

        <li>
            <a href="/signout">
                Çıxış <i class="entypo-logout right"></i>
            </a>
        </li>
    </ul>

</div>

</div>

<hr/>