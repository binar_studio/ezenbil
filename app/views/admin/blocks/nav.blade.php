<ul id="main-menu" class="">
<!-- add class "multiple-expanded" to allow multiple submenus to open -->
<!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
<!-- Search Bar -->
    <li class="root-level" id="search">
        <form method="get" action="">
            <input name="q" class="search-input" placeholder="Axtar..." type="text">
            <button type="submit">
                <i class="entypo-search"></i>
            </button>
        </form>
    </li>
    <li class="root-level has-sub">
        <a href="index.html">
            <i class="entypo-list"></i>
            <span>Kataloq</span>
        </a>
        <ul>
            <li>
                <a href="{{ url("/admin/products/") }}">
                    <span>Məhsullar</span>
                </a>
            </li>
            <li>
                <a href="{{ url("/admin/categories/") }}">
                    <span>Kateqoriyalar</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="root-level has-sub">
        <a href="index.html">
            <i class="entypo-layout"></i>
            <span>Anbar</span>
        </a>
        <ul>
            <li>
                <a href="{{ url("/admin/quantities/") }}">
                <span>Məhsul miqdarları</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="root-level has-sub">
        <a href="index.html">
            <i class="entypo-basket"></i>
            <span>Sifarişlər</span>
        </a>
        <ul>
            <li>
                <a href="{{ url("/admin/orders") }}">
                <span>Sifarişlər</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="root-level has-sub">
        <a href="index.html">
            <i class="entypo-chart-pie"></i>
            <span>Marketinq</span>
        </a>
        <ul>
            <li>
                <a href="{{ url("/admin/discounts") }}">
                <span>Endirimlər</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="root-level has-sub">
        <a href="index.html">
            <i class="entypo-users"></i>
            <span>İstifadəçilər</span>
        </a>
        <ul>
            <li>
                <a href="{{ url("/admin/users") }}">
                <span>İstifadəçilər</span>
                </a>
            </li>
            <li>
                <a href="{{ url("/admin/groups") }}">
                <span>İstifadəçi qrupları</span>
                </a>
            </li>
        </ul>
    </li>
    <li class="root-level has-sub">
        <a href="index.html">
            <i class="entypo-plus"></i>
            <span>Digər</span>
        </a>
        <ul>
            <li>
                <a href="{{ url("/admin/unitgroups/") }}">
                <span>Ölçü vahidləri</span>
                </a>
            </li>
        </ul>
    </li>
</ul>