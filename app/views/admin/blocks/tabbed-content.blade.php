@extends('admin.layouts.master')

@section('content')


<div class="tabs-vertical-env">

    <ul class="nav tabs-vertical">
        @yield('nav')
    </ul>

    <div class="tab-content">
        <div class="tab-pane active">

            @yield('inner-content')

        </div>
    </div>

</div>


@stop