@foreach($links as $name => $link)
<li class="{{{ $name == $active ? 'active' : '' }}}">
    <a href="{{ url($link[1]) }}">{{ $link[0] }}</a>
</li>
@endforeach