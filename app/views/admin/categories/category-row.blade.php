<tr class="odd">
    <td class="sorting_1">
        <div class="checkbox checkbox-replace neon-cb-replacement">
            <label class="cb-wrapper"><input id="chk-1" type="checkbox">

                <div class="checked"></div>
            </label>
        </div>
    </td>
    <td class="}}"><span class="col-sm-offset-{{ $node->depth }}">{{ $node->name_az }}</span></td>
    <td class=" ">
        <a href="{{ url('admin/category/'.$node->id) }}" class="btn btn-default btn-sm btn-icon icon-left">
            <i class="entypo-pencil"></i>
            Düzəliş et
        </a>

        <a href="{{ url('admin/category/'.$node->id.'/features') }}" class="btn btn-info btn-sm btn-icon icon-left">
            <i class="entypo-info"></i>
            Xüsusiyyətlər
        </a>

        <a href="{{ url('admin/category/'.$node->id.'/remove') }}" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="entypo-cancel"></i>
            Sil
        </a>
    </td>
</tr>