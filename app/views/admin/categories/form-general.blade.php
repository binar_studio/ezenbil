<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Ad (AZ)</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'name_az', $category->name_az, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Ad (RU)</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'name_ru', $category->name_ru, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Valideyn kateqoriya</label>

    <div class="col-sm-5">
        {{ Form::select('parent_id', [null=>'Yoxdur'] + $category->lists('name_az', 'id'), $category->parent_id,
        ['class' => 'select2', 'data-allow-clear' => 'true']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-ta" class="col-sm-3 control-label">Məlumat (AZ)</label>

    <div class="col-sm-5">
        {{ Form::textarea('description_az', $category->description_az, ['class' => 'form-control autogrow']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-ta" class="col-sm-3 control-label">Məlumat (RU)</label>

    <div class="col-sm-5">
        {{ Form::textarea('description_ru', $category->description_ru, ['class' => 'form-control autogrow']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-5">
        <div class="checkbox">
            <label>
                {{ Form::checkbox('visible', 1, $category->visible) }} Saytda görünməsi
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Tags Input</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'tags', $category->tags, ['class' => 'form-control tagsinput', 'data-role' => 'tagsinput']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-5">
        <div class="checkbox">
            <label>
                {{ Form::checkbox('is_banner', 0, $category->is_banner) }} Sektorun əsas səhifəsində banner olması:
            </label>
        </div>
    </div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Bannerin simvolu (FontAwesome class):</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'symbol', $category->symbol, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-ta" class="col-sm-3 control-label">Üslub rəngi</label>

    <div class="col-sm-5">
        {{ Form::text('color', $category->color, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-ta" class="col-sm-3 control-label">FontAwesome simvol sinfi</label>

    <div class="col-sm-5">
        {{ Form::text('fa_icon', $category->fa_icon, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-5">
        <button type="submit" class="btn btn-green btn-icon icon-left">Yadda saxla<i class="entypo-check"></i></button>
    </div>
</div>