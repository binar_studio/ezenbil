@extends('admin.layouts.master')

@section('content')

<h3>Kateqoriyaların siyahısı</h3>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">

    <div class="row">
        <div class="col-xs-6 col-left">
            <div>

                {{ Form::open(array('url' => '/admin/categories', 'method' => 'get')) }}
                <label>Bölməni seçin

                    {{ Form::select('sector', [null=>'Hamısı'] + $sector_list, $sector_id,
                    ['class' => 'select2', 'data-allow-clear' => 'true']) }}

                    <button type="submit" class="btn btn-blue btn-icon icon-left">Göstər<i class="entypo-search"></i></button>

                </label>

                {{ Form::close() }}
            </div>
        </div>
    </div>

    <table aria-describedby="table-2_info" class="table table-bordered table-striped datatable dataTable" id="table-2">
        <thead>
        <tr role="row">
            <th aria-label="" style="width: 50px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
                <div class="checkbox checkbox-replace neon-cb-replacement">
                    <label class="cb-wrapper"><input id="chk-1" type="checkbox">

                        <div class="checked"></div>
                    </label>
                </div>
            </th>
            <th style="width: 354px;" colspan="1" rowspan="1"
                aria-controls="table-2" tabindex="0" role="columnheader" class="sorting">Category name
            </th>
            <th style="width: 470px;" colspan="1" rowspan="1"
                aria-controls="table-2" tabindex="0" role="columnheader" class="sorting">Actions
            </th>
        </tr>
        </thead>


        <tbody aria-relevant="all" aria-live="polite" role="alert">
        <?php

        foreach($nodes as $node) renderNode($node);

        // *Very simple* recursive rendering function
        function renderNode($node) {

            echo View::make('admin.categories.category-row')->with(compact('node'))->__toString();

            if ( $node->children()->count() > 0 ) {
                foreach($node->children as $child) renderNode($child);
            }

        }
        ?>
        </tbody>
    </table>

    <div>
        <div class="form-group">
            <a class="btn btn-orange btn-icon icon-left" href="{{ url('admin/categories/new') }}">Əlavə et<i class="entypo-plus"></i></a>
        </div>
    </div>


@stop