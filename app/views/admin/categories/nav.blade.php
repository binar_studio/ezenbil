<?php

$links = [
    'general' => ['Əsas məlumatlar', 'admin/category/'.$id],
    'features' => ['Xüsusiyyətlər', 'admin/category/'.$id.'/features'],
    'remove' => ['Sil', 'admin/category/'.$id.'/remove']
];

?>
@include('admin.blocks.tabbed-nav', ['links' => $links, 'active' => $active])