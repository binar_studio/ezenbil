@extends('admin.layouts.master')

@section('content')


    {{ Form::open(array('url' => '/admin/categories/new', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
    @include('admin.categories.form-general', compact('category'))
    {{ Form::close() }}


@stop