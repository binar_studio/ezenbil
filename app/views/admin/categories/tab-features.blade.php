@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.categories.nav', ['id' => $category->id, 'active' => 'features'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Xüsusiyyətlərin dəyişdirilməsi
        </div>
    </div>

    <div class="panel-body">

        <h3>Xüsusiyyətlər</h3>

        <table class="table table-condensed table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Ad</th>
                <th>Ölçü qrupu</th>
                <th>Əməliyyatlar</th>
            </tr>
            </thead>

            <tbody>

            @forelse($category->features as $feature)
            <tr>
                <td>{{ $feature->id }}</td>
                <td>{{ $feature->global_name }}</td>
                <td>{{ isset($feature->unitGroup) ? $feature->unitGroup->name : '' }}</td>
                <td>
                    {{ Form::open(array('url' => '/admin/category/'.$category->id.'/feature/'.$feature->id.'/remove', 'role' => 'form')) }}
                    <button class="btn btn-danger btn-sm btn-icon icon-left">
                        <i class="entypo-cancel"></i>Sil
                    </button>
                    {{ Form::close() }}
                </td>
            </tr>
            @empty
            No features added
            @endforelse

            </tbody>
        </table>
        <div>
            <div class="form-group">
                <a id="btn-add-feature" class="btn btn-orange btn-icon icon-left pull-left">Əlavə et<i class="entypo-plus"></i></a>
            </div>
        </div>

        <div class="clearfix"></div>

        <div id="form-add-feature" class="hide row col-sm-12">
            <hr/>
            {{ Form::open(array('url' => '/admin/category/'.$category->id.'/add-feature', 'role' => 'form', 'class' => 'form-horizontal')) }}
            <div class="form-group">
                <label class="col-sm-3 control-label">Xüsusiyyəti seçin</label>

                <div class="col-sm-5">
                    {{ Form::select('feature_id', $featuresList, null, ['id' => 'feature_id', 'class' => 'select2', 'data-placeholder' => 'Birini seçin...']) }}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-5">
                    <button type="submit" id="btn-load-attr-list" class="btn btn-green btn-icon icon-left">Davam et<i class="entypo-check"></i></button>
                </div>
                <div class="form-group">
                    <a id="btn-add-new-feature" class="btn btn-orange btn-icon icon-left pull-right">Yeni xüsusiyyət əlavə et<i class="entypo-plus"></i></a>
                </div>
            </div>
            {{ Form::close() }}
        </div>

        <div id="form-add-new-feature" class="hide row">

            {{ Form::open(array('url' => '/admin/category/'.$category->id.'/add-new-feature', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Ad (AZ)</label>

                <div class="col-sm-5">
                    {{ Form::input('text', 'name_az', null, ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Ad (RU)</label>

                <div class="col-sm-5">
                    {{ Form::input('text', 'name_ru', null, ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Fərqləndirici</label>

                <div class="col-sm-5">
                    {{ Form::input('text', 'descriptor', null, ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Hə/Yox tiplidirmi?</label>

                <div class="col-sm-5">
                    {{ Form::checkbox('is_boolean', true, 0, ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Ölçü qrupu</label>

                <div class="col-sm-5">
                    {{ Form::select('unit_group_id', [null=>'Boş'] + $unitGroupList, null, ['id' => 'unit_group_id', 'class' => 'select2', 'data-placeholder' => 'Birini seçin...']) }}
                </div>

            </div>

            <div>
                <div class="form-group">
                    <button type="submit" class="btn btn-green btn-icon icon-left pull-right">Yadda saxla<i class="entypo-check"></i></button>
                </div>
            </div>
            {{ Form::close() }}

        </div>

        <script type="application/javascript">
            $(document).ready(function() {
                $("#btn-add-feature").click(function(){
                    $("#form-add-feature").removeClass('hide');
                });

                $("#btn-add-new-feature").click(function(){
                    $("#form-add-new-feature").removeClass('hide');
                });
            });
        </script>

    </div>

</div>
@stop