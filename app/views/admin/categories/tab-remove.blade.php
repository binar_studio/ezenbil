@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.categories.nav', ['id' => $category->id, 'active' => 'remove'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Kateqoriyanın silinməsi
        </div>
    </div>

    <div class="panel-body">

        <p>Bu kateqoriyanı silmək istədiyinizə əminsinizmi?</p>

        <p class="bs-example">
            <a href="" class="btn btn-green btn-icon icon-left">
                İmtina et
                <i class="entypo-check"></i>
            </a>

            {{ Form::open(array('url' => '/admin/category/'.$category->id.'/remove', 'role' => 'form', 'method' => 'post')) }}

            <button type="submit" class="btn btn-red btn-icon icon-left">
                Sil
                <i class="entypo-cancel"></i>
            </button>
            {{ Form::close() }}

        </p>
    </div>

</div>
@stop