{{ Form::open(array('url' => '/admin/discount/'.$id.'/add-products/', 'role' => 'form')) }}
<table class="table table-condensed table-bordered table-hover table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Ad</th>
    </tr>
    </thead>

    <tbody>

    @forelse($products as $p)
    <tr>
        <td>{{ Form::checkbox('product-'.$p->id) }}</td>
        <td>{{ $p->name_az }}</td>
    </tr>
    @empty
    No products found added
    @endforelse

    </tbody>
</table>
<div>

    <div class="form-group">
        <button type="submit" class="btn btn-green btn-icon icon-left pull-right">Tətbiq et<i class="entypo-check"></i></button>
    </div>
</div>
{{ Form::close() }}