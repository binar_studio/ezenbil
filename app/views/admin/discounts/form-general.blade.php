<?php

$type = isset($type) ? $type : $discount->type;

?>


<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Ad</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'title', $discount->title, ['class' => 'form-control']) }}
    </div>
</div>

@if($discount->id)
<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">İstifadə olunma sayı</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'used', $discount->used, ['class' => 'form-control']) }}
    </div>
</div>
@else
    {{ Form::input('hidden', 'type', $type) }}
@endif

@if($type == 'percentage')
<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Endirim faizi</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'percentage', $discount->percentage, ['class' => 'form-control']) }}
    </div>
</div>
@endif

@if($type == 'nplus1')
<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Alış miqdarı</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'nplusm_n', $discount->nplusm_n, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Hədiyyə olunan miqdar miqdarı</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'nplusm_m', $discount->nplusm_m, ['class' => 'form-control']) }}
    </div>
</div>
@endif

<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">İstifadə sayına limit</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'limit', $discount->limit, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Başlanma tarixi</label>

    <div class="col-sm-5">
        {{ Form::input('date', 'starts', $discount->starts, ['class' => 'form-control datetimepicker']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Bitmə tarixi</label>

    <div class="col-sm-5">
        {{ Form::input('date', 'ends', $discount->ends, ['class' => 'form-control datetimepicker']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-5">
        <button type="submit" class="btn btn-green btn-icon icon-left">Yadda saxla<i class="entypo-check"></i></button>
    </div>
</div>