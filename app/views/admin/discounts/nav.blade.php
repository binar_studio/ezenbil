<?php

$links = [
    'general' => ['Əsas məlumatlar', 'admin/discount/'.$id],
    'products' => ['Məhsullar', 'admin/discount/'.$id.'/products'],
    'remove' => ['Sil', 'admin/discount/'.$id.'/remove']
];

?>
@include('admin.blocks.tabbed-nav', ['links' => $links, 'active' => $active])