@extends('admin.layouts.master')

@section('content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Yeni endirimin əlavəsi
        </div>
    </div>

    <div class="panel-body">

        <a class="btn btn-green btn-icon icon-left" href="{{ url('admin/discounts/new?type=percentage') }}">
            Faiz endirimi <i class="entypo-tag"></i>
        </a>

        <a class="btn btn-green btn-icon icon-left" href="{{ url('admin/discounts/new?type=nplus1') }}">
            N+M endirimi <i class="entypo-plus"></i>
        </a>

    </div>

</div>
@stop