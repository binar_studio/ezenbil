@extends('admin.layouts.master')

@section('content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Yeni faiz endirimininin əlavəsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/discounts/new', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}

        @include('admin.discounts.form-general', ['discount' => $discount, 'type' => $type])

        {{ Form::close() }}

    </div>

</div>
<script type="application/javascript">
    // TODO date time picker
</script>
@stop