@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.discounts.nav', ['id' => $discount->id, 'active' => 'general'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Əsas məlumatların dəyişdirilməsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/discount/'.$discount->id.'/general', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
        @include('admin.discounts.form-general', ['discount' => $discount])
        {{ Form::close() }}

    </div>

</div>
@stop