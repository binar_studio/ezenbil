@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.discounts.nav', ['id' => $discount->id, 'active' => 'features'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Əlaqədar məhsullar
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/discount/'.$discount->id.'/products', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
        <table class="table table-condensed table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>Ad</th>
            </tr>
            </thead>

            <tbody>
            @forelse($discount->products as $p)
            
                <tr>
                    <td>{{ $p->name_az }}</td>
                </tr>

            @empty
            Bu kampaniya hələ heç bir məhsula aid edilməyib
            @endforelse
            </tbody>
        </table>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-5">
                <button type="submit" class="btn btn-green btn-icon icon-left">Yadda saxla<i class="entypo-check"></i></button>
            </div>
        </div>
        {{ Form::close() }}

        <div class="form-group">
            <a id="btn-add-products" class="btn btn-orange btn-icon icon-left pull-left">Əlavə et<i class="entypo-plus"></i></a>
        </div>

        <div class="clearfix"></div>

        <div id="form-add-products" class="hide row col-sm-12">
            <hr/>
            <form role="form" class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Məhsulun adını yazın</label>

                    <div class="col-sm-5">
                        {{ Form::input('text', 'search', '',  ['id' => 'search-term', 'data-placeholder' => 'Axtarış sözünü yazın...']) }}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <a id="btn-load-product-list" class="btn btn-green btn-icon icon-left">Davam et<i class="entypo-check"></i></a>
                    </div>
                </div>
            </form>
        </div>

        <div id="search-results-area">

        </div>

    </div>

</div>

<script type="application/javascript">
    $(document).ready(function() {
        $("#btn-add-products").click(function(){
            $("#form-add-products").removeClass('hide');
        });


        $("#btn-load-product-list").click(function() {
            var search = $("#search-term").val();
            var d_id = "{{ $discount->id }}";

            $("#search-results-area").load(
                "{{ url('/') }}" + "/admin/discount/" + d_id + "/ajax-product-search/?search=" + search
            );
        });
    });
</script>
@stop