<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Ad</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'name', $group->name, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-5">
        <button type="submit" class="btn btn-green btn-icon icon-left">Yadda saxla<i class="entypo-check"></i></button>
    </div>
</div>