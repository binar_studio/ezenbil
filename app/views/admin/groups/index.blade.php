@extends('admin.layouts.master')

@section('content')

<h3>Qrupların siyahısı</h3>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
<table aria-describedby="table-2_info" class="table table-bordered table-striped datatable dataTable" id="table-2">
<thead>
<tr role="row">
    <th>
        <div class="checkbox checkbox-replace neon-cb-replacement">
            <label class="cb-wrapper"><input id="chk-1" type="checkbox">

                <div class="checked"></div>
            </label>
        </div>
    </th>
    <th>Ad</th>
    <th>Əməliyyatlar</th>
</tr>
</thead>


<tbody aria-relevant="all" aria-live="polite" role="alert">
@foreach($groups as $group)
<tr class="odd">
    <td class="sorting_1">
        <div class="checkbox checkbox-replace neon-cb-replacement">
            <label class="cb-wrapper"><input id="chk-1" type="checkbox">

                <div class="checked"></div>
            </label>
        </div>
    </td>
    <td class=" ">{{ $group->name }}</td>
    <td class=" ">
        <a href="{{ url("admin/group/$group->id/") }}" class="btn btn-default btn-sm btn-icon icon-left">
            <i class="entypo-pencil"></i>
            Ətraflı
        </a>

        <a href="{{ url("admin/users?group_id=$group->id") }}" class="btn btn-info btn-sm btn-icon icon-left">
            <i class="entypo-info"></i>
            İstifadəçilər
        </a>

        <a href="{{ url("admin/group/$group->id/remove") }}" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="entypo-cancel"></i>
            Sil
        </a>

    </td>
</tr>
@endforeach
</tbody>
</table>
<div class="row">
    <div class="col-xs-6 col-left">
        <div id="table-2_info" class="dataTables_info">Showing {{ $groups->getFrom(); }} to {{ $groups->getTo(); }} of {{ $groups->getTotal(); }} entries</div>
    </div>
    <div class="col-xs-6 col-right">
        <div class="dataTables_paginate paging_bootstrap">
            {{ $groups->links() }}
        </div>
    </div>
</div>
</div>

<div>
    <div class="form-group">
        <a class="btn btn-orange btn-icon icon-left" href="{{ url('admin/groups/new') }}">Əlavə et<i class="entypo-plus"></i></a>
    </div>
</div>

@stop