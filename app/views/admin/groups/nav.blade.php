<?php

$links = [
    'general' => ['Əsas məlumatlar', 'admin/group/'.$id],
    //'list' => ['İstifadəçilər', 'admin/users?group_id='.$id],
    'remove' => ['Sil', 'admin/group/'.$id.'/remove']
];

?>
@include('admin.blocks.tabbed-nav', ['links' => $links, 'active' => $active])