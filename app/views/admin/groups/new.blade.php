@extends('admin.layouts.master')

@section('content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Yeni qrupun əlavəsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/groups/new', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
        @include('admin.groups.form-general', ['group' => $group])
        {{ Form::close() }}

    </div>

</div>
@stop