@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.groups.nav', ['id' => $group->id, 'active' => 'general'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Əsas məlumatların dəyişdirilməsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/group/'.$group->id.'/general', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
        @include('admin.groups.form-general', ['group' => $group])
        {{ Form::close() }}

    </div>

</div>
@stop