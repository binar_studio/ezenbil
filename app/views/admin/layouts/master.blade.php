<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="e-Zənbil Admin Panel"/>
    <meta name="author" content=""/>

    <title>Admin Panel | e-Zənbil</title>

    {{ HTML::style('back/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css') }}
    {{ HTML::style('back/css/font-icons/entypo/css/entypo.css') }}
    {{ HTML::style('http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic') }}
    {{ HTML::style('back/css/bootstrap.css') }}
    {{ HTML::style('back/css/neon-core.css') }}
    {{ HTML::style('back/css/neon-theme.css') }}
    {{ HTML::style('back/css/neon-forms.css') }}
    {{ HTML::style('back/css/custom.css') }}
    {{ HTML::style('back/js/select2/select2-bootstrap.css') }}
    {{ HTML::style('back/js/select2/select2.css') }}

    {{ HTML::script('back/js/jquery-1.11.0.min.js') }}

    {{ HTML::style('back/js/dropzone/dropzone.css') }}


    {{ HTML::style('back/css/custom.css') }}

    {{ HTML::script('back/js/jquery-1.11.0.min.js') }}

    <!--[if lt IE 9]>{{ HTML::script('back/js/ie8-responsive-file-warning.js') }}<![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>
<body class="page-body" data-url="http://neon.dev">

<div class="page-container">
    <!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    <div class="sidebar-menu">


        <header class="logo-env">

            <!-- logo -->
            <div class="logo">
                <a href="index.html">
                    {{ HTML::image('back/images/logo@2x.png', $alt = '', $attributes = array('width' => '120')) }}
                </a>
            </div>

            <!-- logo collapse icon -->

            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon with-animation">
                    <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        @include('admin.blocks.nav')


    </div>
    <div class="main-content">

        @include('admin.blocks.header')

        @if(Session::has('message'))

            <?php
            $message = Session::get('message');

            $text = null;
            $class = null;

            if (strpos($message, '|') !== false) {
                list($class, $text) = explode('|', $message);
            } else {
                $text = $message;
                $class = 'info';
            }

            ?>

            <div class="alert {{ 'alert-'.$class }}">
                {{ $text }}
            </div>
        @endif

        @yield('content')

        @include('admin.blocks.footer')

    </div>
</div>



<!-- Bottom Scripts -->
{{ HTML::script('back/js/gsap/main-gsap.js') }}
{{ HTML::script('back/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js') }}
{{ HTML::script('back/js/bootstrap.js') }}
{{ HTML::script('back/js/joinable.js') }}
{{ HTML::script('back/js/resizeable.js') }}
{{ HTML::script('back/js/neon-api.js') }}
{{ HTML::script('back/js/bootstrap-switch.min.js') }}
{{ HTML::script('back/js/neon-custom.js') }}
{{ HTML::script('back/js/neon-demo.js') }}
{{ HTML::script('back/js/select2/select2.min.js') }}
{{ HTML::script('back/js/bootstrap-tagsinput.min.js') }}
{{ HTML::script('back/js/dropzone/dropzone.js') }}
{{ HTML::script('back/js/jcrop/jquery.Jcrop.js') }}

</body>
</html>