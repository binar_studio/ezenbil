@extends('admin.layouts.master')

@section('content')

<h3>Sifarişlərin siyahısı</h3>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
<table aria-describedby="table-2_info" class="table table-bordered table-striped datatable dataTable" id="table-2">
<thead>
<tr role="row">
    <th>
        <div class="checkbox checkbox-replace neon-cb-replacement">
            <label class="cb-wrapper"><input id="chk-1" type="checkbox">

                <div class="checked"></div>
            </label>
        </div>
    </th>
    <th>Müştəri</th>
    <th>Sektor</th>
    <th>Status</th>
    <th>Mal sayı</th>
    <th>Əməliyyatlar</th>
</tr>
</thead>


<tbody aria-relevant="all" aria-live="polite" role="alert">
@foreach($orders as $o)
<tr class="odd">
    <td class="sorting_1">
        <div class="checkbox checkbox-replace neon-cb-replacement">
            <label class="cb-wrapper"><input id="chk-1" type="checkbox">

                <div class="checked"></div>
            </label>
        </div>
    </td>
    <td class=" ">{{ $o->user->first_name.' '.$o->user->last_name }}</td>
    <td class=" ">{{ Config::get("app.sections.byid.$o->sector_id")['slug'] }}</td>
    <td class=" ">{{ trans("cart.status.$o->status") }}</td>
    <td class=" ">{{ $o->lineItems->count() }}</td>
    <td class=" ">
        <a href="{{ url("admin/order/$o->id/") }}" class="btn btn-default btn-sm btn-icon icon-left">
            <i class="entypo-pencil"></i>
            Ətraflı
        </a>

        <a href="{{ url("admin/order/$o->id/list") }}" class="btn btn-info btn-sm btn-icon icon-left">
            <i class="entypo-info"></i>
            Səbətdəkilər
        </a>

        <a href="{{ url("admin/order/$o->id/remove") }}" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="entypo-cancel"></i>
            Sil
        </a>

    </td>
</tr>
@endforeach
</tbody>
</table>
<div class="row">
    <div class="col-xs-6 col-left">
        <div id="table-2_info" class="dataTables_info">Showing {{ $orders->getFrom(); }} to {{ $orders->getTo(); }} of {{ $orders->getTotal(); }} entries</div>
    </div>
    <div class="col-xs-6 col-right">
        <div class="dataTables_paginate paging_bootstrap">
            {{ $orders->links() }}
        </div>
    </div>
</div>
</div>

<div>
    <div class="form-group">
        <a class="btn btn-orange btn-icon icon-left" href="{{ url('admin/orders/new') }}">Əlavə et<i class="entypo-plus"></i></a>
    </div>
</div>

@stop