<?php

$links = [
    'general' => ['Əsas məlumatlar', 'admin/order/'.$id],
    'list' => ['Səbətdəkilər', 'admin/order/'.$id.'/list'],
    'remove' => ['Sil', 'admin/order/'.$id.'/remove']
];

?>
@include('admin.blocks.tabbed-nav', ['links' => $links, 'active' => $active])