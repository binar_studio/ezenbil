@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.orders.nav', ['id' => $order->id, 'active' => 'general'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Əsas məlumatların dəyişdirilməsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/order/'.$order->id.'/general', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
        @include('admin.orders.form-general', ['order' => $order, 'statusList' => $statusList])
        {{ Form::close() }}

    </div>

</div>
@stop