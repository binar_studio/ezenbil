@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.orders.nav', ['id' => $order->id, 'active' => 'remove'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Məhsulun silinməsi
        </div>
    </div>

    <div class="panel-body">

        <p>Bu məhsulu silmək istədiyinizə əminsinizmi?</p>

        <p class="bs-example">

            {{ Form::open(array('url' => '/admin/order/'.$order->id, 'role' => 'form', 'method' => 'get')) }}
            <button type="submit" class="btn btn-green btn-icon icon-left">
                İmtina et
                <i class="entypo-cancel"></i>
            </button>
            {{ Form::close() }}

            <br/>

            {{ Form::open(array('url' => '/admin/order/'.$order->id.'/remove', 'role' => 'form', 'method' => 'post')) }}
            <button type="submit" class="btn btn-red btn-icon icon-left">
                Sil
                <i class="entypo-check"></i>
            </button>
            {{ Form::close() }}

        </p>
    </div>

</div>
@stop