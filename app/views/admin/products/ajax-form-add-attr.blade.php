{{ Form::open(array('url' => '/admin/product/'.$id.'/add-attributes/'.$group_id, 'role' => 'form')) }}
<table class="table table-condensed table-bordered table-hover table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Ad</th>
    </tr>
    </thead>

    <tbody>

    @forelse($attributes as $attr)
    <tr>
        <td>{{ Form::checkbox('attr-'.$attr->id) }}</td>
        <td>{{ $attr->name_az }}</td>
    </tr>
    @empty
    No attributes added
    @endforelse

    </tbody>
</table>
<div>
    <div class="form-group">
        <a id="btn-add-new-attr" class="btn btn-orange btn-icon icon-left pull-right">Yeni atribut əlavə et<i class="entypo-plus"></i></a>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-green btn-icon icon-left pull-right">Yadda saxla<i class="entypo-check"></i></button>
    </div>
</div>
{{ Form::close() }}

<div id="form-add-new-attr" class="hide row">

    {{ Form::open(array('url' => '/admin/product/'.$id.'/add-new-attributes/'.$group_id, 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}

    <div class="form-group">
        <label for="field-1" class="col-sm-3 control-label">Ad (AZ)</label>

        <div class="col-sm-5">
            {{ Form::input('text', 'name_az', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <label for="field-1" class="col-sm-3 control-label">Ad (RU)</label>

        <div class="col-sm-5">
            {{ Form::input('text', 'name_ru', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div class="form-group">
        <label for="field-1" class="col-sm-3 control-label">Rəng</label>

        <div class="col-sm-5">
            {{ Form::input('text', 'color', null, ['class' => 'form-control']) }}
        </div>
    </div>

    <div>
        <div class="form-group">
            <button type="submit" class="btn btn-green btn-icon icon-left pull-right">Yadda saxla<i class="entypo-check"></i></button>
        </div>
    </div>
    {{ Form::close() }}

</div>

<script type="application/javascript">
    $(document).ready(function() {

        $("#btn-add-new-attr").click(function(){
            $("#form-add-new-attr").removeClass('hide');
        });

    });
</script>