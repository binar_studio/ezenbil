<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Ad (AZ)</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'name_az', $product->name_az, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Ad (RU)</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'name_ru', $product->name_ru, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label">Kateqoriya</label>

    <div class="col-sm-5">
        {{ Form::select('category_id', $categoryList, $product->category_id, ['class' => 'select2', 'data-placeholder' => 'Bir kateqoriya seçin...']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-ta" class="col-sm-3 control-label">Məlumat (AZ)</label>

    <div class="col-sm-5">
        {{ Form::textarea('description_az', $product->description_az, ['class' => 'form-control autogrow']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-ta" class="col-sm-3 control-label">Məlumat (RU)</label>

    <div class="col-sm-5">
        {{ Form::textarea('description_ru', $product->description_ru, ['class' => 'form-control autogrow']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-ta" class="col-sm-3 control-label">Qiymət</label>

    <div class="col-sm-5">
        {{ Form::text('price', $product->price, ['class' => 'form-control']) }}
    </div>
</div>

@if($product->id)
<div class="form-group">
    <div class="col-sm-offset-3 col-sm-5">
        <div class="checkbox">
            <label>
                {{ Form::checkbox('visible', 1, $product->visible) }} Saytda görünməsi
            </label>
        </div>
    </div>
</div>
@endif

<div class="form-group">
    <label class="col-sm-3 control-label">Tags Input</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'tags', $product->tags, ['class' => 'form-control tagsinput', 'data-role' => 'tagsinput']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-5">
        <button type="submit" class="btn btn-green btn-icon icon-left">Yadda saxla<i class="entypo-check"></i></button>
    </div>
</div>