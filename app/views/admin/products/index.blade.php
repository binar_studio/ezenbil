@extends('admin.layouts.master')

@section('content')

<h3>List of products</h3>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
<table aria-describedby="table-2_info" class="table table-bordered table-striped datatable dataTable" id="table-2">
<thead>
<tr role="row">
    <th aria-label="" style="width: 50px;" colspan="1" rowspan="1" role="columnheader" class="sorting_disabled">
        <div class="checkbox checkbox-replace neon-cb-replacement">
            <label class="cb-wrapper"><input id="chk-1" type="checkbox">

                <div class="checked"></div>
            </label>
        </div>
    </th>
    <th aria-label="Student Name: activate to sort column ascending" style="width: 354px;" colspan="1" rowspan="1"
        aria-controls="table-2" tabindex="0" role="columnheader" class="sorting">Product name
    </th>
    <th aria-label="Average Grade: activate to sort column ascending" style="width: 194px;" colspan="1" rowspan="1"
        aria-controls="table-2" tabindex="0" role="columnheader" class="sorting">Category
    </th>
    <th aria-label="Curriculum / Occupation: activate to sort column ascending" style="width: 433px;" colspan="1"
        rowspan="1" aria-controls="table-2" tabindex="0" role="columnheader" class="sorting">Price
    </th>
    <th aria-label="Actions: activate to sort column ascending" style="width: 470px;" colspan="1" rowspan="1"
        aria-controls="table-2" tabindex="0" role="columnheader" class="sorting">Actions
    </th>
</tr>
</thead>


<tbody aria-relevant="all" aria-live="polite" role="alert">
@foreach($products as $p)
<tr class="odd">
    <td class="sorting_1">
        <div class="checkbox checkbox-replace neon-cb-replacement">
            <label class="cb-wrapper"><input id="chk-1" type="checkbox">

                <div class="checked"></div>
            </label>
        </div>
    </td>
    <td class=" ">{{ $p->name_az }}</td>
    <td class=" ">{{ $p->category->name_az }}</td>
    <td class=" ">{{ $p->price }}</td>
    <td class=" ">
        <a href="{{ url("admin/product/$p->id/") }}" class="btn btn-default btn-sm btn-icon icon-left">
            <i class="entypo-pencil"></i>
            Ətraflı
        </a>

        <a href="{{ url("admin/product/$p->id/features") }}" class="btn btn-info btn-sm btn-icon icon-left">
            <i class="entypo-info"></i>
            Xüsusiyyətlər
        </a>

        <a href="{{ url("admin/product/$p->id/remove") }}" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="entypo-cancel"></i>
            Sil
        </a>

    </td>
</tr>
@endforeach
</tbody>
</table>
<div class="row">
    <div class="col-xs-6 col-left">
        <div id="table-2_info" class="dataTables_info">Showing {{ $products->getFrom(); }} to {{ $products->getTo(); }} of {{ $products->getTotal(); }} entries</div>
    </div>
    <div class="col-xs-6 col-right">
        <div class="dataTables_paginate paging_bootstrap">
            {{ $products->links() }}
        </div>
    </div>
</div>
</div>

<div>
    <div class="form-group">
        <a class="btn btn-orange btn-icon icon-left" href="{{ url('admin/products/new') }}">Əlavə et<i class="entypo-plus"></i></a>
    </div>
</div>

@stop