<?php

$links = [
    'general' => ['Əsas məlumatlar', 'admin/product/'.$id],
    'features' => ['Xüsusiyyətlər', 'admin/product/'.$id.'/features'],
    'attributes' => ['Atributlar', 'admin/product/'.$id.'/attributes'],
    'photos' => ['Şəkillər', 'admin/product/'.$id.'/photos'],
    'quantities' => ['Anbar məlumatları', 'admin/product/'.$id.'/quantities'],
    'remove' => ['Sil', 'admin/product/'.$id.'/remove']
];

?>
@include('admin.blocks.tabbed-nav', ['links' => $links, 'active' => $active])