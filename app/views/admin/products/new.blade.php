@extends('admin.layouts.master')

@section('content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Yeni məhsulun əlavəsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/products/new', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
        @include('admin.products.form-general', ['product' => $product])
        {{ Form::close() }}

    </div>

</div>
@stop