<h3>Atributlar</h3>


{{ Form::open(array('url' => '/admin/product/'.$product->id.'/attributes', 'role' => 'form')) }}
<table class="table table-condensed table-bordered table-hover table-striped">
    <thead>
    <tr>
        <th>#</th>
        <th>Ad</th>
        <th>Rəng</th>
        <th>Qiymət təsir</th>
    </tr>
    </thead>

    <tbody>

        @forelse($product->attributes as $attr)
        <tr>
            <td>{{ Form::checkbox('attr-'.$attr->id.'-check', 1) }}</td>
            <td>{{ $attr->name_az }}</td>
            <td>{{ $attr->color }}</td>
            <td>{{ Form::input('text', 'attr-'.$attr->id.'-price-impact', $attr->pivot->price_impact, ['class' => 'form-control']) }}</td>
        </tr>
        @empty
        <tr>
            <td colspan=5>No attributes added</td>
        </tr>
        @endforelse

    </tbody>
</table>
<div>
    <div class="form-group">
        <a id="btn-add-attr" class="btn btn-orange btn-icon icon-left pull-left">Əlavə et<i class="entypo-plus"></i></a>
    </div>
    <div class="form-group">
        <button type="submit" name="save" value="save" class="btn btn-green btn-icon icon-left pull-right">Yadda saxla<i class="entypo-check"></i></button>
    </div>
    <div class="form-group">
        <button type="submit" name="remove" value="remove" class="btn btn-red btn-icon icon-left pull-right">Sil<i class="entypo-cancel"></i></button>
    </div>
</div>
{{ Form::close() }}

<div class="clearfix"></div>

<div id="form-add-attr" class="hide row col-sm-12">
    <hr/>
    <form role="form" class="form-horizontal">
        <div class="form-group">
            <label class="col-sm-3 control-label">Atribut qrupunu seçin</label>

            <div class="col-sm-5">
                {{ Form::select('group_id', $attrGroups, null, ['id' => 'group_id', 'class' => 'select2', 'data-placeholder' => 'Bir qrup seçin...']) }}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-5">
                <a id="btn-load-attr-list" class="btn btn-green btn-icon icon-left">Davam et<i class="entypo-check"></i></a>
            </div>
        </div>
    </form>
</div>

<div id="attr-table-area">

</div>

<script type="application/javascript">
    $(document).ready(function() {
        $("#btn-add-attr").click(function(){
            $("#form-add-attr").removeClass('hide');
        });


        $("#btn-load-attr-list").click(function() {
            var group_id = $("#group_id").val();
            var p_id = "{{ $product->id }}";

            $("#attr-table-area").load("{{ url('/') }}" + "/admin/product/" + p_id + "/add-attributes/" + group_id);
        });
    });
</script>