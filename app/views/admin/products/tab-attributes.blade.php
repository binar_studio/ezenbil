@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.products.nav', ['id' => $product->id, 'active' => 'attributes'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Atributların dəyişdirilməsi
        </div>
    </div>

    <div class="panel-body">

        @include('admin.products.panel-attributes', ['product' => $product])

    </div>

</div>
@stop