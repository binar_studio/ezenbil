@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.products.nav', ['id' => $product->id, 'active' => 'features'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Xüsusiyyətlərin dəyişdirilməsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/product/'.$product->id.'/features', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
        <table class="table table-condensed table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>Ad</th>
                <th>Qiymət</th>
                <th>Vahid</th>
            </tr>
            </thead>

            <tbody>
            @forelse($product->features as $feature)
            
                <tr>
                    <td>{{ $feature->name_az }}</td>
                    <td>
                        @if($feature->is_boolean)

                        {{ Form::checkbox('feature-'.$feature->id, true, $feature->pivot->value, ['class' => 'form-control']) }}

                        @elseif($feature->has_options)

                        <div class="row">
                            <div class="col-md-10">
                                {{ Form::select('feature-'.$feature->id.'-option',
                                [null=>'Boş'] + $feature->options()->lists('name_az', 'id'),
                                $feature->pivot->option_id, ['class' => 'select2', 'data-placeholder' => 'Birini seçin...']) }}
                            </div>
                            <div class="col-md-2">
                                <a class="btn btn-orange" data-toggle="collapse"
                                   data-target="#feature-{{ $feature->id }}-option-new-form"><i class="entypo-plus "></i></a>
                            </div>
                        </div>


                        <div id="feature-{{ $feature->id }}-option-new-form" class="collapse out">
                            Ad (AZ)
                            {{ Form::text('feature-'.$feature->id.'-option-new-name_az', "", ['class' => 'form-control']) }}
                            Ad (RU)
                            {{ Form::text('feature-'.$feature->id.'-option-new-name_ru', "", ['class' => 'form-control']) }}
                        </div>

                        @else

                        {{ Form::input('text', 'feature-'.$feature->id, $feature->pivot->value, ['class' =>
                        'form-control']) }}

                        @endif

                    </td>
                    <td>
                        @if($feature->unit_group_id)

                        {{ Form::select('feature-'.$feature->id.'-unit',
                        [null=>'Boş'] + $feature->unit_group->units()->lists('name_az', 'id'),
                        $feature->pivot->unit_id, ['class' => 'select2', 'data-placeholder' => 'Birini seçin...']) }}

                        @endif
                    </td>
                </tr>

            @empty
            No features added
            @endforelse
            </tbody>
        </table>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-5">
                <button type="submit" class="btn btn-green btn-icon icon-left">Yadda saxla<i class="entypo-check"></i></button>
            </div>
        </div>
        {{ Form::close() }}

    </div>

</div>
@stop