@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.products.nav', ['id' => $product->id, 'active' => 'general'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Əsas məlumatların dəyişdirilməsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/product/'.$product->id.'/general', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
        @include('admin.products.form-general', ['product' => $product, 'categoryList' => $categoryList])
        {{ Form::close() }}

    </div>

</div>
@stop