@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.products.nav', ['id' => $product->id, 'active' => 'photos'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Şəkillər
        </div>
    </div>

    <div class="panel-body">

        <!-- Gallery -->

        <div class="gallery-env">

            <div class="row">

                @foreach($product->photos as $photo)
                <div class="col-sm-2 col-xs-4" data-tag="1d" data-photo-id="{{ $photo->id }}">

                    <article class="image-thumb">



                        <a href="#" class="image">
                            <img src="{{ url($photo->filename) }}">

                            @if($product->main_photo_id == $photo->id)
                            <span class="badge badge-secondary badge-roundless">Əsas şəkil</span>
                            @endif
                        </a>

                        <div class="image-options">
                            <a href="#" class="edit"><i class="entypo-pencil"></i></a>
                            <a href="#" class="setmain"><i class="entypo-check"></i></a>
                            <a href="#" class="delete"><i class="entypo-cancel"></i></a>
                        </div>

                    </article>

                </div>
                @endforeach

            </div>

        </div>

        <!-- Album Image Settings Modal -->
        <div class="modal fade" id="album-image-options">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="gallery-image-edit-env">
                        <img src="{{ url('back/images/sample-crop-2.png') }}" class="img-responsive" />

                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    </div>

                    <div class="modal-body">


                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label for="field-1" class="control-label">Title</label>

                                    <input type="text" class="form-control" id="field-1" placeholder="Enter image title">
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label for="field-1" class="control-label">Description</label>

                                    <textarea class="form-control autogrow" id="field-2" placeholder="Enter image description" style="min-height: 80px;"></textarea>
                                </div>

                            </div>
                        </div>



                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-success btn-icon">
                            <i class="entypo-check"></i>
                            Apply Changes
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <!-- End of galery -->



        <!-- <div id="myId"></div>

        <script>
            $(document).ready(function() {
                $("div#myId").dropzone({ url: "/file/post" });
            });
        </script>-->

        <form action="{{ url("admin/product/$product->id/photos") }}" class="dropzone" id="product-protos-dropzone">
            <div class="fallback">
                <input name="file" type="file" multiple />
            </div>
        </form>

        <div id="dze_info" class="hidden">

            <br />
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="panel-title">Dropzone Uploaded Files Info</div>
                </div>

                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th width="40%">File name</th>
                        <th width="15%">Size</th>
                        <th width="15%">Type</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="4"></td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>


    </div>

</div>

<script type="text/javascript">
    $(document).ready(function($)
    {
        $(".gallery-env").on("click", ".image-thumb .image-options a.delete", function(ev)
        {
            ev.preventDefault();


            var $image = $(this).closest('[data-tag]');

            var p_id = "{{ $product->id }}";

            var ajax_url = "{{ url() }}" + "/admin/product/" + p_id + "/photos/" + $image.attr('data-photo-id') + "/remove";

            $.ajax({
                url: ajax_url,
                type: 'POST',
                dataType: 'json'
            }).done(function( data ) {
                console.log(data);

                if (data.status == "ok") {

                    console.log(data.message);
                } else {

                    alert(data.message);
                }
            });

            var t = new TimelineLite({
                onComplete: function()
                {
                    $image.slideUp(function()
                    {
                        $image.remove();
                    });
                }
            });

            $image.addClass('no-animation');

            t.append( TweenMax.to($image, .2, {css: {scale: 0.95}}) );
            t.append( TweenMax.to($image, .5, {css: {autoAlpha: 0, transform: "translateX(100px) scale(.95)"}}) );

        }).on("click", ".image-thumb .image-options a.edit", function(ev)
        {
            ev.preventDefault();

            // This will open sample modal
            $("#album-image-options").modal('show');

            // Sample Crop Instance
            var image_to_crop = $("#album-image-options img"),
                img_load = new Image();

            img_load.src = image_to_crop.attr('src');
            img_load.onload = function()
            {
                if(image_to_crop.data('loaded'))
                    return false;

                image_to_crop.data('loaded', true);

                image_to_crop.Jcrop({
                    //boxWidth: $("#album-image-options").outerWidth(),
                    boxWidth: 580,
                    boxHeight: 385,
                    onSelect: function(cords)
                    {
                        // you can use these vars to save cropping of the image coordinates
                        var h = cords.h,
                            w = cords.w,

                            x1 = cords.x,
                            x2 = cords.x2,

                            y1 = cords.w,
                            y2 = cords.y2;

                    }
                }, function()
                {
                    var jcrop = this;

                    jcrop.animateTo([600, 400, 100, 150]);
                });
            }
        }).on("click", ".image-thumb .image-options a.setmain", function(ev)
        {
            ev.preventDefault();


            var $image = $(this).closest('[data-tag]');
            var p_id = "{{ $product->id }}";

            var ajax_url = "{{ url() }}" + "/admin/product/" + p_id + "/photos/" + $image.attr('data-photo-id') + "/setmain";

            $.ajax({
                url: ajax_url,
                type: 'POST',
                dataType: 'json'
            }).done(function( data ) {
                console.log(data);

                if (data.status == "ok") {

                    alert(data.message);
                } else {

                    alert(data.message);
                }
            });

        });


    });
</script>
@stop