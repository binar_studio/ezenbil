@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.products.nav', ['id' => $id, 'active' => 'quantities'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Anbar məlumatları
        </div>
    </div>

    <div class="panel-body">

        <table class="table table-condensed table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Atribut</th>
                <th>Miqdar</th>
                <th>Əməliyyat</th>
            </tr>
            </thead>

            <tbody>

            @forelse($quantities as $q)
            <tr>
                <td>{{ $q->id }}</td>
                <td>{{ $q->attribute ? $q->attribute->name_az : 'Ümumi' }}</td>
                <td>{{ $q->value}}</td>
                <td>--</td>
            </tr>
            @empty
            <tr>
                <td colspan=4>Məlumatlar hələ daxil edilməyib. Zəhmət olmasa, "Yoxla" düyməsini basın</td>
            </tr>
            @endforelse

            </tbody>
        </table>
        <div>
            {{ Form::open(array('url' => '/admin/product/'.$id.'/quantities/check', 'role' => 'form')) }}
            <div class="form-group">
                <button type="submit" name="save" value="save" class="btn btn-green btn-icon icon-left pull-right">Yoxla<i class="entypo-check"></i></button>
            </div>
            {{ Form::close() }}
        </div>


    </div>

</div>
@stop