@extends('admin.layouts.master')

@section('content')

<h3>Anbar məlumatları</h3>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
<table aria-describedby="table-2_info" class="table table-bordered table-striped datatable dataTable" id="table-2">
<thead>
<tr role="row">

    <th style="width: 354px;" colspan="1" rowspan="1"
        aria-controls="table-2" tabindex="0" role="columnheader" class="sorting">Məhsul
    </th>
    <th style="width: 194px;" colspan="1" rowspan="1"
        aria-controls="table-2" tabindex="0" role="columnheader" class="sorting">Atribut
    </th>
    <th style="width: 433px;" colspan="1"
        rowspan="1" aria-controls="table-2" tabindex="0" role="columnheader" class="sorting">Miqdar
    </th>
    <th style="width: 470px;" colspan="1" rowspan="1"
        aria-controls="table-2" tabindex="0" role="columnheader" class="sorting">Əməliyyatlar
    </th>
</tr>
</thead>


<tbody aria-relevant="all" aria-live="polite" role="alert">
@foreach($quantities as $q)
<tr class="odd">
    <td class=" ">{{ $q->product ? $q->product->name_az : 'Mövcud deyil' }}</td>
    <td class=" ">{{ $q->attribute ? $q->attribute->name_az : 'Əsas' }}</td>
    <td class=" ">{{ $q->value }}</td>
    <td class=" ">
        <a href="{{ url("admin/quantity/$q->id/") }}" class="btn btn-info btn-sm btn-icon icon-left">
            <i class="entypo-pencil"></i>
            Dəyərə düzəliş
        </a>
        <a href="{{ url("admin/quantity/$q->id/remove") }}" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="entypo-cancel"></i>
            Sil
        </a>
    </td>
</tr>
@endforeach
</tbody>
</table>
<div class="row">
    <div class="col-xs-6 col-left">
        <div id="table-2_info" class="dataTables_info">Showing {{ $quantities->getFrom(); }} to {{ $quantities->getTo(); }} of {{ $quantities->getTotal(); }} entries</div>
    </div>
    <div class="col-xs-6 col-right">
        <div class="dataTables_paginate paging_bootstrap">
            {{ $quantities->links() }}
        </div>
    </div>
</div>
</div>

@stop