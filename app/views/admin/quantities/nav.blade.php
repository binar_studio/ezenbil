<?php

$links = [
    'general' => ['Əsas məlumatlar', 'admin/quantity/'.$id],
    'remove' => ['Sil', 'admin/quantity/'.$id.'/remove']
];

?>
@include('admin.blocks.tabbed-nav', ['links' => $links, 'active' => $active])