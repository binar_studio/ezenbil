@extends('admin.blocks.tabbed-content')

@section('nav')
@include('admin.quantities.nav', ['id' => $quantity->id, 'active' => 'general'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Anbar miqdarının dəyişdirilməsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/quantity/'.$quantity->id, 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}

        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Məhsul:</label>

            <div class="col-sm-5">
                {{ $quantity->product->name_az }}
            </div>
        </div>

        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Atribut:</label>

            <div class="col-sm-5">
                {{ $quantity->attribute->name_az }}
            </div>
        </div>

        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Anbardakı miqdarı</label>

            <div class="col-sm-5">
                {{ Form::input('text', 'value', $quantity->value, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-5">
                <button type="submit" class="btn btn-green btn-icon icon-left">Yadda saxla<i class="entypo-check"></i></button>
            </div>
        </div>

        {{ Form::close() }}

    </div>

</div>
@stop