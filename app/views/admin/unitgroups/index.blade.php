@extends('admin.layouts.master')

@section('content')

<h3>Ölçü vahidi qrupları</h3>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
    <table class="table table-bordered table-striped datatable dataTable" id="table-2">
        <thead>
        <tr role="row">
            <th role="columnheader" class="sorting">
                #
            </th>
            <th role="columnheader" class="sorting">
                Qrup adı
            </th>
            <th role="columnheader" class="sorting">
                Əməliyyatlar
            </th>
        </tr>
        </thead>


        <tbody role="alert">

        @foreach($unitGroups as $unitGroup)

            <tr class="odd">
                <td class="sorting_1">
                    {{ $unitGroup->id }}
                </td>
                <td class="}}">{{ $unitGroup->name }}</span></td>
                <td class=" ">
                    <a href="{{ url('admin/unitgroup/'.$unitGroup->id) }}" class="btn btn-default btn-sm btn-icon icon-left">
                        <i class="entypo-pencil"></i>
                        Düzəliş et
                    </a>

                    <a href="{{ url('admin/unitgroup/'.$unitGroup->id.'/units') }}" class="btn btn-info btn-sm btn-icon icon-left">
                        <i class="entypo-info"></i>
                        Vahidlər
                    </a>

                    <a href="{{ url('admin/unitgroup/'.$unitGroup->id.'/remove') }}" class="btn btn-danger btn-sm btn-icon icon-left">
                        <i class="entypo-cancel"></i>
                        Sil
                    </a>
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>

    <div>
        <div class="form-group">
            <a class="btn btn-orange btn-icon icon-left" href="{{ url('admin/unitgroups/new') }}">Əlavə et<i class="entypo-plus"></i></a>
        </div>
    </div>


@stop