<?php

$links = [
    'general' => ['Əsas məlumatlar', 'admin/unitgroup/'.$id],
    'units' => ['Vahidlər', 'admin/unitgroup/'.$id.'/units'],
    'remove' => ['Sil', 'admin/unitgroup/'.$id.'/remove']
];

?>
@include('admin.blocks.tabbed-nav', ['links' => $links, 'active' => $active])