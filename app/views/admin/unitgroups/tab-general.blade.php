@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.unitgroups.nav', ['id' => $unitGroup->id, 'active' => 'general'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Əsas məlumatların dəyişdirilməsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/unitgroup/'.$unitGroup->id.'/general', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}

        <div class="form-group">
            <label for="field-1" class="col-sm-3 control-label">Ad</label>

            <div class="col-sm-5">
                {{ Form::input('text', 'name', $unitGroup->name, ['class' => 'form-control']) }}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-5">
                <button type="submit" class="btn btn-green btn-icon icon-left">Yadda saxla<i class="entypo-check"></i></button>
            </div>
        </div>

        {{ Form::close() }}

    </div>

</div>
@stop