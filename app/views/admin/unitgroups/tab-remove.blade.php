@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.unitgroups.nav', ['id' => $unitGroup->id, 'active' => 'remove'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Qrupun silinməsi
        </div>
    </div>

    <div class="panel-body">

        <p>Bu vahidlər qrupunu silmək istədiyinizə əminsinizmi?</p>

        <p class="bs-example">
            <a href="" class="btn btn-green btn-icon icon-left">
                İmtina et
                <i class="entypo-cancel"></i>
            </a>

            {{ Form::open(array('url' => '/admin/unitgroup/'.$unitGroup->id.'/remove', 'role' => 'form', 'method' => 'post')) }}

            <button type="submit" class="btn btn-red btn-icon icon-left">
                Sil
                <i class="entypo-check"></i>
            </button>
            {{ Form::close() }}

        </p>
    </div>

</div>
@stop