@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.unitgroups.nav', ['id' => $unitGroup->id, 'active' => 'units'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Qrupa aid vahidlər
        </div>
    </div>

    <div class="panel-body">

        <table class="table table-condensed table-bordered table-hover table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Ad</th>
                <th>Əməliyyatlar</th>
            </tr>
            </thead>

            <tbody>

            @forelse($unitGroup->units as $unit)
            <tr>
                <td>{{ $unit->id }}</td>
                <td>{{ $unit->name_az }}</td>
                <td>
                    {{ Form::open(array('url' => '/admin/unitgroup/'.$unitGroup->id.'/unit/'.$unit->id.'/remove', 'role' => 'form')) }}
                    <button class="btn btn-danger btn-sm btn-icon icon-left">
                        <i class="entypo-cancel"></i>Sil
                    </button>
                    {{ Form::close() }}
                </td>
            </tr>
            @empty
            No units added
            @endforelse

            </tbody>
        </table>
        <div>
            <div class="form-group">
                <a id="btn-add-unit" class="btn btn-orange btn-icon icon-left pull-left">Əlavə et<i class="entypo-plus"></i></a>
            </div>
        </div>

        <div class="clearfix"></div>

        <div id="form-add-unit" class="hide row">

            {{ Form::open(array('url' => '/admin/unitgroup/'.$unitGroup->id.'/units/new', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Ad (AZ)</label>

                <div class="col-sm-5">
                    {{ Form::input('text', 'name_az', null, ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                <label for="field-1" class="col-sm-3 control-label">Ad (RU)</label>

                <div class="col-sm-5">
                    {{ Form::input('text', 'name_ru', null, ['class' => 'form-control']) }}
                </div>
            </div>

            <div>
                <div class="form-group">
                    <button type="submit" class="btn btn-green btn-icon icon-left pull-right">Yadda saxla<i class="entypo-check"></i></button>
                </div>
            </div>
            {{ Form::close() }}

        </div>

        <script type="application/javascript">
            $(document).ready(function() {
                $("#btn-add-unit").click(function(){
                    $("#form-add-unit").removeClass('hide');
                });
            });
        </script>

    </div>

</div>
@stop