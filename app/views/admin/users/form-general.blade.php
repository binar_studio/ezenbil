<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Email</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'email', $user->email, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Ad</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'first_name', $user->first_name, ['class' => 'form-control']) }}
    </div>
</div>

<div class="form-group">
    <label for="field-1" class="col-sm-3 control-label">Soyad</label>

    <div class="col-sm-5">
        {{ Form::input('text', 'last_name', $user->last_name, ['class' => 'form-control']) }}
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label">Qrup</label>

    <div class="col-sm-5">
        {{ Form::select('group_id', $groupList, null, ['class' => 'select2', 'data-placeholder' => 'Bir qrup seçin...']) }}
    </div>
</div>

<div class="form-group">
    <div class="col-sm-offset-3 col-sm-5">
        <button type="submit" class="btn btn-green btn-icon icon-left">Yadda saxla<i class="entypo-check"></i></button>
    </div>
</div>