@extends('admin.layouts.master')

@section('content')

<h3>İstifadəçilərin siyahısı</h3>

<div id="table-2_wrapper" class="dataTables_wrapper form-inline" role="grid">
<table aria-describedby="table-2_info" class="table table-bordered table-striped datatable dataTable" id="table-2">
<thead>
<tr role="row">
    <th>
        <div class="checkbox checkbox-replace neon-cb-replacement">
            <label class="cb-wrapper"><input id="chk-1" type="checkbox">

                <div class="checked"></div>
            </label>
        </div>
    </th>
    <th>Ad</th>
    <th>Soyad</th>
    <th>Email</th>
    <th>Əməliyyatlar</th>
</tr>
</thead>


<tbody aria-relevant="all" aria-live="polite" role="alert">
@foreach($users as $user)
<tr class="odd">
    <td class="sorting_1">
        <div class="checkbox checkbox-replace neon-cb-replacement">
            <label class="cb-wrapper"><input id="chk-1" type="checkbox">

                <div class="checked"></div>
            </label>
        </div>
    </td>
    <td class=" ">{{ $user->first_name }}</td>
    <td class=" ">{{ $user->last_name }}</td>
    <td class=" ">{{ $user->email }}</td>
    <td class=" ">
        <a href="{{ url("admin/user/$user->id/") }}" class="btn btn-default btn-sm btn-icon icon-left">
            <i class="entypo-pencil"></i>
            Ətraflı
        </a>

        <a href="{{ url("admin/orders?user_id=$user->id") }}" class="btn btn-info btn-sm btn-icon icon-left">
            <i class="entypo-info"></i>
            Sifarişləri
        </a>

        <a href="{{ url("admin/user/$user->id/remove") }}" class="btn btn-danger btn-sm btn-icon icon-left">
            <i class="entypo-cancel"></i>
            Sil
        </a>

    </td>
</tr>
@endforeach
</tbody>
</table>
<div class="row">
    <div class="col-xs-6 col-left">
        <div id="table-2_info" class="dataTables_info">Showing {{ $users->getFrom(); }} to {{ $users->getTo(); }} of {{ $users->getTotal(); }} entries</div>
    </div>
    <div class="col-xs-6 col-right">
        <div class="dataTables_paginate paging_bootstrap">
            {{ $users->links() }}
        </div>
    </div>
</div>
</div>

<div>
    <div class="form-group">
        <a class="btn btn-orange btn-icon icon-left" href="{{ url('admin/users/new') }}">Əlavə et<i class="entypo-plus"></i></a>
    </div>
</div>

@stop