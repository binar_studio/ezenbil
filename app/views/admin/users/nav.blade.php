<?php

$links = [
    'general' => ['Əsas məlumatlar', 'admin/user/'.$id],
    'list' => ['Sifarişləri', 'admin/orders?user_id='.$id],
    'remove' => ['Sil', 'admin/user/'.$id.'/remove']
];

?>
@include('admin.blocks.tabbed-nav', ['links' => $links, 'active' => $active])