@extends('admin.layouts.master')

@section('content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Yeni istifadəçinin əlavəsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/users/new', 'method' => 'post', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
        @include('admin.users.form-general', ['user' => $user])
        {{ Form::close() }}

    </div>

</div>
@stop