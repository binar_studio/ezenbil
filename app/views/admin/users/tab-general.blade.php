@extends('admin.blocks.tabbed-content')

@section('nav')
    @include('admin.users.nav', ['id' => $user->id, 'active' => 'general'])
@stop

@section('inner-content')
<div class="panel panel-primary" data-collapsed="0">

    <div class="panel-heading">
        <div class="panel-title">
            Əsas məlumatların dəyişdirilməsi
        </div>
    </div>

    <div class="panel-body">

        {{ Form::open(array('url' => '/admin/user/'.$user->id.'/general', 'role' => 'form', 'class' => 'form-horizontal form-groups-bordered')) }}
        @include('admin.users.form-general', ['user' => $user])
        {{ Form::close() }}

    </div>

</div>
@stop