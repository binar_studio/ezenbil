@extends('front.layouts.master')


@section('css')
    {{ HTML::style('front/css/orange.css', array("rel" => "stylesheet")); }} <!-- Orange -->
@stop


@section('content')

    <section class="block" id="inner-head">
        <div class="fixed-img sec-bg4"></div>
        <div class="container">
            <h1>{{ trans('auth.signin') }}</h1>
        </div>
    </section>

    <section class="block" id="login-bg">
        <div class="container">
            <div class="login-sec">
                <div class="tab-content" id="myTabContent">
                    <div id="signin">
                        <div class="signin-form">
                            <h3>{{ trans('auth.sign_in_to_your_account') }}</h3>

                            {{ Form::open(array('url' => '/signin')) }}
                                {{ Form::input('text', 'email', null, ['placeholder' => trans('auth.email')]) }}
                                @if ($errors->has('email')) <p class="alert alert-danger alert-form">{{ $errors->first('email') }}</p> @endif

                                {{ Form::input('password', 'password', null, ['placeholder' => trans('auth.password')]) }}
                                @if ($errors->has('password')) <p class="alert alert-danger alert-form">{{ $errors->first('password') }}</p> @endif


                            <a href="#" title="">{{ trans('auth.forgot_password') }}</a><br/>
                                <a href="/signup" title="">{{ trans('auth.no_account_register') }}</a>
                                <input type="submit" class="submit" value="{{ trans('auth.login') }}" />
                            {{ Form::close() }}
                        </div><!-- Sign in Form -->
                    </div>
                    
                </div><!-- Tab-End -->
            </div>
        </div>
    </section>
@stop

@section('endjs')

@stop