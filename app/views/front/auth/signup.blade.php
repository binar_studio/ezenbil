@extends('front.layouts.master')


@section('css')
    {{ HTML::style('front/css/orange.css', array("rel" => "stylesheet")); }} <!-- Orange -->
@stop


@section('content')

    <section class="block" id="inner-head">
        <div class="fixed-img sec-bg6"></div>
        <div class="container">
            <h1 style="color: #F0B70C;">{{ trans('static.signup') }}</h1>
        </div>
    </section>


    <section class="block" id="login-bg">
        <div class="container">
            <div class="login-sec">
                <div class="tab-content" id="myTabContent">
                    <div id="signup" class="tab-pane active">
                        <div class="register-form">
                            <div class="row">
                                {{ Form::open(array('url' => '/signup')) }}
                                <h3>{{ trans('static.please_enter_your_details') }}</h3>

                                <div class="col-md-12">
                                    <div class="form-element">
                                        <i class="fa fa-envelope-o"></i>
                                        {{ Form::input('text', 'email', null, ['placeholder' => trans('static.your_email_address')]) }}
                                    </div>
                                    @if ($errors->has('email')) <p class="alert alert-danger alert-form">{{ $errors->first('email') }}</p> @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="form-element">
                                        <i class="fa fa-lock"></i>
                                        {{ Form::input('password', 'password', null, ['placeholder' => trans('static.choose_password')]) }}
                                    </div>
                                    @if ($errors->has('password')) <p class="alert alert-danger alert-form">{{ $errors->first('password') }}</p> @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="form-element">
                                        <i class="fa fa-phone"></i>
                                        {{ Form::input('text', 'phone', null, ['placeholder' => trans('static.your_phone_number')]) }}
                                    </div>
                                    @if ($errors->has('phone')) <p class="alert alert-danger alert-form">{{ $errors->first('phone') }}</p> @endif
                                </div>
                                <div class="col-md-6">
                                    <div class="form-element">
                                        <i class="fa fa-male"></i>
                                        {{ Form::input('text', 'first_name', null, ['placeholder' => trans('static.your_first_name')]) }}
                                    </div>
                                    @if ($errors->has('first_name')) <p class="alert alert-danger alert-form">{{ $errors->first('first_name') }}</p> @endif
                                </div>
                                <div class="col-md-6">
                                    <div class="form-element">
                                        <i class="fa fa-male"></i>
                                        {{ Form::input('text', 'last_name', null, ['placeholder' => trans('static.your_last_name')]) }}
                                    </div>
                                    @if ($errors->has('last_name')) <p class="alert alert-danger alert-form">{{ $errors->first('last_name') }}</p> @endif
                                </div>
                                <div class="col-md-12">
                                    <div class="submit-form">
                                        <input type="submit" value="{{ trans('static.complete_sign_up') }}"/>

                                        <p>By registering you comfirm that you agree with our <i>Term & Condition</i>
                                            and <i>Privacy Policy</i></p>
                                    </div>
                                </div>
                                {{ Form::close() }}
                            </div>
                        </div>
                        <!-- Signup Form -->
                    </div>
                </div>
                <!-- Tab-End -->
            </div>
        </div>
    </section>
@stop

@section('endjs')

@stop