@forelse($details['products'] as $productItem)

<li data-line-item-id="{{ $productItem['item']->id }}" data-price="{{ $productItem['price'] }}">
    <span><img src="{{  Image::path($productItem['item']["mainPhoto"]["filename"], 'resizeCrop', 60, 60)  }}" alt=""/></span>
    <a href="#" title="">
        <span>{{ $productItem['item']->product->name_az }}</span>
        @if($productItem['item']->attribute)

            @if(is_null($productItem['item']->attribute->color))
                <span>({{ $productItem['item']->attribute->name }})</span>
            @else
                <span>({{ trans('product.color') }}: {{ $productItem['item']->attribute->name }})</span>
            @endif

        @endif
        </a>
    <i>{{ $productItem['price'] }} AZN </i> - {{ $productItem['quantity'] }}
    <div class="cart-bar-hover">
        <ul>
            <li><a href="#" title=""><i class="fa fa-cog"></i></a></li>
            <li><a href="#" title=""><i class="fa fa-trash-o"></i></a></li>
        </ul>
    </div>
</li>
@empty
<li>
    <h6>{{ trans('cart.your_cart_is_empty') }}</h6>
</li>
@endforelse

<li>
    <h6>{{ trans('cart.total') }} : {{ $details['total'] }} AZN</h6>
    <a href="/cart" title="" class="checkout-btn">{{ trans('cart.checkout') }}</a>
</li>