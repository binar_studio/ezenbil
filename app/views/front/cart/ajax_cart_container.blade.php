<div class="row">
    <div class="col-md-12">
        <div class="add-cart-sec">

            <div class="cart-head">
                <h2 class="cart-product">{{ trans('cart.upper_product') }}</h2>
                <h2 class="cart-price">{{ trans('cart.upper_price') }}</h2>
                <h2 class="cart-quanity">{{ trans('cart.upper_quantity') }}</h2>
                <h2 class="cart-total">{{ trans('cart.upper_total') }}</h2>
            </div>
            <ul>

                @forelse($details['products'] as $productItem)


                <li class="animated flipInX">
                    <div class="cart-product">
                        <div class="cart-thumb">
                            <img src="{{ $productItem['item']->product->mainPhoto->filename }}" alt="">
                            <a title=""><i class="fa fa-trash-o"></i></a>
                        </div>
                        <div class="cart-detals">
                            <h5>{{ $productItem['item']->product->name }}</h5>


                            @if($productItem['item']->attribute)
                            @if($productItem['item']->attribute->color)
                                            <span>
                                                <strong>{{ $productItem['item']->attribute->group->name }}: </strong> <i style="background-color: {{ $productItem['item']->attribute->color }}; border: 1px solid;"></i>
                                            </span>
                            @else
                            <p>{{ $productItem['item']->attribute->group->name }}: {{ $productItem['item']->attribute->name }}</p>
                            @endif
                            @endif
                        </div>
                    </div>
                    <div class="cart-price">
                        <span>{{ $productItem['price'] }} AZN</span>
                    </div>
                    <div class="cart-quanity">
                        <div class="col-md-3 cart-quanity-plus">
                        </div>
                        <div class="col-md-6">
                            <div class="decrements">
                                <form class="decrements-form" data-product-line-item-id="{{ $productItem['item']->id }}">
                                    <input field="quantity" class="qtyplus" value="+" type="button" onclick="changeCartItemQuantity( '{{ $productItem['item']->id }}', '{{ $productItem['quantityOriginal']+1 }}' )">
                                    <input class="qty" value="{{ $productItem['quantityOriginal'] }}" name="quantity" type="text">
                                    <input field="quantity" class="qtyminus" value="-" type="button" onclick="changeCartItemQuantity( '{{ $productItem['item']->id }}', '{{ $productItem['quantityOriginal']-1 }}' )">
                                </form>
                            </div>
                        </div>
                        <div class="col-md-3 cart-quanity-plus">
                            @if($productItem['quantitySale'])
                            <span>+{{ $productItem['quantityPlus'] }}</span>
                            @endif
                        </div>
                    </div>
                    <div class="cart-total">
                                <span>

                                    <i>

                                        @if($productItem['subtotalSale'])
                                       <span style="text-decoration: line-through; color: #909090; font-weight: 100;">
                                           {{ $productItem['subtotalOriginal'] }}
                                       </span> {{ $productItem['subtotal'] }}
                                        @else
                                        {{ $productItem['subtotal'] }}
                                        @endif
                                        AZN
                                    </i>

                                </span>
                    </div>
                </li>

                @empty

                No products added yet

                @endforelse


            </ul>
        </div>
    </div>

    <div class="col-md-4">
        <div class="cart-total-box animated bounceIn">
            <div class="cart-head">
                <h2 class="cart-product">{{ trans('cart.upper_cart_total') }}</h2>
            </div>
            <ul>
                <li><span>{{ trans('cart.subtotal') }}</span><i>{{ $details['total'] }} AZN</i></li>
                <li><span>{{ trans('cart.shipping') }}</span><i>{{ trans('cart.free_shipping') }}</i></li>
                <li><span>{{ trans('cart.order_total') }}</span><i>{{ $details['total'] }} AZN</i></li>
            </ul>
        </div>
    </div>

    <!--<div class="col-md-5">
        <div class="cart-total-box animated bounceIn">
            <div class="cart-head">
                <h2 class="cart-product">{{ trans('cart.upper_calculate_shipping') }}</h2>
            </div>
            <ul>
                <li class="customlist">
                    <select>
                        <option>United State</option>
                        <option>02</option>
                        <option>03</option>
                        <option>04</option>
                        <option>05</option>
                    </select>
                </li>
                <li class="customlist">
                    <select>
                        <option>Select State</option>
                        <option>02</option>
                        <option>03</option>
                        <option>04</option>
                        <option>05</option>
                    </select>
                </li>
                <li><input placeholder="Postal Code / Zip Code" type="text"></li>
                <li><a href="#" title="">{{ trans('cart.update_total') }}</a></li>
            </ul>

        </div>
    </div>-->


</div>

<script>
    $(document).ready(function() {
        $(".decrements-form").submit(function(event){
            event.preventDefault();
            changeCartItemQuantity( $(this).parent().attr('data-product-line-item-id'), $(this).children('.qty').first().val() );
        });
    })
</script>