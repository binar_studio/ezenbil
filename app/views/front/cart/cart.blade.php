@extends('front.layouts.master')


@section('css')
{{ HTML::style('front/css/cyan.css', array("rel" => "stylesheet")); }} <!-- Orange -->
@stop


@section('content')

<section class="block" id="inner-head">
    <div class="fixed-img" style="background-image: url({{ asset('front/images/sec-bg4.jpg') }})"></div>
    <div class="container">
        <h1>{{ trans('cart.upper_your_shopping_cart') }}</h1>
    </div>
</section>

<section class="block">
    <div id="cart-container" class="container">
        Loading
    </div>
</section>

@stop

@section('endjs')

<script type="application/javascript">

    function changeQuantity(product_line_item_id, quantity) {
        console.log('changing quanity');
    }

    $(document).ready(function() {

        refreshCartContainer();

    });
</script>

@stop
