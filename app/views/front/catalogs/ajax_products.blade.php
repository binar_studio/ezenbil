@foreach($products as $product)
{{--    <li class="col-md-12">
        <div class="post grid-product">
            <div class="post-thumb">
                <div class="product-image" style="background-image: url('{{ asset($product["photos"][0]["filename"]) }}')"></div>
                <a href="/product/{{ $product['id'] }}" title="">{{ trans("general.viewMore") }}</a>
            </div>
            <h2><a href="#" title="">{{ $product["name_az"] }}</a></h2>
            <span class="view-post"><i class="fa fa-eye"></i>{{ $product["price"] }} AZN</span>
            <ul>
                <li><a href="#" title="">20 purchases</a></li>
                <li><a onclick="addToCompare({{ $product->id }})" title="">{{ trans('general.add_to_compare') }}</a>
                </li>
                <li class="share-post"><a href="#" title=""><i class="fa fa-random"></i></a>
                    <ul>
                        <li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" title=""><i class="fa fa-google-plus"></i></a>
                        </li>
                        <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" title=""><i class="fa fa-pinterest"></i></a>
                        </li>
                        <li><a href="#" title=""><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </li>--}}

    <div class="col-md-3 margin-bottom">
        <div class="shop-products">
            <div class="shop-product-thumb">
                <img src="{{ Image::path($product["mainPhoto"]["filename"], 'resizeCrop', 269, 315) }}" alt=""/>
                @if(strtotime($product->created_at) > strtotime("-1 month"))
                    <span>{{ trans('general.upper_new') }}</span>
                @endif
                <a href="/product/{{ $product['id'] }}" title=""><i class="fa fa-shopping-cart"></i></a>
            </div>
            <h3>{{ $product["name_az"] }}</h3>
            <ul>
                <li><a onclick="addToCompare({{ $product->id }})">{{ trans('header.compare') }}</a> </li>
            </ul>
            <p>{{ $product["price"] }} AZN</p>
        </div>
    </div>
@endforeach