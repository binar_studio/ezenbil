@extends('front.layouts.master')


@section('css')
    {{ HTML::style('front/css/cyan.css', array("rel" => "stylesheet")); }} <!-- Orange -->
@stop


@section('content')
    {{--<section class="block" id="inner-head">--}}
        {{--<div class="fixed-img sec-bg4" style="background-image: url('{{ asset("/front/images/sec-bg1.jpg") }}')"></div>--}}
        {{--<div class="container">--}}

            {{--<h1>--}}
                {{--@foreach($category->getAncestors() as $cat)--}}
                    {{--{{ $cat->name_az }} /--}}
                {{--@endforeach--}}
                {{--{{ $category->name_az }}--}}
            {{--</h1>--}}
        {{--</div>--}}
    {{--</section>--}}

    <section class="block remove-gap">
        <div class="container">
            <div class="row">
                <div class="col-md-12 margin-bottom category-crumb" >
                    <ul>
                        @foreach($category->getAncestors() as $cat)
                        <li style="border-color: {{ $cat->color }}">{{ $cat->name_az }} ></li>
                        @endforeach
                            <li style="border-color: {{ $category->color }}">{{ $category->name_az }} </li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <aside>
                        @include('front.elements.categoriesWidget', array('title' =>  $category["name_az"] , 'cache_name' => "category_children_widget-$category->id", 'roots' => $category->children))

                        <div class="widget-body">
                            <div class="w-get-help">
                                <ul>
                                    <li id="compare"><a href="{{$category->id}}/compare">{{ trans("header.compare") }}
                                            <span id="compare-count">{{ count(Session::get('compare.items'.$category->id)) }}</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        @include('front.elements.filters', array('category_id' => $category['id']))

                        @include('front.elements.tabbedProductsWidget', array('title1' => trans("general.recent_opened_products"), 'title2' => trans("general.popularProducts"), 'collection1' => $recent_products, 'collection2' => $popular_products))

                        <div class="widget-body">
                            <div class="heading-5">
                                <h2><i class="fa fa-folder-open"></i>DISCOUNT PRODUCT</h2>
                            </div>

                            <div class="w-discount">
                                <img alt="" src="http://placehold.it/368x286">

                                <div class="w-discount-des">
                                    <h3>WEBINANE.COM</h3>
                                    <i>20% OFF</i>

                                    <p>ODDER OVER $80</p>
                                    <a title="" href="#">SHOP NOW</a>
                                </div>
                            </div>
                        </div>
                    </aside>
                </div>

                <div class="col-md-9">
                    <div id="product-sales">

                    </div>

                    <div class="blog-post-sec">
                        <ul id="products" class="people-list row grid-view block">


                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>
@stop

@section('endjs')

    {{ HTML::script("front/js/jquery.navgoco.js"); }}

    <script type="text/javascript">
        var base_url = "{{ Request::url() }}";
        var current_page = 0;
        var filters = '';

        function loadNextPage() {

            current_page++;

            $.ajax({
                url: base_url + '/ajaxProducts?page=' + current_page + '&' + filters,
                context: document.body
            }).done(function (html) {

                // TODO status code. stop if last page
                $("#product-sales").append(html);
            });

        }

        $(document).ready(function () {

            loadNextPage();

            // This button will increment the value
            $('.qtyplus').click(function (e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('input[name=' + fieldName + ']').val());
                // If is not undefined
                if (!isNaN(currentVal)) {
                    // Increment
                    $('input[name=' + fieldName + ']').val(currentVal + 1);
                } else {
                    // Otherwise put a 0 there
                    $('input[name=' + fieldName + ']').val(0);
                }
            });

            // This button will decrement the value till 0
            $(".qtyminus").click(function (e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('input[name=' + fieldName + ']').val());
                // If it isn't undefined or its greater than 0
                if (!isNaN(currentVal) && currentVal > 0) {
                    // Decrement one
                    $('input[name=' + fieldName + ']').val(currentVal - 1);
                } else {
                    // Otherwise put a 0 there
                    $('input[name=' + fieldName + ']').val(0);
                }
            });

            $("#tab-recent").owlCarousel({
                autoPlay: true,
                stopOnHover: true,
                navigation: true,
                goToFirstSpeed: 2000,
                slideSpeed: 1500,
                singleItem: true,
                autoHeight: true,
                pagination: false,
                transitionStyle: "goDown"
            });

            $("#tab-popular").owlCarousel({
                autoPlay: true,
                stopOnHover: true,
                navigation: true,
                goToFirstSpeed: 2000,
                slideSpeed: 1500,
                singleItem: true,
                autoHeight: true,
                pagination: false,
                transitionStyle: "goDown"
            });

            $("#product-post").owlCarousel({
                autoPlay: 20000, //Set AutoPlay to 20 seconds
                navigation: true,
                items: 1
            });

            // navgoco sidebar category tree script
            $('#catlist').navgoco({
                caretHtml: '',
                accordion: true,
                openClass: 'open',
                slide: {
                    duration: 400,
                    easing: 'swing'
                },
                toggle: true
            });

        });
    </script>
@stop