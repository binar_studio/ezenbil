@extends('front.layouts.master')


@section('css')
    {{ HTML::style('front/css/cyan.css', array("rel" => "stylesheet")); }} <!-- Orange -->
@stop


@section('content')

    <section class="block" id="inner-head">
        <div class="fixed-img sec-bg5"></div>
        <div class="container">
            <h1>{{ $category->name }}</h1>
        </div>
    </section>

    <section class="block">
        <div class="container">
            @if($category->isLeaf())
                @if($compare_products != null)
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <td>
                            </td>
                            @foreach($compare_products as $product)
                                <td>
                                    <button class="btn btn-blue" onclick="removeFromCompare('{{ $product->id }}')"
                                            value="{{ $product->id }}">Siyahidan cixar <i class="fa fa-remove"></i>
                                    </button>
                                </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>
                                image
                            </td>
                            @foreach($compare_products as $product)
                                <td>
                                    <img src="{{ Image::path($product["mainPhoto"]["filename"], 'resizeCrop', 269, 315) }}" alt=""/>
                                </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>
                                name
                            </td>
                            @foreach($compare_products as $product)
                                <td>
                                    {{ $product->name_az }}
                                </td>
                            @endforeach
                        </tr>
                        <tr>
                            <td>
                                price
                            </td>
                            @foreach($compare_products as $product)
                                <td>
                                    {{ $product->price }}
                                </td>
                            @endforeach
                        </tr>

                        @forelse($category->features as $category_feature)
                            <tr>
                                <td>{{ $category_feature->name_az }}</td>
                                @forelse($compare_products as $product)
                                    <?php $found = false; ?>
                                    @foreach($product->features as $product_feature)
                                        @if($product_feature->id == $category_feature->id)
                                            <td>
                                                @if($product_feature["is_boolean"])
                                                    @if($product_feature["pivot"]["value"] == 1)
                                                        <i class="fa fa-check"
                                                           style="font-size: 18px; color:#007534;position: absolute"></i>
                                                    @elseif($product_feature["pivot"]["value"] == 0)
                                                        <i class="fa fa-times"
                                                           style="font-size: 18px; color:#000;position: absolute"></i>
                                                    @endif
                                                @else
                                                    {{ $product_feature->pivot->value }}
                                                @endif
                                            </td>
                                            <?php $found = true; ?>
                                        @endif
                                    @endforeach
                                    @if( !$found)
                                        <td>―</td>
                                    @endif
                                @empty
                                    <td></td>
                                @endforelse
                            </tr>
                        @empty
                            <tr>
                                <td>
                                    no feature on this category
                                <td>
                            <tr>
                        @endforelse
                    </table>
                @else
                    Muqaise ucun mehsul elave etmemisiniz.
                @endif
            @else
                <div class="col-lg-3"></div>
            <div class="col-lg-6" style="text-align: center">
                <h4>Muqaise etmek ucun asagidaki kategoriyalardan birini secin</h4>
                <div class="product-details">
                    <ul class="col-md-6">
                        @foreach($category->getLeaves() as $leaf_category)
                            <li>
                                <span>{{ count(Session::get('compare.items'.$leaf_category->id)) }} məhsul</span>
                                <p>
                                    <a href="/electronics/{{ $leaf_category->id }}/compare">{{ $leaf_category->name_az }}</a>
                                </p>
                            </li>
                        @endforeach
                    </ul>
                </div>
                </div>
            @endif
        </div>

    </section>
@stop


@section('endjs')

    {{ HTML::script("front/js/tiny-carousal.js"); }}<!-- Tiny Carousal -->

    <script type="text/javascript">
        $(document).ready(function () {

            $('#compare1').tinycarousel({axis: 'y'});

            $('#compare2').tinycarousel({axis: 'y'});
        });
    </script>
@stop