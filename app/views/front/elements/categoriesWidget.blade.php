<?php if(count($roots)){ ?>
<div class="categories-widget widget-body">
    <div class="heading-5">
        <h2>{{ $title }}</h2>
    </div>

    <?php echo Flatten::section($cache_name . '-' . LaravelLocalization::getCurrentLocale(), function () use ($roots) { ?>
    <ul id="catlist">
        @foreach($roots as $root)
            <li style="border-color: {{ $root->color }}">
                @if(count($root->children) >= 1)
                    <a href="#" title=""><i class="fa {{ $root->fa_icon }}" style="background: transparent !important"></i>  {{ $root["name_".LaravelLocalization::getCurrentLocale()] }}</a>
                    <ul>
                        <?php $root->children->each(function($child){ ?>
                        <li  style="border-color: {{ $child->color }}"><a href="/electronics/{{ $child->id }}">{{ $child->name_az }}</a></li>
                        <?php }); ?>
                    </ul>
                @else
                    <a href="/electronics/{{ $root->id }}" title=""><i class="fa {{ $root->fa_icon }}" style="background: transparent !important"></i> {{ $root["name_".LaravelLocalization::getCurrentLocale()] }}</a>
                @endif
            </li>
        @endforeach
    </ul>
    <?php }); ?>
</div>
<?php }else{ ?>

<?php } ?>
