@if($category->isLeaf())
    <div class="widget-body animated fadeInRightBig">
        <form id="filters" class="w-filters">
            <ul>
                <li>
                    {{ Form::input('text', 'form') }}
                </li>

                @if($priceMin != $priceMax)
                    <li>
                        <h3>{{ trans('general.price_range') }}</h3>

                        {{ Form::input('text', 'price_range', $priceMin.';'.$priceMax) }}
                    </li>
                @endif

                @foreach($optionables as $f)
                    <li>
                        <h3>{{ $f->name }}</h3>
                        <ul>
                            @foreach($f->options as $fo)
                                <li>{{ Form::radio('feature[option]['.$f->id.']', $fo->id) }} {{ $fo->name }}</li>
                            @endforeach
                        </ul>
                    </li>
                @endforeach


                @if(! empty($booleanables))
                    <li>
                        <h3>{{ trans('general.features') }}</h3>
                        <ul>
                            @foreach($booleanables as $f)
                                <li>{{ Form::checkbox('feature[bool]['.$f->id.']') }} {{ $f->name }}</li>
                            @endforeach
                        </ul>
                    </li>
                @endif

                <button id="btn-filter" class="btn btn-primary">{{ trans('general.search') }}</button>
            </ul>
        </form>
    </div>
    {{ HTML::script("front/js/ion.rangeSlider.min.js"); }}
    {{ HTML::style('front/css/ion.rangeSlider.css') }}
    {{ HTML::style('front/css/ion.rangeSlider.skinModern.css') }}
    <script>
        $(document).ready(function () {

            $("input[name=price_range]").ionRangeSlider({
                type: "double",
                grid: true,
                min: '{{ $priceMin }}',
                max: '{{ $priceMax }}',
                prefix: "AZN "
            });

            $("#btn-filter").click(function (event) {
                event.preventDefault();
                var data = $("#filters").serialize();
                console.log(data);

                current_page = 0;
                $("#product-sales").html('');

                filters = data;
                loadNextPage();

            });

        });
    </script>
@endif