
<footer class="block">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="footer-about">
					<h3><i>ABOUT</i> US</h3>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s ext ever since the 1500s  1500s ext ever since the 1500s  industry's standard dummy text ever.</p>
					<ul>
						<li><span><i class="fa fa-mobile"></i></span><p>Phone:   + 98392039330</p><p>Fax:        + 98392039330</p></li>
						<li><span><i class="fa fa-envelope-o"></i></span><p>Email:       meinto@tolee.com</p><p>Address:   Excel Tower OPG Rpad</p></li>
					</ul>
				</div>
			</div>

			<div class="col-md-3">
				<div class="footer-product">
					<h3><i>RECENT</i> PRODUCT</h3>
					<span><img src="http://placehold.it/145x256" alt="" /></span>
					<h4><a href="#" title="">Best Offer Seller This Dress</a></h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry inprinting.</p>
					<ul>
						<li><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
						<li><a href="#" title=""><i class="fa fa-heart"></i></a></li>
					</ul>
				</div>
			</div>

			<div class="col-md-3">
				<div class="footer-post">
				<h3><i>BLOG</i> POST</h3>
					<ul>
						<li><a href="#" title="">Consectetur Adipisicing</a><i>Novermber 20,2013</i></li>
						<li><a href="#" title="">Consectetur Adipisicing</a><i>Novermber 20,2013</i></li>
						<li><a href="#" title="">Consectetur Adipisicing</a><i>Novermber 20,2013</i></li>
						<li><a href="#" title="">Consectetur Adipisicing</a><i>Novermber 20,2013</i></li>
					</ul>
				</div>
			</div>

			<div class="col-md-3">
				<h3><i>HOT</i> DEALS</h3>
				<div class="footer-deal">
					<div class="footer-deals">
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
					</div>

					<div class="footer-deals">
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
					</div>

					<div class="footer-deals">
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
						<a href="#" title=""><img src="http://placehold.it/85x123" alt="" /><span>30% <i>Off</i></span></a>
					</div>
				</div>
			</div>

		</div>
	</div>
</footer>

<div class="bottom-line">

	<span>Copyright 2015 <i>E-zenbil</i>.</span>
</div>

<script type="text/javascript">
	$(document).ready(function () {

		$(".footer-deal").owlCarousel({
			autoPlay: true,
			stopOnHover: true,
			goToFirstSpeed: 2000,
			slideSpeed: 1500,
			singleItem: true,
			autoHeight: true,
			transitionStyle: "goDown",
			paginationNumbers: true
		});
	});
</script>