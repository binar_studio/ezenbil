<header class="header1">
    <div class="container">
        <div class="top-bar">
            <ul class="logo bar-dropdown" style="margin: 0;padding: 0;">
                <li><a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), '/') }}"
                       title="Logo">{{ HTML::image('front/images/logo.png');  }}</a></li>
            </ul>
            @if(Sentry::check())
                <ul class="language bar-dropdown">
                    <li>
                        <a href="/signout"
                           title=""><i class="fa fa-users"></i>{{ Sentry::getUser()->first_name }} {{ Sentry::getUser()->last_name }}</a>
                        <ul>
                            <li><a href="#"><i class="fa fa-user"></i> Profile</a></li>
                            <li><a href="/signout"><i class="fa fa-sign-out"></i> Sign Out</a></li>
                        </ul>
                    </li>
                </ul>
            @else
                <ul class="compare-btn bar-dropdown">
                    <li>
                        <a href="/signin"
                           title=""><i class="fa fa-sign-in"></i>Sign in</a>
                    </li>
                </ul>
            @endif


            <ul class="language bar-dropdown">
                <li>
                    <a href="#" title=""><i class="fa fa-font"></i>
                        {{ LaravelLocalization::getCurrentLocaleName() }}
                    </a>
                    <ul>
                        @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                            <li>
                                <a rel="alternate" hreflang="{{$localeCode}}"
                                   href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                                    {{{ $properties['native'] }}}
                                </a>
                            </li>
                        @endforeach


                    </ul>
                </li>
            </ul>

            <ul class="whishlist-bar bar-dropdown">
                <li>
                    <a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), 'wishlist') }}"
                       title=""><i class="fa fa-heart"></i>{{ trans('header.wishlist') }}</a></li>
            </ul>

				<span><i class="fa fa-mobile"></i>
				24/7 {{ trans("header.support") }}: 055 448 78 25
                </span>
        </div>
    </div>
    @include('front.elements.menu')
</header>

<header class="responsive-header">
    <div class="top-bar">
        <ul class="shop-cart bar-dropdown">
            <li><a href="#" title=""><i class="fa fa-shopping-cart"></i></a>
                <ul>
                    <li><span><img src="http://placehold.it/50x50" alt=""/></span>
                        <a href="#" title="">Short T-Shirt 2013</a>
                        <i>$360.00</i>

                        <div class="cart-bar-hover">
                            <ul>
                                <li><a href="#" title=""><i class="fa fa-cog"></i></a></li>
                                <li><a href="#" title=""><i class="fa fa-trash-o"></i></a></li>
                            </ul>
                        </div>
                    </li>
                    <li><span><img src="http://placehold.it/50x50" alt=""/></span>
                        <a href="#" title="">New Shoes T-Pain</a>
                        <i>$560.00</i>

                        <div class="cart-bar-hover">
                            <ul>
                                <li><a href="#" title=""><i class="fa fa-cog"></i></a></li>
                                <li><a href="#" title=""><i class="fa fa-trash-o"></i></a></li>
                            </ul>
                        </div>
                    </li>

                    <li><span><img src="http://placehold.it/50x50" alt=""/></span>
                        <a href="#" title="">Cottom Jeans Paint</a>
                        <i>$56.00</i>

                        <div class="cart-bar-hover">
                            <ul>
                                <li><a href="#" title=""><i class="fa fa-cog"></i></a></li>
                                <li><a href="#" title=""><i class="fa fa-trash-o"></i></a></li>
                            </ul>
                        </div>

                    </li>
                    <li>
                        <h6>Total : $960</h6>
                        <a href="#" title="" class="checkout-btn">Checkout</a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="language bar-dropdown">
            <li><a href="#" title=""><i class="fa fa-font"></i>English</a>
                <ul>
                    <li><a href="#" title="">ENGLISH</a></li>
                    <li><a href="#" title="">FRENCH</a></li>
                    <li><a href="#" title="">ARABIC</a></li>
                </ul>
            </li>
        </ul>

        <ul class="compare-btn bar-dropdown">
            <li><a href="#" title=""><i class="fa fa-random"></i>Compare</a></li>
        </ul>

        <ul class="whishlist-bar bar-dropdown">
            <li><a href="#" title=""><i class="fa fa-heart"></i>Whishlist</a></li>
        </ul>
    </div>

    <Div class="logo">
        <a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), '/') }}" title="">
            {{ HTML::image('front/images/logo.png');  }}
        </a>
    </div>

    <div class="search-header">
        <form><input type="text" placeholder="Search Here"/><a href="#" title=""><i class="fa fa-search"></i></a></form>
    </div>

    <div class="responsive-menu">
        <a class="menu-dropdown-btn" title="">MENU <i class="fa fa-align-justify"></i></a>
        <ul>
            <li><a href="#" title="">Home</a>
                <ul>
                    <li><a href="index.html" title="">Home Page 1</a></li>
                    <li><a href="index2.html" title="">Home Page 2</a></li>
                    <li><a href="index3.html" title="">Home Page 3</a></li>
                    <li><a href="index4.html" title="">Tech Home Page 1</a></li>
                    <li><a href="index5.html" title="">Tech Home Page 2</a></li>
                    <li><a href="index11.html" title="">Tech Home Page 3</a></li>
                    <li><a href="index6.html" title="">Home Page 6</a></li>
                    <li><a href="index7.html" title="">Music Home Page</a></li>
                    <li><a href="index8.html" title="">Home with Sidebar</a></li>
                    <li><a href="index9.html" title="">Book Home Page</a></li>
                    <li><a href="index12.html" title="">Book Home Page 2</a></li>
                    <li><a href="index10.html" title="">Home Page 12</a></li>
                </ul>
            </li>

            <li><a href="#" title="">Features</a>
                <ul>
                    <li><a href="mega-menu.html" title="">4 Styles Mega Menu</a></li>
                    <li><a href="header1.html" title="">Header Style 1</a></li>
                    <li><a href="header2.html" title="">Header Style 2</a></li>
                    <li><a href="boxed.html" title="">Boxed Layout</a></li>
                    <li><a href="wide.html" title="">Wide Layout</a></li>
                    <li><a href="compare.html" title="">Compare Product </a></li>
                    <li><a href="compare2.html" title="">Compare Product 2</a></li>
                    <li><a href="widgets.html" title="">Widgets</a></li>
                    <li><a href="product1.html" title="">Masonary Product</a></li>
                    <li><a href="product4.html" title="">Masonary Product 2</a></li>
                    <li><a href="product3.html" title="">Searchable Products</a></li>
                    <li><a href="product2.html" title="">Products Page</a></li>
                    <li><a href="price-table.html" title="">2 Price Tables</a></li>
                    <li><a href="cart.html" title="">Cart</a></li>
                </ul>
            </li>
            <li><a href="#" title="">Pages</a>
                <ul>
                    <li><a href="blog.html" title="">Masonary Blog</a></li>
                    <li><a href="blog2.html" title="">Blog One Column</a></li>
                    <li><a href="product1.html" title="">Masonary Product</a></li>
                    <li><a href="product4.html" title="">Masonary Product 2</a></li>
                    <li><a href="product3.html" title="">Searchable Products</a></li>
                    <li><a href="product2.html" title="">Products Page</a></li>
                    <li><a href="compare.html" title="">Compare Product </a></li>
                    <li><a href="compare2.html" title="">Compare Product 2</a></li>
                    <li><a href="review.html" title="">Review Page</a></li>
                    <li><a href="404.html" title="">404 Error page</a></li>
                    <li><a href="faq.html" title="">FAQ Page</a></li>
                    <li><a href="single-post.html" title="">Single Product</a></li>
                    <li><a title="" href="signin.html">Login/Register</a></li>
                </ul>
            </li>
            <li><a href="#" title="">Shortcode</a>
                <ul>
                    <li><a href="price-table.html" title="">Price Box</a></li>
                    <li><a href="office.html" title="">Our Offices</a></li>
                    <li><a href="skills.html" title="">Our Skills</a></li>
                    <li><a href="services.html" title="">Our Services</a></li>
                    <li><a href="team.html" title="">Our Team</a></li>
                    <li><a href="faq.html" title="">FAQ</a></li>
                </ul>
            </li>

            <li><a href="about.html" title="">About </a></li>
            <li><a href="contact.html" title="">Contact</a></li>
        </ul>
    </div>
</header>
