<div class="menu-space"></div>
<nav id="wide-menu">
    <div class="container">
        <?php echo Flatten::section('menu-' . LaravelLocalization::getCurrentLocale(), function () use ($root_categories) { ?>
        <div class="menu style2">
            <ul>
                <li>
                    <a href="{{ LaravelLocalization::getLocalizedURL(LaravelLocalization::getCurrentLocale(), 'electronics') }}"
                       title="">Texnologiya<span>electronics</span><i class="fa fa-laptop cyan menuhover"></i></a>
                    <ul class="mega-menu row reverse menu-item-1">
                        <li class="col-md-3">
                            <ul>
                                @foreach($root_categories as $root)
                                    <li>
                                        <span><a href="/electronics/{{ $root->id }}"> {{ $root->name }}</a></span>
                                    </li>

                                    <?php $children = $root->children()->get(); ?>

                                    <?php $children->each(function($child){ ?>
                                    <li><a href="/electronics/{{ $child->id }}"> {{ $child->name; }} </a></li>
                                    <?php }); ?>

                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <?php });  ?>

        <div class="search-header">
            <ul class="shop-cart bar-dropdown">
                <li><a href="#" title=""><i class="fa fa-shopping-cart"></i></a>
                    <ul id="shop-cart-box">
                        <li style="text-align: center;">
                            <h6><i class="fa fa-thumbs-o-down"></i> Sizin zənbiliniz boşdur.</h6>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
