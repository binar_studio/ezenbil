@if ($paginator->getLastPage() > 1)

    <div class="custom-pagination">
        <ul>
            @for ($i = 1; $i <= $paginator->getLastPage(); $i++)
                <li class=""><a class="{{ ($paginator->getCurrentPage() == $i) ? 'blue' : '' }}"
                                href="{{ $paginator->getUrl($i) }}" title="">{{ $i }}</a></li>
            @endfor
        </ul>
        <span><i>Pages {{ $paginator->getCurrentPage() }} of {{ $paginator->getLastPage() }}</i></span>
        <a href="{{ $paginator->getUrl($paginator->getCurrentPage()+1) }}" title="" class="{{ ($paginator->getCurrentPage() == $paginator->getLastPage()) ?  'disabled' : '' }}"><span>Next Page</span></a>
        <a href="{{ ($paginator->getCurrentPage() == $paginator->getLastPage()) ?  $paginator->getUrl(1) : '' }}"
           title="" class="{{ ($paginator->getCurrentPage() == 1) ?  'disabled' : '' }}"><span>Previous</span></a>
    </div>

@endif