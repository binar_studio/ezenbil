<div class="widget-body product-tab">
    <ul class="nav nav-tabs" id="myTab">
        <li class="active"><a data-toggle="tab" href="#recent">{{ $title1 }}</a></li>
        <li><a data-toggle="tab" href="#popular">{{ $title2 }}</a></li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div id="recent" class="tab-pane fade in active">
            <div class="tab-recent" id="tab-recent">
                <?php $i = 1; ?>
                @foreach($collection1 as $product)
                    <?php if ($i == 1) {
                        echo '<ul>';
                    } ?>
                    <li><span><img src="{{ Image::path($product["mainPhoto"]["filename"], 'resizeCrop', 85, 100) }}" alt=""/></span>

                        <h3><a href="/product/{{ $product->id }}" title="">{{ $product["name_az"] }}</a></h3>

                        <p>{{ $product["description_az"] }}</p>
                        <ul>
                            <li><a href="#" title=""><i class="fa fa-shopping-cart"></i></a>
                            </li>
                            <li><a href="#" title=""><i class="fa fa-heart"></i></a></li>
                        </ul>
                    </li>
                    <?php if ($i == 3) {
                        echo '</ul>';
                        $i = 0;
                    } $i++; ?>
                @endforeach
            </div>
        </div>
        <div id="popular" class="tab-pane fade">
            <div id="recent2" class="tab-pane fade in active">
                <div class="tab-recent" id="tab-popular">
                    <?php $i = 1; ?>
                    @foreach($collection2 as $product)
                        <?php if ($i == 1) {
                            echo '<ul>';
                        } ?>
                        <li><span><img src="{{ Image::path($product["mainPhoto"]["filename"], 'resizeCrop', 85, 100) }}" alt=""/></span>

                            <h3><a href="/product/$product->id" title="">{{ $product["name_az"] }}</a></h3>

                            <p>{{ $product["description_az"] }}</p>
                            <ul>
                                <li><a href="#" title=""><i class="fa fa-shopping-cart"></i></a>
                                </li>
                                <li><a href="#" title=""><i class="fa fa-heart"></i></a></li>
                            </ul>
                        </li>
                        <?php if ($i == 3) {
                            echo '</ul>';
                            $i = 0;
                        } $i++; ?>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>