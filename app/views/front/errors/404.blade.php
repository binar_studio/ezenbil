@extends('front.layouts.master')


@section('css')
    {{ HTML::style('front/css/cyan.css', array("rel" => "stylesheet")); }} <!-- Orange -->
@stop


@section('content')

    <section class="block">
        <Div class="container">
            <div class="row">
                <Div class="col-md-12">
                    <div class="error-page">
                        <img src="{{ asset('front/images/404.jpg') }}" alt="" />
                        <h2><i>Oops!</i> Page was not found</h2>
                        <h3>It appears the page you were looking for doesn't exist. Sorry about that.</h3>
                        <form>
                            <input type="text" placeholder="Search Here" />
                            <input type="submit" class="submit" value="Go"/>
                        </form>
                        <ul>
                            <li><a href="#" title=""><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" title=""><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" title=""><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" title=""><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#" title=""><i class="fa fa-rss"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>


@stop

@section('endjs')

@stop