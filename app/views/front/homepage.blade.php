@extends('front.layouts.master')

@section('content')



	<div class="tp-banner-container">
		<div class="tp-banner" >
			<ul>
				<li data-transition="3dcurtain-vertical" data-slotamount="7" data-masterspeed="500" >
					<img src="{{ asset('front/images/slides/slide1.jpg') }}"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" />

					<div class="tp-caption lfl" data-x="-20" data-y="12" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><span><img src="images/boy.png" alt="" /></span></div>

					<div class="tp-caption lfr" data-x="290" data-y="12" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><span><img style="z-index:0;" src="images/girl1.png" alt="" /></span></div>

					<div class="tp-caption lfb" data-x="150" data-y="12" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><span><img src="images/girl2.png" alt="" /></span></div>



					<div class="tp-caption lft" data-x="640" data-y="230" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><span><img src="images/box1.png" alt="" /></span></div>

					<div class="tp-caption lfb" data-x="1045" data-y="400" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><span><img src="images/box2.png" alt="" /></span></div>

					<div class="tp-caption skewfromright" data-x="670" data-y="205" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style=""><h2 class="slide4-text1">More then <i>branding</i> agancy</h2></div>

					<div class="tp-caption skewfromleft" data-x="670" data-y="250" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="z-index:8;"><h2 class="slide4-text2">Discover more about store Advanced Technology</h2></div>

					<div class="tp-caption skewfromright" data-x="670" data-y="290" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="z-index:8;"><p class="slide4-text3">Sed eget tellus sapien. Proinupmoso etiamgo velmis dolor amet pPro-<br/>inupmoso  etiamgo velmis dolor amet promo elit.</p></div>

					<div class="tp-caption skewfromright" data-x="670" data-y="360" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><a href="#" title="" class="slide2-btn">Purchase on Themeforest</a></div>
				</li><!-- SLIDE 1  -->

				<li data-transition="slotzoom-horizontal" data-slotamount="7" data-masterspeed="500" >
					<img src="{{ asset('front/images/slides/slide2.jpg') }}"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" />
					<div class="tp-caption skewfromleft" data-x="292" data-y="196" data-speed="1500" data-start="800" data-easing="SlowMo.ease" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><span class="slide-text1">Shop<i>inn</i></span></div>

					<div class="tp-caption skewfromright" data-x="158" data-y="244" data-speed="1500" data-start="800" data-easing="Sine.easeInOut" data-endspeed="500" data-captionhidden="on" style="font-size:30px; z-index: 4"><span class="slide-text1">More then branding <i>agency</i></span></div>

					<div class="tp-caption fade" data-x="193" data-y="285" data-speed="1500" data-start="800" data-easing="Circ.easeIn" data-endspeed="500" data-captionhidden="on" style="font-size:14px; z-index: 4"><span class="slide-text1"><i>Discover more about store Advanced Technology</i></span></div>

					<div class="tp-caption sfr" data-x="314" data-y="326" data-speed="1000" data-start="800" data-easing="easeInBounce" data-endspeed="500" data-captionhidden="on" style="font-size:14px; z-index: 4"><a class="slide-text1-btn" href="#" title="">VIEW MORE</a></div>

					<div class="tp-caption lfr" data-x="723" data-y="88"  data-speed="1500" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="z-index: 4;"><img src="images/slide-hand.png" alt="" /></div>

				</li><!-- SLIDE 2  -->

				<li data-transition="curtain-2" data-slotamount="7" data-masterspeed="500" >
					<img src="{{ asset('front/images/slides/slide3.jpg') }}"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" />

					<div class="tp-caption skewfromleft" data-x="2" data-y="141" data-speed="2000" data-start="1200" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="z-index: 4"><span><img src="images/slide-girl.png" alt="" /></span></div>

					<div class="tp-caption skewfromright" data-x="650" data-y="162" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4; font-size:40px;"><span class="slide2-text1">SHOPPING</span></div>

					<div class="tp-caption skewfromleft" data-x="650" data-y="224" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><span class="slide2-text2">More then branding agency</span></div>

					<div class="tp-caption skewfromright" data-x="650" data-y="294" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4; font-size:30px;"><span class="slide2-text1">More then branding</span></div>

					<div class="tp-caption skewfromleft " data-x="650" data-y="375" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><a href="#" title="" class="slide2-btn">Purchase on Themeforest</a></div>

				</li><!-- SLIDE 3  -->

				<li data-transition="boxslide" data-slotamount="7" data-masterspeed="500" >
					<img src="{{ asset('front/images/slides/slide4.jpg') }}"  alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat" />

					<div class="tp-caption lfb" data-x="2" data-y="2" data-speed="2000" data-start="800" data-easing="Power0.easeIn" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><span><img src="images/balloons.png" alt="" /></span></div>

					<div class="tp-caption skewfromleft" data-x="680" data-y="210" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><h2 class="slide3-text1"> our most complete <i>WooCommerce</i> theme yet!</h2></div>

					<div class="tp-caption skewfromright" data-x="680" data-y="252" data-speed="2000" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4">
						<ul class="slide3-list">
							<li><i class="fa fa-bitbucket"></i><span>Truly Fantastic Theme For Your Shop Venture</span></li>
							<li><i class="fa fa-shopping-cart"></i><span>Buy It Now! Fantastic Deals</span></li>
							<li><i class="fa fa-thumb-tack"></i><span>For More Information Call +386 40 000 1111 now!</span></li>
							<li><i class="fa fa-lightbulb-o"></i><span>Buy It On Themeforest For Only $55</span></li>
						</ul>
					</div>

					<div class="tp-caption" data-x="-20" data-y="95" data-speed="500" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-captionhidden="on" style="font-size:40px; z-index: 4"><span><img src="images/slide-mobile.png" alt="" /></span></div>

				</li><!-- SLIDE 4  -->
			</ul>
		</div>
	</div>




<section class="block">
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<div class="promo">
					<img src="http://placehold.it/367x205" alt="" />
					<span> <i>30% OFFER</i> Collections</span>
				</div>
			</div>

			<div class="col-md-4">
				<div class="promo yellow">
					<img src="http://placehold.it/367x205" alt="" />
					<span> <i>FREE DELIVERY</i> Over $60 / $80</span>
				</div>
			</div>

			<div class="col-md-4">
				<div class="promo purple">
					<img src="http://placehold.it/367x205" alt="" />
					<span> <i>SEASON SALE</i> Up To 50%</span>
				</div>
			</div>
		</div>
	</div>
</section>

<Section class="block">
	<Div class="container">
		<div class="fixed-img sec-bg2"></div>
	<div id="best-seller">
		<div class="row">
			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>

			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>

			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>

			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>

			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>

			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>

			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>

			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>

			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>

			<div class="col-md-3">
				<div class="best-seller seller2">
					<img class="seller-still" alt="" src="http://placehold.it/238x361">
					<img class="seller-hover" alt="" src="http://placehold.it/238x361">
					<h3>BEST KIDS DESIGN 2014</h3>

					<ul class="tooltip-btn">
						<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
						<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
					</ul>
					<span><i>$360</i>$460</span>
				</div>
			</div>
		</div>

	</div>
	</div>
</section>

<section class="block">
	<div class="container">
		<div class="heading1">
			<h2><i>PRODUCT </i>ON SALE</h2>
			<span>BRAND FOR EVERY STYLE. FIND YOURS.</span>
		</div>

		<div class="section-tab2">

			<!-- tabs -->
			<div class="sky-tabs sky-tabs-pos-top-left sky-tabs-anim-flip">








				<input type="radio" name="sky-tabs" id="sky-tab5" class="sky-tab-content-5" />
				<label for="sky-tab5"><span><span>SHIRTS</span></span></label>

				<input type="radio" name="sky-tabs" id="sky-tab4" class="sky-tab-content-4" />
				<label for="sky-tab4"><span><span>SHOES</span></span></label>

				<input type="radio" name="sky-tabs" id="sky-tab3" class="sky-tab-content-3" />
				<label for="sky-tab3"><span><span>CAPS</span></span></label>

				<input type="radio" name="sky-tabs" id="sky-tab2" class="sky-tab-content-2" />
				<label for="sky-tab2"><span><span>BEAUTY</span></span></label>

				<input type="radio" name="sky-tabs" checked id="sky-tab1" class="sky-tab-content-1" />
				<label for="sky-tab1"><span><span>PAINTS</span></span></label>


				<ul>
					<li class="sky-tab-content-1">
						<div class="row">
							<div class="col-md-3">
								<div class="best-seller seller3">
									<img alt="" src="http://placehold.it/251x279">
									<h3>BEST KIDS DESIGN 2014</h3>

									<ul class="tooltip-btn">
										<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
										<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
									</ul>
									<a href="#" title="">DETAILS</a>
									<span><i>$360</i>$460</span>
								</div>
							</div>

							<div class="col-md-3">
								<div class="best-seller seller3">
									<img alt="" src="http://placehold.it/251x279">
									<h3>BEST KIDS DESIGN 2014</h3>

									<ul class="tooltip-btn">
										<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
										<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
									</ul>
									<a href="#" title="">DETAILS</a>
									<span><i>$360</i>$460</span>
								</div>
							</div>

							<div class="col-md-3">
								<div class="best-seller seller3">
									<img alt="" src="http://placehold.it/251x279">
									<h3>BEST KIDS DESIGN 2014</h3>

									<ul class="tooltip-btn">
										<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
										<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
									</ul>
									<a href="#" title="">DETAILS</a>
									<span><i>$360</i>$460</span>
								</div>
							</div>

							<div class="col-md-3">
								<div class="best-seller seller3">
									<img alt="" src="http://placehold.it/251x279">
									<h3>BEST KIDS DESIGN 2014</h3>

									<ul class="tooltip-btn">
										<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
										<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
									</ul>
									<a href="#" title="">DETAILS</a>
									<span><i>$360</i>$460</span>
								</div>
							</div>

						</div>
						<div class="row">
							<div class="col-md-3">
								<div class="best-seller seller3">
									<img alt="" src="http://placehold.it/251x279">
									<h3>BEST KIDS DESIGN 2014</h3>

									<ul class="tooltip-btn">
										<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
										<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
									</ul>
									<a href="#" title="">DETAILS</a>
									<span><i>$360</i>$460</span>
								</div>
							</div>

							<div class="col-md-3">
								<div class="best-seller seller3">
									<img alt="" src="http://placehold.it/251x279">
									<h3>BEST KIDS DESIGN 2014</h3>

									<ul class="tooltip-btn">
										<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
										<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
									</ul>
									<a href="#" title="">DETAILS</a>
									<span><i>$360</i>$460</span>
								</div>
							</div>

							<div class="col-md-3">
								<div class="best-seller seller3">
									<img alt="" src="http://placehold.it/251x279">
									<h3>BEST KIDS DESIGN 2014</h3>

									<ul class="tooltip-btn">
										<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
										<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
									</ul>
									<a href="#" title="">DETAILS</a>
									<span><i>$360</i>$460</span>
								</div>
							</div>

							<div class="col-md-3">
								<div class="best-seller seller3">
									<img alt="" src="http://placehold.it/251x279">
									<h3>BEST KIDS DESIGN 2014</h3>

									<ul class="tooltip-btn">
										<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
										<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
									</ul>
									<a href="#" title="">DETAILS</a>
									<span><i>$360</i>$460</span>
								</div>
							</div>

						</div>


					</li>

					<li class="sky-tab-content-2">

							<div class="row">
								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

							</div>

					</li>

					<li class="sky-tab-content-3">

						<div class="row">
							<div class="col-md-3">
								<div class="best-seller seller3">
									<img alt="" src="http://placehold.it/251x279">
									<h3>BEST KIDS DESIGN 2014</h3>

									<ul class="tooltip-btn">
										<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
										<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
									</ul>
									<a href="#" title="">DETAILS</a>
									<span><i>$360</i>$460</span>
								</div>
							</div>

							<div class="col-md-3">
								<div class="best-seller seller3">
									<img alt="" src="http://placehold.it/251x279">
									<h3>BEST KIDS DESIGN 2014</h3>

									<ul class="tooltip-btn">
										<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
										<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
									</ul>
									<a href="#" title="">DETAILS</a>
									<span><i>$360</i>$460</span>
								</div>
							</div>
						</div>



					</li>

					<li class="sky-tab-content-4">

							<div class="row">
								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

							</div>

					</li>

					<li class="sky-tab-content-5">

					<div id="product-shirt">
							<div class="row">
								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

								<div class="col-md-3">
									<div class="best-seller seller3">
										<img alt="" src="http://placehold.it/251x279">
										<h3>BEST KIDS DESIGN 2014</h3>

										<ul class="tooltip-btn">
											<li class="cart"><a href="#" title=""><i class="fa fa-heart"></i></a></li>
											<li class="shop"><a href="#" title=""><i class="fa fa-shopping-cart"></i></a></li>
										</ul>
										<a href="#" title="">DETAILS</a>
										<span><i>$360</i>$460</span>
									</div>
								</div>

							</div>
					</div>


					</li>
				</ul>
			</div>

				</div>


		</div>
</section>


<section id="brands">
	<div class="fixed-img sec-bg3"></div>
	<div class="container">
		<span><i>OUR</i> BRANDS</span>
		<div class="brands">
			<a href="#" title=""><img src="http://placehold.it/169x101" alt="" /></a>
			<a href="#" title=""><img src="http://placehold.it/169x101" alt="" /></a>
			<a href="#" title=""><img src="http://placehold.it/169x101" alt="" /></a>
			<a href="#" title=""><img src="http://placehold.it/169x101" alt="" /></a>
			<a href="#" title=""><img src="http://placehold.it/169x101" alt="" /></a>

			<a href="#" title=""><img src="http://placehold.it/169x101" alt="" /></a>
			<a href="#" title=""><img src="http://placehold.it/169x101" alt="" /></a>
			<a href="#" title=""><img src="http://placehold.it/169x101" alt="" /></a>
			<a href="#" title=""><img src="http://placehold.it/169x101" alt="" /></a>
			<a href="#" title=""><img src="http://placehold.it/169x101" alt="" /></a>
		</div>
	</div>

</section>



@stop

@section('endjs')
<!-- SLIDER REVOLUTION -->
{{ HTML::script("front/js/jquery.themepunch.plugins.min.js"); }}
{{ HTML::script("front/js/jquery.themepunch.revolution.min.js"); }}

<script type="text/javascript">
	var revapi;
	jQuery(document).ready(function() {
	revapi = jQuery('.tp-banner').revolution(
	{
	delay:10000,
	startwidth:1170,
	startheight:533,
	autoHeight:"off",
	navigationType:"none",
	hideThumbs:10,
	fullWidth:"on",
	fullScreen:"off",
	fullScreenOffsetContainer: ""
	});
	});
</script>

<script type="text/javascript">

	$(document).ready(function() {



	$("#best-seller").owlCarousel({
	autoPlay :true,
	stopOnHover : true,
	navigation:true,
	goToFirstSpeed : 2000,
	slideSpeed:1500,
	singleItem : true,
	autoHeight : true,
	pagination:false,
	transitionStyle:"goDown"
	});

	$(".brands").owlCarousel({
	autoPlay: 3000, //Set AutoPlay to 20 seconds
	items : 6
	});

	$(".footer-deal").owlCarousel({
	autoPlay :true,
	stopOnHover : true,
	goToFirstSpeed : 2000,
	slideSpeed:1500,
	singleItem : true,
	autoHeight : true,
	transitionStyle:"goDown",
	paginationNumbers:true
	});

	});


</script>


@stop