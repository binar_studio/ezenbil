<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Index</title>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>

    <!-- Styles -->

    {{ HTML::style('front/css/bootstrap.min.css'); }} <!-- bootstrap css -->
    {{ HTML::style('front/font-awesome-4.0.3/css/font-awesome.css'); }} <!-- Font Awesome -->
    {{ HTML::style('front/css/owl.carousel.css'); }} <!-- Owl Carousal -->
    {{ HTML::style('front/css/animate.css'); }} <!-- Animation -->

    {{ HTML::style('front/css/style.css'); }} <!-- Responsive -->
    {{ HTML::style('front/css/responsive.css'); }} <!-- Responsive -->
    {{ HTML::style('front/css/revolution.css'); }} <!-- Slider Revolution -->

    {{ HTML::script("front/js/jquery-1.10.2.js"); }}<!-- Jquery -->
    {{ HTML::script("front/js/jquery-ui.min.js"); }}<!-- Jquery -->
    {{ HTML::script("front/js/jquery.noty.packaged.min.js"); }}<!-- Jquery noty for notifications -->

    @yield('css')

</head>
<body>
<div class="theme-layout">

    @include('front.elements.header')

    @if(Session::has('message'))

        <?php
        $message = Session::get('message');

        $text = null;
        $class = null;

        if (strpos($message, '|') !== false) {
            list($class, $text) = explode('|', $message);
        } else {
            $text = $message;
            $class = 'info';
        }

        ?>

        <div class="alert alert-header {{ 'alert-'.$class }}">
            {{ $text }}
        </div>
    @endif

    @yield('content')

    @include('front.elements.footer')

</div>

<!-- Script -->
{{ HTML::script("front/js/script.js"); }}<!-- Common -->
{{ HTML::script("front/js/bootstrap.min.js"); }}<!-- Bootstrap -->
{{ HTML::script("front/js/modernizr.js"); }}<!-- modernizr -->
{{ HTML::script("front/js/owl.carousel.min.js"); }}<!-- Owl Carousal -->
{{ HTML::script("front/js/onscreen.js"); }}<!-- On Screen -->

{{ HTML::script("front/js/cart.js"); }}<!-- Cart.js -->
{{ HTML::script("front/js/compare.js"); }}<!-- compare.js -->

@yield('endjs')

<script type="text/javascript">
    $(document).ready(function(){
        var nav = $('#wide-menu');
        var menuSpace = $('.menu-space');

        $(window).scroll(function () {
            if ($(this).scrollTop() > 80) {
                nav.addClass("f-nav");
                menuSpace.css({"display": "block"});
            } else {
                nav.removeClass("f-nav");
                menuSpace.css({"display": "none"});
            }
        });

        refreshCartBox();
    });
</script>

</body>
</html>