@extends('front.layouts.master')


@section('css')
    {{ HTML::style('front/css/cyan.css', array("rel" => "stylesheet")); }} <!-- Orange -->
@stop


@section('content')

    <section class="block">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="single-product-tab">
                                <div class="tab-content" id="single-post-content">
                                    @foreach($product["photos"] as $photo)
                                        <?php $defaultPhoto = $photo['id'] == $product['main_photo_id'] ?>
                                        <div id="image{{ $photo["id"] }}"
                                             class="tab-pane fade @if($defaultPhoto) in active @endif">
                                            <img src="{{ asset($photo["filename"]) }}" alt=""/>
                                        </div>
                                    @endforeach
                                </div>

                                <ul class="nav nav-tabs" id="single-post-tabs">
                                    @foreach($product["photos"] as $photo)
                                        <li><a data-toggle="tab" href="#image{{ $photo["id"] }}">
                                                <img style="width: 100px;" src="{{ asset($photo["filename"]) }}"
                                                     alt=""/></a>
                                        </li>
                                    @endforeach
                                </ul>

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="single-post-desc">
                                <div class="single-post-head">
                                    <ul>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li class="un-rate"><i class="fa fa-star"></i></li>
                                    </ul>
                                    <span><i>AZN</i>{{ $product["price"] }}</span>

                                    <h3>{{ $product->name }}<br/> <i>{{ $category->name }}</i></h3>

                                    <p>
                                        <i>
                                            @foreach($category->getAncestors() as $ancestor)
                                                <a href="/electronics/{{ $ancestor->id }}">{{ $ancestor->name }}</a>
                                            @endforeach
                                        </i>
                                    </p>
                                </div>

                                <div class="cart-options">
                                    <div class="decrements">
                                        <form id="myform" method="POST" action="#">
                                            <input type="button" value="-" class="qtyminus" field="quantity"/>
                                            <input id="quantity" type="text" name="quantity" value="1" class="qty"/>
                                            <input type="button" value="+" class="qtyplus" field="quantity"/>
                                        </form>
                                    </div>
                                </div>

                                <div class="choose-size-tab margin">
                                    @if(! $product->attributes->isEmpty())
                                        <?php $attrGroup = $product->attributes->first()->group; ?>
                                        <p>
                                            {{ $attrGroup->name }}:

                                            @if($attrGroup->is_color_group)
                                                <ul class="select-color">
                                                    @foreach($product->attributes as $attribute)
                                                        <li>
                                                            <label class="big">
                                                                <span style="background-color: {{ $attribute->color }}"></span>
                                                                <input name="attrRadio" type="radio" hidden="hidden"
                                                                       value="{{ $attribute->id }}"/>
                                                            </label>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            @endif
                                    @else

                                        <select name="attrSelect">
                                            @foreach($product->attributes as $attribute)
                                                <option value="{{ $attribute->id }}">{{ $attribute->name }}</option>
                                            @endforeach
                                        </select>
                                        </p>
                                    @endif
                                </div>

                                <a id="btn-add-to-cart" title="">Add to Cart</a>
                                <a href="#" title="">Add to Compare</a>

                                <div class="product-details margin-top">
                                    <ul>
                                        @foreach($product->features as $feature)
                                            <li><span>{{ $feature["name_az"]; }}</span>

                                                <p>
                                                    @if($feature["is_boolean"])
                                                        @if($feature["pivot"]["value"] == 1)
                                                            <i class="fa fa-check"
                                                               style="font-size: 18px; color:#007534;position: absolute"></i>
                                                        @elseif($feature["pivot"]["value"] == 0)
                                                            <i class="fa fa-times"
                                                               style="font-size: 18px; color:#000;position: absolute"></i>
                                                        @endif
                                                    @else
                                                        {{ $feature["pivot"]["value"] }}
                                                    @endif
                                                </p>
                                            </li>
                                        @endforeach
                                    </ul>
                                    <p>
                                        {{ $product["description_az"] }}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane fade in active block">
                        <div class="fb-comments" data-width="100%" data-href="{{ Request::url() }}"
                             data-colorscheme="light"></div>
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id)) return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=132308013630186&version=v2.0";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                    </div>


                </div>

                <div class="col-md-3">
                    <aside>

                        @include('front.elements.tabbedProductsWidget', array('title1' => trans('general.recentProducts'), 'title2' =>  trans('general.popularProducts'), 'collection1' => $recent_products, 'collection2' => $popular_products))

                        <div class="widget-body">
                            <div class="heading-5">
                                <h2>DISCOUNT PRODUCT</h2>
                            </div>

                            <div class="w-discount">
                                <img alt="" src="http://placehold.it/368x286">

                                <div class="w-discount-des">
                                    <h3>WEBINANE.COM</h3>
                                    <i>20% OFF</i>

                                    <p>ODDER OVER $80</p>
                                    <a title="" href="#">SHOP NOW</a>
                                </div>
                            </div>
                        </div>

                    </aside>
                </div>

            </div>
        </div>
    </section>
@stop

@section('endjs')
    <!-- Range Slider -->
    <script>
        $(document).ready(function () {
            // This button will increment the value
            $('.qtyplus').click(function (e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('input[name=' + fieldName + ']').val());
                // If is not undefined
                if (!isNaN(currentVal)) {
                    // Increment
                    $('input[name=' + fieldName + ']').val(currentVal + 1);
                } else {
                    // Otherwise put a 0 there
                    $('input[name=' + fieldName + ']').val(0);
                }
            });
            // This button will decrement the value till 0
            $(".qtyminus").click(function (e) {
                // Stop acting like a button
                e.preventDefault();
                // Get the field name
                fieldName = $(this).attr('field');
                // Get its current value
                var currentVal = parseInt($('input[name=' + fieldName + ']').val());
                // If it isn't undefined or its greater than 0
                if (!isNaN(currentVal) && currentVal > 0) {
                    // Decrement one
                    $('input[name=' + fieldName + ']').val(currentVal - 1);
                } else {
                    // Otherwise put a 0 there
                    $('input[name=' + fieldName + ']').val(0);
                }
            });

            $("#tab-recent").owlCarousel({
                autoPlay: true,
                stopOnHover: true,
                navigation: true,
                goToFirstSpeed: 2000,
                slideSpeed: 1500,
                singleItem: true,
                autoHeight: true,
                pagination: false,
                transitionStyle: "goDown"
            });

            $("#tab-popular").owlCarousel({
                autoPlay: true,
                stopOnHover: true,
                navigation: true,
                goToFirstSpeed: 2000,
                slideSpeed: 1500,
                singleItem: true,
                autoHeight: true,
                pagination: false,
                transitionStyle: "goDown"
            });
        });


    </script>


    <script>
        $(document).ready(function () {

            $("#btn-add-to-cart").click(function () {
                attribute_id = null;
                quantity = $("#quantity").val();

                if ($("input[name=attrRadio]").length) {
                    attribute_id = $("input[name=attrRadio]:checked").val();
                } else if ($("select[name=attrSelect]").length) {
                    attribute_id = $("select[name=attrSelect]").val();
                }

                addToCart({{ $product->id }}, attribute_id, quantity)

            });

        });
    </script>
@stop
