/**
 * Created by togrul on 2/4/15.
 */

function addToCart(product_id, attribute_id, quantity) {

    console.log("add to cart");

    if (typeof(quantity) === 'undefined') quantity = 1;

    $.post(
        "/ajaxAddToCart",
        {
            product_id: product_id,
            attribute_id: attribute_id,
            quantity: quantity
        }
    ).done(function (data) {

            refreshCartBox();
            var shopCart = $(".shop-cart");

            shopCart.effect("shake", {times: 2, distance: 10}, 500);


            //alert( "Data Loaded: " + data );
            console.log(data);
        });

}

function changeCartItemQuantity(product_line_item_id, quantity) {

    console.log("change item quantity");

    if(typeof(quantity)==='undefined') quantity = 10;

    $.post(
        "../ajaxChangeQuantity",
        {
            product_line_item_id: product_line_item_id,
            quantity: quantity
        }
    ).done(function( data ) {

            refreshCartContainer();
            refreshCartBox();

            //$(".shop-cart").effect("shake");


            //alert( "Data Loaded: " + data );
            console.log(data);
        });

}

function refreshCartBox() {

    console.log("refresh cart box");

    $("#shop-cart-box").load("../ajaxCart");

    console.log("cart box refreshed");
}

function refreshCartContainer() {
    console.log('refresh container');
    $('#cart-container').load("../ajaxCartContainer");
}