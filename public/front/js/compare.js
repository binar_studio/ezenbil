function addToCompare(product_id) {
    console.log("add to compare");

    $.post(
        "../ajaxAddToCompare",
        {
            product_id: product_id
        }
    ).done(function (data) {
            var n = noty({
                layout: 'bottomRight',
                text: '<h5>'+data.message+'<h5>'+data.count,
                dismissQueue: true,
                type: data.notification,
                animation: {
                    open: 'animated bounceInLeft', // Animate.css class names
                    close: 'animated bounceOutLeft' // Animate.css class names
                },
                timeout: 2500,
                modal: false,
                killer: true
            });

            $("#compare").removeClass('hidden');
            var comparecount = $("#compare-count");
            comparecount.html(data.count);

        }).error(function(error){
            console.log(error);
        });
}

function removeFromCompare(product_id){
    console.log("remove from compare");

    $.post(
        "/ajaxRemoveFromCompare",
        {
            product_id: product_id
        }
    ).done(function (data) {
            //console.log(data);

            location.reload();
        }).error(function(error){
            console.log(error);
        });
}

